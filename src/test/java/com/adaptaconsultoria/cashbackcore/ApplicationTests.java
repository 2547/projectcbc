package com.adaptaconsultoria.cashbackcore;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.adaptaconsultoria.cashbackcore.services.AuthService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ApplicationTests {
	@Autowired
	private MockMvc mockMvc;

	@InjectMocks
	private AuthService authService;

	@Test
	public void shouldReturnDefaultMessage() {
		
	}

	public void postRequest(String url, Object param) {
		try {
			MvcResult result = mockMvc.perform(post("/campaings?companyId=34").contentType(MediaType.APPLICATION_JSON).content("json").characterEncoding("utf-8")).andExpect(status().isOk()).andReturn();
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
