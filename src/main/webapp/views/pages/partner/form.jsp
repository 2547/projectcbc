<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="../../tiles/templates/css.jsp"></jsp:include>
<jsp:include page="../../tiles/templates/header.jsp"></jsp:include>
<!-- INCLUDED PLUGIN CSS ON THIS PAGE -->

</head>
<body>
	<!-- body >> main >> wrapper >> content -->
	<!-- START MAIN -->
	<div id="main">
		<!-- START WRAPPER -->
		<div class="wrapper">
			<!-- START LEFT SIDEBAR NAV  MENU-->
			<jsp:include page="../../tiles/templates/menu.jsp"></jsp:include>
			<!-- END LEFT SIDEBAR NAV MENU-->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START CONTENT -->
			<section id="content">
				<div class="container">
					<div id="basic-form" class="section">
						<div class="row">
							<div class="col s12 m12 l12 ">
								<div class="card-panel">

									<div class="row row-header">
										<div class="col s12 m12 l12 ">
											<h5>${title}</h5>
											<div class="divider"></div>
										</div>
									</div>

									<div class="row">
										<form:form class="col s12" modelAttribute="obj" id="${formId}">

											<div class="row">
												<div class="toolbar col s12">
													<a class="btn btn-floating" href="${pageContext.request.contextPath}${uri}/list">
														<i class="material-icons">arrow_back</i>
													</a>
													<span class="divider"></span>
													<button class="btn btn-floating" type="submit">
														<i class="material-icons right">save</i>
													</button>
													<a class="btn btn-floating" href="${pageContext.request.contextPath}${uri}">
														<i class="material-icons">add</i>
													</a>
												</div>
											</div>

											<form:input type="hidden" name="id" id="id" path="id" />
											<input type="hidden" name="account" id="account" value="${obj.account.id}" />

											<div class="row">
												<div class="input-field col s12 l5">
													<form:input id="name" type="text" class="required" name="name" path="name" />
													<label for="name">Name *</label>
												</div>
												<div class="input-field col s9 l5">
													<form:input id="email" type="email" class="required" name="email" path="email" />
													<label for="email">E-mail *</label>
												</div>
												<div class="col s3 l2">
													<br>
													<input type="checkbox" class="checkbox" id="isactive" name="isactive" <c:if test="${obj.isactive}">checked</c:if> /> <label for="isactive">Active</label>
												</div>
											</div>

											<div class="row">
												<div class="input-field col s12">
													<form:input id="description" type="text" name="description" path="description" />
													<label for="description">Description</label>
												</div>
											</div>

											<div class="row">
												<div class="input-field col s12 l8">
													<form:input id="url" type="text" name="url" path="url" />
													<label for="url">URL</label>
												</div>
												<div class="input-field col s12 l4">
													<form:input id="phone" type="text" name="phone" path="phone" maxlength="40" />
													<label for="phone">Phone</label>
												</div>
											</div>

											<div class="row">
												<div class="input-field col s12 l6">
													<label class="label-control active" for="partnerCategory">Category *</label>
													<select class="select2 browser-default required" name="partnerCategory" id="partnerCategory" >
														<option value="" disabled selected>Not selected</option>
														<c:forEach items="${categories}" var="category">
															<option <c:if test='${not empty obj && obj.partnerCategory.id == category.id}'> selected </c:if> value="${category.id}">${category.name}</option>
														</c:forEach>
													</select>
												</div>
												<div class="input-field col s12 l6">
													<label class="label-control active" for="leader">Leader</label>
													<select class="browser-default" name="leader" id="leader" >
														<option value="" disabled selected>Not selected</option>
														<c:if test='${not empty obj && not empty obj.leader}'>
															<option selected value="${obj.leader.id}">${obj.leader.name}</option>
														</c:if> 
													</select>
												</div>
											</div>

											<div class="row row-header" style="margin-bottom: 20px;">
												<div class="col s12">
													<h5>Address</h5>
													<div class="divider"></div>
												</div>
											</div>

											<div class="row">
												<div class="input-field col s12 l6">
													<label class="label-control active" for="country">Country</label>
													<select class="browser-default required" name="country" id="country" >
														<option value="" disabled selected>Not selected</option>
														<c:if test='${not empty obj && not empty obj.address && not empty obj.address.country}'>
															<option selected value="${obj.address.country.id}">${obj.address.country.name}</option>
														</c:if> 
													</select>
												</div>
												<div class="input-field col s12 l6">
													<input id="zipcode" type="text" name="zipcode" value="${obj.address.zipcode}" />
													<label for="zipcode">Zip code</label>
												</div>
												<div class="input-field col s12 l6">
													<input id="address1" type="text" name="address1" value="${obj.address.address1}" />
													<label for="address1">Address 1</label>
												</div>
												<div class="input-field col s12 l6">
													<input id="address2" type="text" name="address2" value="${obj.address.address2}" />
													<label for="address2">Address 2</label>
												</div>
												<div class="input-field col s12 l6">
													<input id="number" type="text" name="number" value="${obj.address.number}" />
													<label for="number">Number</label>
												</div>
												<div class="input-field col s12 l6">
													<input id="district" type="text" name="district" value="${obj.address.district}" />
													<label for="district">District</label>
												</div>
												<div class="input-field col s12 l6">
													<label class="label-control active" for="region">Region</label>
													<select class="browser-default" name="region" id="region" >
														<option value="" disabled selected>Not selected</option>
														<c:if test='${not empty obj && not empty obj.address && not empty obj.address.region}'>
															<option selected value="${obj.address.region.id}">${obj.address.region.name}</option>
														</c:if> 
													</select>
												</div>
												<div class="input-field col s12 l6">
													<label class="label-control active" for="city">City</label>
													<select class="browser-default" name="city" id="city" >
														<option value="" disabled selected>Not selected</option>
														<c:if test='${not empty obj && not empty obj.address && not empty obj.address.city}'>
															<option selected value="${obj.address.city.id}">${obj.address.city.name}</option>
														</c:if> 
													</select>
												</div>
											</div>

											<div class="row">
												<div class="input-field col s12 l3">
													<input id="lat" type="number" name="lat" value="${obj.address.lat}" />
													<label for="lat">Lat.</label>
												</div>
												<div class="input-field col s12 l3">
													<input id="lng" type="number" name="lng" value="${obj.address.lng}" />
													<label for="lng">Lng.</label>
												</div>
												<div class="input-field col s12 l6">
													<input id="geoinformation" type="text" name="geoinformation" value="${obj.address.geoinformation}" />
													<label for="geoinformation">Geoinformation</label>
												</div>
											</div>

										</form:form>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				<!--end container-->
			</section>
			<!-- END CONTENT -->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START RIGHT SIDEBAR NAV-->
			<!-- END RIGHT SIDEBAR NAV-->
		</div>
		<!-- END WRAPPER -->
	</div>
	<!-- END MAIN -->
	<!-- //////////////////////////////////////////////////////////////////////////// -->
	<jsp:include page="../../tiles/templates/footer.jsp"></jsp:include>
	<!-- SCRIPTS -->
	<jsp:include page="../../tiles/templates/js.jsp"></jsp:include>

	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/data-tables/js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pages/${js}"></script>

</body>
</html>
