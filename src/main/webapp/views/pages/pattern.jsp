<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="${pageContext.request.contextPath}/tiles/templates/css.jsp"></jsp:include>
<jsp:include page="${pageContext.request.contextPath}/tiles/templates/header.jsp"></jsp:include>
<!-- INCLUDED PLUGIN CSS ON THIS PAGE -->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">
 
    <!-- Include Editor style. -->
    <link href="https://cdn.jsdelivr.net/npm/froala-editor@2.9.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/froala-editor@2.9.1/css/froala_style.min.css" rel="stylesheet" type="text/css" />

</head>
<body>
	<!-- body >> main >> wrapper >> content -->
	<!-- START MAIN -->
	<div id="main">
		<!-- START WRAPPER -->
		<div class="wrapper">
			<!-- START LEFT SIDEBAR NAV  MENU-->
			<jsp:include page="${pageContext.request.contextPath}/tiles/templates/menu.jsp"></jsp:include>
			<!-- END LEFT SIDEBAR NAV MENU-->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START CONTENT -->
			<section id="content">
				<!--breadcrumbs start-->
				<div id="breadcrumbs-wrapper">
					<!-- Search for small screen -->
					<div class="header-search-wrapper grey lighten-2 hide-on-large-only">
						<input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
					</div>
					<div class="container">
						<div class="row">
							<div class="col s10 m6 l6">
								<h5 class="breadcrumbs-title">${title}</h5>
								<ol class="breadcrumbs">
									<li><a href="home">Home</a></li>
									<li><a href="campaign">${title}</a></li>
								</ol>
							</div>
						</div>
						<div id="basic-form" class="section">
							<div class="row">
								<div class="col s12 m12 l12">
									<div class="card-panel">
										<h4 class="header2">${formTitle}</h4>
										<div class="row">
											<form:form class="col s12" modelAttribute="campaign" id="${formId}">
												<form:input type="hidden" name="id" id="id" path="id" />
												<div class="row">
													<div class="input-field col s12">
														<form:input id="name" type="text" class="required" name="name" path="name" />
														<label for="name">Nome*</label>
													</div>
												</div>

												<div class="row">
													<div class="input-field col s12">
														<textarea id="description" class="materialize-textarea" name="description"></textarea>
														<label for="description">Descrição</label>
													</div>
												</div>

												<div class="row">
													<div class="input-field col s12">
														<form:input id="qtyLogins" type="number" min="0" name="qtyLogins" path="qtyLogins" />
														<label for="qtyLogins">Quantidade de Logins</label>
													</div>
												</div>

												<div class="row">
													<div class="input-field col s12">
														<form:input id="registerUrl" type="text" name="registerUrl" path="registerUrl" />
														<label for="registerUrl">URL de Registro</label>
													</div>
												</div>
												<br>
												<h4 class="header2">Configuração servidor de email</h4>

												<div class="row">
													<div class="input-field col s12">
														<form:input id="smtpHost" type="text" name="smtpHost" path="smtpHost" />
														<label for="smtpHost">SMTP host</label>
													</div>
												</div>

												<div class="row">
													<div class="input-field col s12">
														<form:input id="smtpPort" type="number" name="smtpPort" path="smtpPort" />
														<label for="smtpPort">SMTP Porta</label>
													</div>
												</div>

												<div class="row">
													<div class="input-field col s12">
														<form:input id="smtpUser" type="text" name="smtpUser" path="smtpUser" />
														<label for="smtpUser">SMTP Usuário</label>
													</div>
												</div>

												<div class="row">
													<div class="input-field col s12">
														<form:input id="smtpPassword" type="text" name="smtpPassword" path="smtpPassword" />
														<label for="smtpPassword">SMTP Senha</label>
													</div>
												</div>

												<div class="row">
													<div class="input-field col s12">
														<form:input id="smtpSender" type="text" name="smtpSender" path="smtpSender" />
														<label for="smtpSender">SMTP Remetente</label>
													</div>
												</div>
												<button class="btn waves-effect waves-light " type="button" id="test-email-config" name="action">Testar</button>
												<br>
												<br>
												<div class="row">
													<div class="col s12">
														<input type="checkbox" class="checkbox" id="email" name="email" ${email} /> <label for="email">Email</label>
													</div>
												</div>
												<br>
												<div class="row">
													<div class="col s12">
														<input type="checkbox" class="checkbox" id="phone" name="phone" ${phone} /> <label for="phone">Telefone</label>
													</div>
												</div>

												<div class="row">
													<div class="input-field col s12">
														<button class="btn cyan waves-effect waves-light right gradient-45deg-light-blue-cyan" type="submit" name="action">
															<i class="material-icons right">send</i>Enviar
														</button>
													</div>
												</div>
											</form:form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--end container-->
			</section>
			<!-- END CONTENT -->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START RIGHT SIDEBAR NAV-->
			<!-- END RIGHT SIDEBAR NAV-->
		</div>
		<!-- END WRAPPER -->
	</div>
	<!-- END MAIN -->
	<!-- //////////////////////////////////////////////////////////////////////////// -->
	<jsp:include page="${pageContext.request.contextPath}/tiles/templates/footer.jsp"></jsp:include>
	<!-- SCRIPTS -->
	<jsp:include page="${pageContext.request.contextPath}/tiles/templates/js.jsp"></jsp:include>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/data-tables/js/dataTables.buttons.js"></script>
	
	 <!-- Include external JS libs. -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>
 
    <!-- Include Editor JS files. -->
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/froala-editor@2.9.1/js/froala_editor.pkgd.min.js"></script>
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pages/${js}"></script>
</body>
</html>















