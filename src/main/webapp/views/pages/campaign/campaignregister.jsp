<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="../../tiles/templates/css.jsp"></jsp:include>
<jsp:include page="../../tiles/templates/header.jsp"></jsp:include>
<!-- INCLUDED PLUGIN CSS ON THIS PAGE -->

</head>
<body>
	<!-- body >> main >> wrapper >> content -->
	<!-- START MAIN -->
	<div id="main">
		<!-- START WRAPPER -->
		<div class="wrapper">
			<!-- START LEFT SIDEBAR NAV  MENU-->
			<jsp:include page="../../tiles/templates/menu.jsp"></jsp:include>
			<!-- END LEFT SIDEBAR NAV MENU-->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START CONTENT -->
			<section id="content">
				<!--breadcrumbs start-->
				<div id="breadcrumbs-wrapper">
					<!-- Search for small screen -->
					<div class="header-search-wrapper grey lighten-2 hide-on-large-only">
						<input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
					</div>
					<div class="container">
						<div class="row">
							<div class="col s10 m6 l6">
								<h5 class="breadcrumbs-title">${title}</h5>
								<ol class="breadcrumbs">
									<li><a href="campaignlist">Lista de Campanhas</a></li>
									<li><a href="campaignregister">Cadastrar Campanha</a></li>
								</ol>
							</div>
						</div>
						<div id="basic-form" class="section">
							<div class="row">
								<div class="col s12 m12 l12 ">
									<div class="card-panel">
										<h4 class="header2">${formTitle}</h4>
										<div class="row">
											<form:form class="col s12" modelAttribute="campaign" id="${formId}">
												<form:input type="hidden" name="id" id="id" path="id" />
												<div class="row">
													<div class="input-field col s12">
														<form:input id="name" type="text" class="required" name="name" path="name" />
														<label for="name">Nome da Campanha*</label>
													</div>
												</div>

												<div class="row">
													<div class="input-field col s12">
														<textarea id="description" class="materialize-textarea" name="description"></textarea>
														<label for="description">Descrição</label>
													</div>
												</div>

												<div class="row">

													<div class="input-field col s4">
														<form:input id="qtyLogins" type="number" min="1" name="qtyLogins" path="qtyLogins" />
														<label for="qtyLogins">Quantidade de Logins</label>
													</div>

													<div class="input-field col s8">
														<div class="input-field col s12">
															<button id="qty-logins" class="waves-effect waves-light tooltipped btn gradient-45deg-green-teal box-shadow-none border-round mr-1" data-position="top" data-delay="50" data-tooltip="Clique para solicitar uma nova quantidade de logins" type="button">
																 Solicitar Quantidade de Logins
															</button>
														</div>
													</div>
													
												</div>

												<div class="row">
													<div class="input-field col s12">
														<form:input id="registerUrl" type="text" name="registerUrl" path="registerUrl" />
														<label for="registerUrl">URL de Registro</label>
													</div>
												</div>
												<br>
												<h4 class="header2">Configuração servidor de email</h4>

												<div class="row">
													<div class="input-field col s12">
														<form:input id="smtpHost" type="text" name="smtpHost" path="smtpHost" />
														<label for="smtpHost">SMTP host</label>
													</div>
												</div>

												<div class="row">
													<div class="input-field col s12">
														<form:input id="smtpPort" type="number" name="smtpPort" path="smtpPort" />
														<label for="smtpPort">SMTP Porta</label>
													</div>
												</div>

												<div class="row">
													<div class="input-field col s12">
														<form:input id="smtpUser" type="text" name="smtpUser" path="smtpUser" />
														<label for="smtpUser">SMTP Usuário</label>
													</div>
												</div>

												<div class="row">
													<div class="input-field col s12">
														<form:input id="smtpPassword" type="text" name="smtpPassword" path="smtpPassword" />
														<label for="smtpPassword">SMTP Senha</label>
													</div>
												</div>

												<div class="row">
													<div class="input-field col s12">
														<form:input id="smtpSender" type="text" name="smtpSender" path="smtpSender" />
														<label for="smtpSender">SMTP Remetente</label>
													</div>
												</div>
												<button class="btn waves-effect waves-light " type="button" id="test-email-config" name="action">Testar</button>
												<br>
												<br>
												<div class="row">
													<div class="col s12">
														<input type="checkbox" class="checkbox" id="email" name="email" ${email} /> <label for="email">Email</label>
													</div>
												</div>
												<br>
												<div class="row">
													<div class="col s12">
														<input type="checkbox" class="checkbox" id="phone" name="phone" ${phone} /> <label for="phone">Telefone</label>
													</div>
												</div>

												<div class="row">
													<div class="input-field col s12">
														<button class="btn cyan waves-effect waves-light right gradient-45deg-light-blue-cyan" type="submit" name="action">
															<i class="material-icons right">send</i>Enviar
														</button>
													</div>
												</div>
											</form:form>
										</div>
									</div>
								</div>

								<div class="col s12 m12 l6 ${hide}" id="phase-div">
									<div class="card-panel">
										<h4 class="header2">
											Cadastrar Fase <a class="waves-effect waves-light gradient-45deg-purple-deep-orange gradient-shadow btn right file-field disabled" id="lead-file-button"> <i class="material-icons left">backup</i>
												Upload Leads <input type="file" name="file" id="lead-file">
											</a>
										</h4>

										<div class="row">
											<form class="col s12" id="phase-form">
												<input type="hidden" name="id" id="id-phase"> <input type="hidden" name=campaignId id="campaign-phase">
												<div class="row">
													<div class="input-field col s12">
														<input id="name-phase" type="text" class="required" name="name"> <label for="name">Nome*</label>
													</div>
												</div>

												<div class="row">
													<div class="input-field col s12">
														<textarea id="description-phase" class="materialize-textarea" name="description"></textarea>
														<label for="description">Descrição</label>
													</div>
												</div>

												<div class="row">
													<div class="input-field col s12">
														<input id="seqNo-phase" min="1" type="number" name="seqNo" required> <label for="description">Número de Sequência*</label>
													</div>
												</div>

												<div class="row">
													<div class="input-field col s12">
														<input id="url-phase" type="text" class="required" name="url"> <label for="url">URL*</label>
													</div>
												</div>
											</form>
										</div>
										<br>
										<table id="phase-table">
										</table>
									</div>
								</div>
								<div class="col s12 m12 l6 ${hide}">
									<div class="card-panel ">
										<h4 class="header2">Logins</h4>
										<div class="row">
											<form class="col s12" id="${formId3}">
												<input type="hidden" name="id" id="id-login"> <input type="hidden" name="campaignId" id="campaign-login">
												<div class="row">
													<div class="input-field col s12">
														<input id="name-login" type="text" class="required" name="name"> <label for="name">Nome*</label>
													</div>
												</div>
												<div class="row">
													<div class="input-field col s12">
														<input id="login" type="text" name="login" class="required"> <label for="login">Login*</label>
													</div>
												</div>
												<div class="row">
													<div class="input-field col s12">
														<input id="email-login" type="email" name="email"> <label for="email">Email</label>
													</div>
												</div>

												<div class="row">
													<div class="input-field col s12">
														<input id="cellphone" type="text" name="cellphone" class="phone"> <label for="cellphone">Telefone</label>
													</div>
												</div>
											</form>
										</div>
										<br>
										<table id="${tableId3}">
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<jsp:include page="campaignactionmodal.jsp"></jsp:include>
				<!--end container-->
			</section>
			<!-- END CONTENT -->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START RIGHT SIDEBAR NAV-->
			<!-- END RIGHT SIDEBAR NAV-->
		</div>
		<!-- END WRAPPER -->
	</div>
	<!-- END MAIN -->
	<!-- //////////////////////////////////////////////////////////////////////////// -->
	<jsp:include page="../../tiles/templates/footer.jsp"></jsp:include>
	<!-- SCRIPTS -->
	<jsp:include page="../../tiles/templates/js.jsp"></jsp:include>

	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/data-tables/js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pages/${js}"></script>

</body>
</html>















