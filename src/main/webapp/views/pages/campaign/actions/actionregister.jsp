<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>

<jsp:include page="../../../tiles/templates/css.jsp"></jsp:include>
<jsp:include page="../../../tiles/templates/header.jsp"></jsp:include>
<!-- INCLUDED PLUGIN CSS ON THIS PAGE -->

<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.css" rel="stylesheet">
<!-- Include Editor style. -->
</head>
<body>
	<!-- body >> main >> wrapper >> content -->
	<!-- START MAIN -->
	<div id="main">
		<!-- START WRAPPER -->
		<div class="wrapper">
			<!-- START LEFT SIDEBAR NAV  MENU-->
			<jsp:include page="../../../tiles/templates/menu.jsp"></jsp:include>
			<!-- END LEFT SIDEBAR NAV MENU-->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START CONTENT -->
			<section id="content">
				<!--breadcrumbs start-->
				<div id="breadcrumbs-wrapper">
					<!-- Search for small screen -->
					<div class="header-search-wrapper grey lighten-2 hide-on-large-only">
						<input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize">
					</div>
					<div class="container">
						<div class="row">
							<div class="col s10 m6 l6">
								<h5 class="breadcrumbs-title">${title}</h5>
								<ol class="breadcrumbs">
									<li><a href="campaignlist">Lista de Campanhas</a></li>
									<li><a href="campaignregister">Cadastrar Campanha</a></li>
									<li><a href="campaign">${title}</a></li>
								</ol>
							</div>
						</div>
						<div id="basic-form" class="section">
							<div class="col s12 m12 l6">
								<div class="card-panel">
									<div class="row">
										<div class="row">
											<form class="col s12" id="${formId}">
												<h4 class="header2">${title}</h4>
												<input type="hidden" name="id" id="id-action"> <input type="hidden" name="phaseId" id="phase-action">
												<div class="row">
													<div class="input-field col s12">
														<input id="seqNo-action" type="number" name="seqNo" required> <label for="seqNo">Número de Sequência</label>
													</div>
												</div>
												<div class="row">
													<div class="input-field col s12">
														<input id="emailSubject-action" type="text" name="emailSubject" required> <label for="emailSubject">Email Assunto</label>
													</div>
												</div>
												<br>
												<h4 class="header2">Conteúdo do email</h4>
												<textarea id="emailText-action" class="materialize-textarea" name="emailText"></textarea>
												<div class="row">
													<div class="input-field col s12">
														<textarea id="smsText-action" class="materialize-textarea" name="smsText"></textarea>
														<label for="smsText">SMS Texto</label>
													</div>
												</div>
												<div class="row">
													<div class="input-field col s12">
														<textarea id="whatsappText-action" class="materialize-textarea" name="whatsappText"></textarea>
														<label for="whatsappText">WhatsApp Texto</label>
													</div>
												</div>
												<div class="row">
													<div class="input-field col s6">
														<input id="deadline-action" type="number" class="required" name="deadline" min="0"> <label for="deadline" style="font-size: 20px">Intervalo de Envio</label>
													</div>

													<div class="input-field col s6">
														<select name="deadlineType" class="required" id="deadlineType-action">
															<c:forEach var="deadline" items="${deadlines}">
																<option value="${deadline}">${deadline.description}</option>
															</c:forEach>
														</select> <label style="font-size: 15px"></label>
													</div>
												</div>
												<table id="${tableId}">
												</table>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Modal Structure -->
				<div id="modal" class="modal">
					<div class="modal-content">
						<h4 id="modal-title"></h4>
						<p id="modal-content"></p>
					</div>
					<div class="modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a>
					</div>
				</div>
			</section>
		</div>
	</div>
	<!-- END MAIN -->
	<jsp:include page="../../../tiles/templates/footer.jsp"></jsp:include>
	<!-- SCRIPTS -->
	<jsp:include page="../../../tiles/templates/js.jsp"></jsp:include>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/data-tables/js/dataTables.buttons.js"></script>
	<!-- Include Editor JS files. -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/summernote/summernote-lite.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pages/${js}"></script>
</body>
</html>

