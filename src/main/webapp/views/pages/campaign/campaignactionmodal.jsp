<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>	
<div id="${modalId}" class="modal">
	<div class="modal-content">
		<h4>${modalTitle}</h4>
		<div class="row">
			<form class="col s12" id="${formId4}">
				<input type="hidden" name="id" id="id-action"> <input type="hidden" name="phaseId" id="phase-action">
				<div class="row">
					<div class="input-field col s12">
						<input id="seqNo-action" type="number" name="seqNo"> <label for="seqNo">Número de Sequência</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="emailSubject-action" type="text" name="emailSubject"> <label for="emailSubject">Email Assunto</label>
					</div>
				</div>
				<br>
				<h4 class="header2">Conteúdo do email</h4>
				<div class="row">
					<div class="input-field col s12">
						<textarea id="emailText-action" class="materialize-textarea" name="emailText"></textarea>
						<label for="emailText" ></label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<textarea id="smsText-action" class="materialize-textarea" name="smsText"></textarea>
						<label for="smsText">SMS Texto</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s12">
						<textarea id="whatsappText-action" class="materialize-textarea" name="whatsappText"></textarea>
						<label for="whatsappText">WhatsApp Texto</label>
					</div>
				</div>

				<div class="row">
					<div class="input-field col s6">
						<input id="deadline-action" type="number" class="required" name="deadline" min="0"> <label for="deadline" style="font-size: 20px">Intervalo de Envio</label>
					</div>

					<div class="input-field col s6">
						<select name="deadlineType" class="required" id="deadlineType-action">
							<c:forEach var="deadline" items="${deadlines}">
								<option value="${deadline}">${deadline.description}</option>
							</c:forEach>
						</select> <label style="font-size: 15px"></label>
					</div>
				</div>
				<table id="${tableId4}">
				</table>
			</form>
		</div>
	</div>
</div>