<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="../../tiles/templates/css.jsp"></jsp:include>
<jsp:include page="../../tiles/templates/header.jsp"></jsp:include>
<!-- INCLUDED PLUGIN CSS ON THIS PAGE -->

</head>
<body>
	<!-- body >> main >> wrapper >> content -->
	<!-- START MAIN -->
	<div id="main">
		<!-- START WRAPPER -->
		<div class="wrapper">
			<!-- START LEFT SIDEBAR NAV  MENU-->
			<jsp:include page="../../tiles/templates/menu.jsp"></jsp:include>
			<!-- END LEFT SIDEBAR NAV MENU-->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START CONTENT -->
			<section id="content">
				<div class="container">
					<div id="basic-form" class="section">
						<div class="row">
							<div class="col s12 m12 l12 ">
								<div class="card-panel">

									<div class="row row-header">
										<div class="col s12 m12 l12 ">
											<h5>${title}</h5>
											<div class="divider"></div>
										</div>
									</div>

									<div class="row">
										<form:form class="col s12" modelAttribute="obj" id="${formId}">

											<div class="row">
												<div class="toolbar col s12">
													<a class="btn btn-floating" href="${pageContext.request.contextPath}${uri}/list">
														<i class="material-icons">arrow_back</i>
													</a>
													<span class="divider"></span>
													<button class="btn btn-floating" type="submit">
														<i class="material-icons right">save</i>
													</button>
													<a class="btn btn-floating" href="${pageContext.request.contextPath}${uri}">
														<i class="material-icons">add</i>
													</a>
												</div>
											</div>

											<form:input type="hidden" name="id" id="id" path="id" />

											<div class="row">
												<div class="input-field col s12 l5">
													<label class="label-control active" for="bonus">Bonus *</label>
													<select class="select2 browser-default required" name="bonus" id="bonus" >
														<option value="" disabled selected>Not selected</option>
														<c:forEach items="${bonuss}" var="bonus">
															<option <c:if test='${not empty obj && obj.bonus.id == bonus.id}'> selected </c:if> value="${bonus.id}">${bonus.name}</option>
														</c:forEach>
													</select>
												</div>
												<div class="input-field col s12 l5">
													<label class="label-control active" for="beneficiary">Beneficiary *</label>
													<select class="select2 browser-default required" name="beneficiary" id="beneficiary" >
														<option value="" disabled selected>Not selected</option>
														<c:forEach items="${beneficiaries}" var="beneficiary">
															<option <c:if test='${not empty obj && obj.beneficiary == beneficiary}'> selected </c:if> value="${beneficiary}">${beneficiary.description}</option>
														</c:forEach>
													</select>
												</div>
												<div class="col s3 l2">
													<br>
													<input type="checkbox" class="checkbox" id="isactive" name="isactive" <c:if test="${obj.isactive}">checked</c:if> /> <label for="isactive">Active</label>
												</div>
											</div>

											<div class="row">
												<div class="input-field col s12 l6">
													<form:input id="level" class="required" type="number" name="level" path="level" />
													<label for="level">Level *</label>
												</div>
												<div class="input-field col s12 l6">
													<form:input id="rate" class="required" type="number" name="rate" path="rate" />
													<label for="rate">Rate *</label>
												</div>
											</div>

										</form:form>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
				<!--end container-->
			</section>
			<!-- END CONTENT -->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START RIGHT SIDEBAR NAV-->
			<!-- END RIGHT SIDEBAR NAV-->
		</div>
		<!-- END WRAPPER -->
	</div>
	<!-- END MAIN -->
	<!-- //////////////////////////////////////////////////////////////////////////// -->
	<jsp:include page="../../tiles/templates/footer.jsp"></jsp:include>
	<!-- SCRIPTS -->
	<jsp:include page="../../tiles/templates/js.jsp"></jsp:include>

	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/data-tables/js/dataTables.buttons.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pages/${js}"></script>

</body>
</html>
