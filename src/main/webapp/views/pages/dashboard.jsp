<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="../tiles/templates/css.jsp"></jsp:include>
<jsp:include page="../tiles/templates/header.jsp"></jsp:include>

<!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
<link href="${pageContext.request.contextPath}/resources/vendors/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/vendors/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/vendors/flag-icon/css/flag-icon.min.css" type="text/css" rel="stylesheet">

</head>
<body>
	<!-- body >> main >> wrapper >> content -->
	<!-- START MAIN -->
	<div id="main">
		<!-- START WRAPPER -->
		<div class="wrapper">

			<jsp:include page="../tiles/templates/menu.jsp"></jsp:include>

			<section id="content">
				<!--start container-->
				<div class="container">
					<div class="row">
						<div class="col s12">
							<div id="weekly-earning" class="card">
								<div class="card-content">
									<h4 class="header mt-0">Bem Vindo!</h4>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--end container-->
			</section>

		</div>
		<!-- END WRAPPER -->
	</div>
	<!-- END MAIN -->

	<jsp:include page="../tiles/templates/footer.jsp"></jsp:include>
	<jsp:include page="../tiles/templates/js.jsp"></jsp:include>

	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pages/dashboard.js"></script>
</body>
</html>