<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
<jsp:include page="../../tiles/templates/css.jsp"></jsp:include>
<jsp:include page="../../tiles/templates/header.jsp"></jsp:include>
<!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
<meta http-equiv="Refresh" content="${timeRedirect}; url=${redirectUrl}">
</head>
<body>
	<!-- body >> main >> wrapper >> content -->
	<!-- START MAIN -->
	<div id="main">
		<!-- START WRAPPER -->
		<div class="wrapper">
			<!-- START LEFT SIDEBAR NAV  MENU-->
			<jsp:include page="../../tiles/templates/menu.jsp"></jsp:include>
			<!-- END LEFT SIDEBAR NAV MENU-->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START CONTENT -->
			<section id="content">
				<div class="container">
					<div id="basic-form" class="section">
						<div class="row">
							<div class="col s12 m12 l12 ">
								<div class="card-panel">

									<div class="row row-header">
										<div class="col s12 m12 l12 ">
											<h5>Redirecionando...</h5>
										</div>
									</div>

								</div>
							</div>

						</div>
					</div>
				</div>
				<!--end container-->
			</section>
			<!-- END CONTENT -->
			<!-- //////////////////////////////////////////////////////////////////////////// -->
			<!-- START RIGHT SIDEBAR NAV-->
			<!-- END RIGHT SIDEBAR NAV-->
		</div>
		<!-- END WRAPPER -->
	</div>
	<!-- END MAIN -->
	<!-- //////////////////////////////////////////////////////////////////////////// -->
	<jsp:include page="../../tiles/templates/footer.jsp"></jsp:include>
	<!-- SCRIPTS -->
	<jsp:include page="../../tiles/templates/js.jsp"></jsp:include>

	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/data-tables/js/dataTables.buttons.js"></script>

</body>
</html>
