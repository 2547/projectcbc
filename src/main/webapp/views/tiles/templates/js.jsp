
<script>
	var contextPath = '${pageContext.request.contextPath}';
	var uri = '${uri}';
	var tableId = '#${tableId}';
	var formId = '#${formId}';
	var mi = '#${mi}';

	function resetForm() {
		window.location.href = contextPath + uri;
	}

</script>


<!-- ================================================
    Scripts
    ================================================ -->
<!-- jQuery Library -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/jquery-3.2.1.min.js"></script>

<!--materialize js-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/materialize.min2.js"></script>

<!--prism-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/prism/prism.js"></script>

<!--scrollbar-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>

<!-- chartjs -->
<%-- <script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/chartjs/chart.min.js"></script> --%>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>

<!--plugins.js - Some Specific JS codes for Plugin Settings-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>

<!--custom-script.js - Add your own theme custom JS-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/custom-script.js"></script>

<!--serialize to object-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/serialize-object/serialize-object.js"></script>

<!-- jquery mask -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/jquery-mask/jquery.mask.js"></script>

<!--  datatable plugin jquery -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/data-tables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/data-tables/js/dataTables.select.min.js"></script>


<!--validator-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/validator/jquery.validate.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/validator/additional-methods.min.js"></script>
<%-- <script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/validator/translations/pt-br.js"></script> --%>

<!--sweetalert-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/vendors/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/pages/menu.js"></script>

<!-- select2 -->
<script src="${pageContext.request.contextPath}/resources/vendors/select2/select2.full.min.js"></script>
