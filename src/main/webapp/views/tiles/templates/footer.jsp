
<!-- START FOOTER -->
<footer class="page-footer">
	<div class="footer-copyright">
		<div class="container">
			<span>Copyright � <script type="text/javascript">
				document.write(new Date().getFullYear());
			</script> <a class="grey-text text-lighten-4" href="http://adaptaconsultoria.com.br" target="_blank">Adapta Consultoria</a> Todos os direitos reservados.
			</span> <span class="right hide-on-small-only"> Criado por <a class="grey-text text-lighten-4" href="https://pixinvent.com/">ADAPTA CONSULTORIA</a></span>
		</div>
	</div>
</footer>
<!-- END FOOTER -->