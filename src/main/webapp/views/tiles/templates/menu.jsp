<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- START LEFT SIDEBAR NAV-->
<aside id="left-sidebar-nav" class="nav-expanded nav-lock nav-collapsible">
	<div class="brand-sidebar">
		<h1 class="logo-wrapper">
			<a href="${pageContext.request.contextPath}/dashboard" class="brand-logo darken-1"> <img src="${pageContext.request.contextPath}/resources/images/logo/materialize-logo.png" alt="materialize logo"> <span
				class="logo-text hide-on-med-and-down">Materialize</span>
			</a>
			<a href="#" class="navbar-toggler">
				<i class="material-icons">radio_button_checked</i>
			</a>
		</h1>
	</div>
	<ul id="slide-out" class="side-nav fixed leftside-navigation">
		<li class="no-padding">
			<ul class="collapsible" data-collapsible="accordion">
				<li class="bold" id="dashboard-menu">
					<a href="${pageContext.request.contextPath}/dashboard" class="waves-effect waves-cyan">
						<i class="material-icons">dashboard</i>
						<span class="nav-text">Home</span>
					</a>
				</li>
				<li class="bold" id="partner-menu">
					<a href="${pageContext.request.contextPath}/partner/list" class="waves-effect waves-cyan">
						<i class="material-icons">person</i>
						<span class="nav-text">Partner</span>
					</a>
				</li>
				<li class="bold" id="partnercategory-menu">
					<a href="${pageContext.request.contextPath}/partnercategory/list" class="waves-effect waves-cyan">
						<i class="material-icons">bookmark</i>
						<span class="nav-text">Partner Category</span>
					</a>
				</li>
				<li class="bold" id="seller-menu">
					<a href="${pageContext.request.contextPath}/seller/list" class="waves-effect waves-cyan">
						<i class="material-icons">person_outline</i>
						<span class="nav-text">Seller</span>
					</a>
				</li>
				<li class="bold" id="sellercategory-menu">
					<a href="${pageContext.request.contextPath}/sellercategory/list" class="waves-effect waves-cyan">
						<i class="material-icons">bookmark_border</i>
						<span class="nav-text">Seller Category</span>
					</a>
				</li>
				<li class="bold" id="entity-menu">
					<a href="${pageContext.request.contextPath}/entity/list" class="waves-effect waves-cyan">
						<i class="material-icons">business</i>
						<span class="nav-text">Entity</span>
					</a>
				</li>
				<li class="bold" id="accountaccess-menu">
					<a href="${pageContext.request.contextPath}/accountaccess" class="waves-effect waves-cyan">
						<i class="material-icons">verified_user</i>
						<span class="nav-text">Account Access</span>
					</a>
				</li>
				<li class="bold" id="payout-menu">
					<a href="${pageContext.request.contextPath}/payout/list" class="waves-effect waves-cyan">
						<i class="material-icons">attach_money</i>
						<span class="nav-text">Payout</span>
					</a>
				</li>
				<li class="bold" id="paymentmethod-menu">
					<a href="${pageContext.request.contextPath}/paymentmethod/list" class="waves-effect waves-cyan">
						<i class="material-icons">credit_card</i>
						<span class="nav-text">Payment Method</span>
					</a>
				</li>
				<li class="bold" id="entity-menu">
					<a href="${pageContext.request.contextPath}/currency/list" class="waves-effect waves-cyan">
						<i class="material-icons">attach_money</i>
						<span class="nav-text">Currency</span>
					</a>
				</li>
				<li class="bold" id="entity-menu">
					<a href="${pageContext.request.contextPath}/bonus/list" class="waves-effect waves-cyan">
						<i class="material-icons">card_membership</i>
						<span class="nav-text">Bonus</span>
					</a>
				</li>
				<li class="bold" id="cashbackrule-menu">
					<a href="${pageContext.request.contextPath}/cashbackrule/list" class="waves-effect waves-cyan">
						<i class="material-icons">monetization_on</i>
						<span class="nav-text">Cashback Rule</span>
					</a>
				</li>
				<li class="bold" id="payout-event-menu">
					<a href="${pageContext.request.contextPath}/payoutevent" class="waves-effect waves-cyan">
						<i class="material-icons">money_off</i>
						<span class="nav-text">Payout Event</span>
					</a>
				</li>
			</ul>
		</li>
	</ul>
	<a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only"> <i
		class="material-icons">menu</i>
	</a>
</aside>
<script> 
	var id = "${menuId}"; 
	var d = document.getElementById(id);
	d.className += ' active';
</script>
<!-- END LEFT SIDEBAR NAV-->
