var table;
var viewedAndCapturedChart;
var campaignId;
var	isFirstClickOnChartPoint;
var resetChart = $("#reset-chart");
var titleChart = $("#title-chart");


$(document).ready(function() {
	initializeTable();
	cSelected()
	onClickResetChart();
});

function cSelected() {
	$(".cselected").click(function() {
		$(".non-selected-campaign-alert").addClass("hide")
		openTable($(this).attr("id"))
		campaignId =  $(this).attr("id");
		loadLineChartsCapturedUsersByCampaignId(campaignId)
		loadPieChartByCampaignId(campaignId)
		$("#campaignHtml").html( $(this).html() )
	});
}

function resetAllCampaignData() {
	if( viewedAndCapturedChart !== undefined) {
		viewedAndCapturedChart.destroy();
		$("#viewed").remove()
		$("#captured").remove()
		$("#chart-div").html('<canvas id="viewed" class="firstShadow" height="350"></canvas> <canvas id="captured" height="350"></canvas>');
	}
}

function onClickResetChart() {
	
	resetChart.click(function() {
		if (!resetChart.hasClass('disabled')) {
			isFirstClickOnChartPoint = true;
			resetChart.addClass('disabled')
			titleChart.html("LEADS CAPTURADOS/DIA")
			loadLineChartsCapturedUsersByCampaignId( campaignId )
		}
	})
}

function initializeTable() {
	table = $('#lead-table').DataTable({
		language : {
			"url" : "resources/vendors/data-tables/json/Portuguese-Brasil.json"
		},
		columns : [ {
			data : "id"
		}, {
			data : "name",
		}, {
			data : "email"
		}, {
			data : "phone"
		} ],
		dom : 'Bfrtip',
		autoWidth: false,
		initComplete: function(settings, json) {
			
	    },
		searching: false,
		columnDefs : [ {
			targets : [ 0 ],
			visible : false,
		}, { 
			className: "dt-center", 
			targets: [] 
		}]
	})
}

function openTable(id) {
	
	$.ajax({
		url : "getleads",
		type: "post",
		data : {
			"campaignId" : id
		},
		success : function(obj) {
			table.clear().rows.add(obj).draw();
		}
	})
}

function loadPieChartByCampaignId(id) {
	
	$.ajax({
		url : "getpiechartbycampaignid",
		type: "post",
		data : {
			"campaignId" : id
		},
		success : function(obj) {
			
			var labelArray = [];
			var dataArray = [];
			obj.forEach(o => {
				labelArray.push(o.name)
			    dataArray.push(o.quantity)
			});
			
			buildPieChart(labelArray, dataArray);
		}
	})
	return 0;
}

function buildPieChart(labelArray, dataArray) {
	
	new Chart(document.getElementById("pie-chart"), {
	    type: 'pie',
	    data: {
	      labels: labelArray,
	      datasets: [{
	        label: "Population (millions)",
	        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
	        data: dataArray
	      }]
	    },
	    options: {
	      title: {
	        display: true,
	        text: 'Contagem de acessos em cada fase'
	      }
	    }
	});
}

function loadLineChartsCapturedUsersByCampaignId(id) {
	
	$.ajax({
		url : "getlinechartscapturedusersbycampaignid",
		type: "post",
		data : {
			"campaignId" : id
		},
		success : function(obj) {
			isFirstClickOnChartPoint = true;
			
			var daysArray = [];
			var viewedArray = [];
			var capturedArray = [];
			
			obj.forEach(o => {
			   daysArray.push(o.day)
			   viewedArray.push(o.viewed)
			   capturedArray.push(o.captured)
			});
			
			initializeChart(daysArray, viewedArray, capturedArray);
		}
	})
	return 0;
}

function loadLineChartsCapturedUsersByCampaignIdAndDate(id, date) {
	
	$.ajax({
		url : "getlinechartscapturedusersbycampaignidanddate",
		type: "post",
		data : {
			"campaignId" : id,
			"date" : date,
		},
		success : function(obj) {
			
			isFirstClickOnChartPoint = false;
			resetChart.removeClass('disabled')
			titleChart.html("LEADS CAPTURADOS/HORA")
			var hoursArray = [];
			var viewedArray = [];
			var capturedArray = [];
			
			obj.forEach(o => {
			   hoursArray.push(o.hour)
			   viewedArray.push(o.viewed)
			   capturedArray.push(o.captured)
			});
			initializeChart(hoursArray, viewedArray, capturedArray);
		}
	})
	return 0;
}

function initializeChart(label, date1, date2) {
	
	if( viewedAndCapturedChart !== undefined) {
		viewedAndCapturedChart.destroy();
		$("#viewed").remove()
		$("#captured").remove()
		$("#chart-div").html('<canvas id="viewed" class="firstShadow" height="350"></canvas> <canvas id="captured" height="350"></canvas>');
	}
	
	var viewedCTX = document.getElementById("viewed").getContext("2d");
	
	var viewedDataAndCapturedData = {
	  labels: label,
	  datasets: [{
		    label: "Total",
		    borderColor: "#8e5ea2",
		    fill: false,
		    data: date1
	  }, {
		    label: "Capturados",
		    borderColor: "#3cba9f",
		    fill: false,
		    data: date2
	  }]
	};
	
	buildViewedAndCapturedChart(viewedCTX, viewedDataAndCapturedData)
	return 0;
}

function buildViewedAndCapturedChart(viewedCTX, viewedDataAndCapturedData) {
	
	viewedAndCapturedChart = new Chart(viewedCTX, {
	    type: 'line',
	    data: viewedDataAndCapturedData,
	    options: {
		      title: {
		        display: true,
		        text: 'Contagem que totaliza todos os acessos gerais e acessos que foram capturados'
		      }
		}
	});
	getClickedPointOnChart()
}

function getClickedPointOnChart() {
	viewedAndCapturedChart.canvas.onclick = function(evt){
	    var activePoint = viewedAndCapturedChart.getElementsAtEvent(evt)[0];
	    var label;
	    var value;
	    if (activePoint !== undefined) {
	        var dataset = viewedAndCapturedChart.data.datasets[activePoint._datasetIndex];
	        label = viewedAndCapturedChart.data.labels[activePoint._index];
	        value = dataset.data[activePoint._index];
	        
	        if (isFirstClickOnChartPoint) {
				 loadLineChartsCapturedUsersByCampaignIdAndDate(campaignId, label)
			}
	    }
	};
}

function updateChart(chart, label, data) {
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });
    chart.update();
}






