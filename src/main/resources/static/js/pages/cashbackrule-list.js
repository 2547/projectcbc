var table;
var len = 10;

$(document).ready(function() {
	openTable()
})

function openTable() {
	table() 
}

function table() {
	
    table = $(tableId)
        .DataTable({
        	ajax: {
				type: "GET",
				url: contextPath + uri + "/getlist",
				dataSrc:""
			},
			language: {
					"sLengthMenu": ""
			},
            columns: getColumns(),
            buttons: getButtons(),
            columnDefs: getColumnDefs(),
            initComplete: function(settings, json) {
                selectTableConfig(this.DataTable());
            },
            dom: 'lBfrtip',
            select: true,
            order: [
                [1, 'asc']
            ]
        });
}

function selectTableConfig(table) {
	genericTableId = getTableId(table);
	
	table.on( 'select deselect', function () {
		 selectDeselectTable(table)
	});
	
	$(genericTableId + ' tbody').on(
			'dblclick',
			'tr',
			function() {
				table.rows(this).select()
				var obj = table.row({
					selected : true
				}).data();
				window.location.href = contextPath + uri + "?id=" + obj.id;
			});
	
	$(genericTableId).on( 'length.dt', function ( e, settings, newlen ) {
	    len = newlen;
	    table.ajax.reload();
	} );
}

function getTableId(table) {
	return '#'+table.tables().nodes().to$().attr('id');
}

function getLen() {
	return len;
}

function selectDeselectTable(table) {
	var selectedRows = table.rows( { selected: true } ).count();
	table.buttons([1,2]).enable( selectedRows == 1 );
   
}

function promptRemove(id) {
	swal({
	    title: "Remove",
	    text: "TemAre you sure you want to remove this record?",
	    icon: "warning",
	    buttons: {
            cancel: {
                text: "No",
                value: null,
                visible: true,
                className: "",
                closeModal: true,
            },
            confirm: {
                text: "Yes",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
	    }
	})
	.then((isConfirm) => {
	    if (isConfirm) {
	    	remove(id)
	    } else {
	    	swal("Canceled", "Record removed!", "error");
	    }
	});
}

function remove(id) {
	$.ajax({
		type: "DELETE",
		url : contextPath + uri + "/" + id,
		success : function(obj) {
			if (obj === true) {
				 table.row({
	                 selected : true
	               }).remove();
				 table.draw();
				swal("Removed!", "Record removed.", "success");
			} else {
				console.log(obj);
				swal("Error to Remove!", "Sorry, but it was not possible to remove this record!", "error");
			}
		}
	})
}

function getButtons() {
	return  [
        {
            text: '<i class="material-icons left">add</i>',
            className : 'btn btn-floating',
            action: function ( e, dt, node, config ) {
				window.location.href = contextPath + uri;
            },
            enabled: true
        },
        {
            text: '<i class="material-icons left">edit</i>',
            className : 'btn btn-floating',
            action: function ( e, dt, node, config ) {
            	var obj = dt.row( { selected: true } ).data();
				window.location.href = contextPath + uri + "?id=" + obj.id;
            },
            enabled: false
        },
        {
            text: '<i class="material-icons left ">clear</i>',
            className : 'btn btn-floating red',
            action: function ( e, dt, node, config ) {
            	var o = dt.row( { selected: true } ).data();
            	promptRemove(o.id);
            },
            enabled: false
        }
    ];
}

function getColumns() {
	 return [{
		         title: "id",
		         data: "id"
			 }, { 
				 title: "Bonus",
				 data: "bonus",
				 render: function ( data, type, full ) {
					 if (full.bonus != undefined || full.bonus != null)
						 return full.bonus.name;
					 else return '';
				 }
			 }, { 
				 title: "Beneficiary",
				 data: "beneficiary",
				 render: function ( data, type, full ) {
					 if (full.beneficiary == undefined || full.beneficiary == null)
						 return '';

					 if (full.beneficiary == 'C')
						 return 'Customer';

					 if (full.beneficiary == 'S')
						 return 'Sponsor';

					 if (full.beneficiary == 'L')
						 return 'Leader';

					 if (full.beneficiary == 'P')
						 return 'Platform';

					 return '';
				 }
		     }, {
		         title: "Level",
		         data: "level"
		     }, {
		         title: "Rate",
		         data: "rate"
	     }];
}

function getColumnDefs() {
	return [{
        targets: [0],
        visible: false
    }];
}
