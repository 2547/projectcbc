
$(document).ready(function() {
	validate();
	select2Initializer();
});

function save() {
	$("#isactive").val( Boolean( $("#isactive").prop("checked") ) );
	$.ajax({
		type : "POST",
		data : $(formId).serializeObject(),
		url : contextPath + uri,
		success : function(obj) {
			if (obj.sucesso) {
				if (!obj.isEditing) {
					swal({
					    title: obj.message,
					    text: "Would like to keep registering?",
					    icon: "success",
					    buttons: {
			                cancel: {
			                    text: "List",
			                    value: null,
			                    visible: true,
			                    className: "",
			                    closeModal: true,
			                },
			                confirm: {
			                    text: "Yes",
			                    value: true,
			                    visible: true,
			                    className: "",
			                    closeModal: true
			                }
					    }
					})
					.then((isConfirm) => {
					    if (isConfirm) {
					    	resetForm()
					    } else {
					    	window.location.href = contextPath + uri + "/list";
					    }
					});
					
				} else {
					$("#version").val(obj.obj.version);
					swal({
					    title: obj.message,
					    text: "Continue to edit?",
					    icon: "success",
					    buttons: {
			                cancel: {
			                    text: "Exit",
			                    value: null,
			                    visible: true,
			                    className: "",
			                    closeModal: true,
			                },
			                confirm: {
			                    text: "Continue",
			                    value: true,
			                    visible: true,
			                    className: "",
			                    closeModal: true
			                }
					    }
					})
					.then((isConfirm) => {
					    if (isConfirm) {
					    	
					    } else {
					    	window.location.href = contextPath + uri;
					    }
					});
					
				}
			} else {
				swal("Canceled", obj.message, "error");
				return 0;
			}
		}
	})
	return false;
}

function validate() {
	$(formId).validate({
		submitHandler : function(form) {
			save();
		},
		rules: {},
		errorElement : 'div',
		errorPlacement : function(error, element) {
			var placement = $(element).data('error');
			if (placement) {
				$(placement).append(error)
			} else {
				if ($(element)[0].tagName === 'SELECT') {
					$(element).parent().append(error);
				} else {
					error.insertAfter(element);
				}
			}
		}
	});

}

function select2Initializer() {
	$('#paymentMethod').select2();
	$('#remittanceSequence').select2();

	$('#partner').select2({
		data: [{
			id: -1,
			text: "None"
		}],
//		minimumInputLength: 3,
		ajax: {
			url: contextPath + "/autocompete/partner",
			dataType: 'json',
			data: function (params) {
				var query = {
					query: (params.term ? params.term : null),
				}
				return query;
			},
			processResults: function (data) {
				var result = [{
					id: -1,
					text: "None"
				}];
				result.push.apply(result,data);
				return {
					results: result
				};
			}
		}
	});

}
