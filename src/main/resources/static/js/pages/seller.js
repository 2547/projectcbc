var table;
var rowIndex;
var saveUrl = contextPath + uri;
var sellerPercentageForm = $('#seller-percentage-form');
var loader = $("#loader-generic");
var url = contextPath + "/seller";
var listUrl = contextPath + "/seller/list";
var getRulesUrl = contextPath + uri + "/getrules";

var pcCashback = $("#pcCashback");
var description = $("#rule-description");
var currency = $("#currency");
var id = 0;
var currencyName;
var sellerId;
var ruleModal = $('.modal').modal();

var isEditingRules = false;

var addPercentageButton = $('#add-percentage')

var currentRules;


$(document).ready(function() {
	$('select').material_select();
	sellerId = getParamUrl().id;
	validate();
	select2Initializer();
	table();
	validateCustomMethod()
	addPercentageClick()
});
// rules register begin
// table begin
function addPercentageClick() {
	addPercentageButton.click(() => {
		if (!isEditingRules) {
			addPercentage()
		} else {
			edition(currentRules)
		}
	})
}

function table() {
    table = $(tableId).DataTable({
            ajax: {
                type: "GET",
                data: {
                	'sellerId': sellerId
                },
                url: getRulesUrl,
                dataSrc: ""
            },
            language: {
                "sLengthMenu": ""
            },
            columns: getColumns(),
            buttons: getButtons(),
            columnDefs: getColumnDefs(),
            initComplete: function(settings, json) {
                selectTableConfig(this.DataTable());
            },
            searching: false,
            dom: 'lBfrtip',
            select: true,
            order: [
                [1, 'asc']
            ]
        });
}

function getButtons() {
    return [{
        text: '<i class="material-icons left">add</i></a>',
        className: 'btn btn-floating',
        action: function(e, dt, node, config) {
        	clearForm()
            isEditingRules = false;
            currency.val('').trigger('change')
            ruleModal.modal('open');
        },
        enabled: true
    }, {
        text: '<i class="material-icons left">edit</i></a>',
        className: 'btn btn-floating',
        attr: {
            'data-position': 'top',
            'data-delay': '50',
            'data-tooltip': 'Limpar Formulário'
        },
        action: function(e, dt, node, config) {
        	
            var obj = dt.row({
                selected: true
            }).data();
            
            rowIndex = dt.row({
                selected: true
            }).index()
            
            isEditingRules = true;
            tableClick(obj)
            currentRules = obj;
            ruleModal.modal('open');
        	$('#seller-percentage-form .input-field label').addClass('active');
        },
        enabled: false
    }, {
        text: '<i class="material-icons left ">clear</i>',
        className: 'btn btn-floating red',
        action: function(e, dt, node, config) {
            var o = dt.row({
                selected: true
            }).data();
            remove(o).then(r => {
            	selectDeselectTable(table)
            })
        },
        enabled: false
    }];
}

function getColumnDefs() {
	return [{
        targets: [0],
        visible: false
    }, {
        className: "dt-center",
        "targets": []
    }];
}

function getColumns() {
	 return [ { 
		 		data: "id" 
		 	},{ 
		 		title: "Descrição",
		 		data: "description" 
		 	},{ 
		 		title: "Moeda",
		 		data: "currency",
	 			render: function(data, type, full) {
	            	return data != undefined ? data.name : currencyName; 
	            }
		 	}, { 
		 		title: "Percentual %",
		 		data: "pcCashback" 
		 	}
		  ];
}

function selectDeselectTable(table) {
	var selectedRows = table.rows( { selected: true } ).count();
    table.buttons( [1, 2] ).enable( selectedRows > 0 );
}

function selectTableConfig(table) {
	table.on( 'select', function () {
		selectDeselectTable(table)
	});
	
	table.on( 'deselect', function () {
		clearForm();
		$('#currency').val('').trigger('change');
		selectDeselectTable(table)
	});
	
	$(tableId + ' tbody').on( 'dblclick', 'tr', function() {
		table.rows(this).select()
		rowIndex = table.row( this ).index();
		var obj = table.row({
			selected : true
		}).data();
		isEditingRules = true;
		currentRules = obj;
		ruleModal.modal('open');
		tableClick(obj)
    	$('#seller-percentage-form .input-field label').addClass('active');
	});
}

function tableClick(data) {
	obj = data.currency;
	data.id ? id = data.id : id = ''; 
	currency.val(data.currency)
	description.val(data.description)
	pcCashback.val(data.pcCashback)
	setValueSelect2(obj)
	return 0;
}

function remove(obj) {
	return new Promise((resolve, reject) => {
		var idPercentage = obj.id;
		if (rowIndex !== '' && rowIndex !== null && idPercentage !== '' && idPercentage !== null) {
			removePercentage(idPercentage, resolve);
		} else {
			table.row(rowIndex).remove().draw();
			clearForm()
			resolve(true)
		}
	});
}

function removePercentage(id, resolve) {
	$.ajax({
		type: "DELETE",
		url : contextPath + uri + "/removepercentage/" + id,
		success : function(obj) {
			if (obj === true) {
				 table.row({
	                 selected : true
	               }).remove();
				 table.draw();
				 resolve(true)
			} else {
				console.log(obj);
				swal("Error to Remove!", "Sorry, but it was not possible to remove this record!", "error");
			}
		}
	})
}

// table end
function edition(data) {
	currencyName = $("#currency option:selected").text();
	if (sellerPercentageForm.valid()) {
		table.row(rowIndex).data({
			"id": data.id,
			"description": description.val(),
			"currency": {
				id: currency.val(),
				name: currencyName
			},
			"pcCashback": pcCashback.val()
	    }).draw()
	    ruleModal.modal('close');
	}
}

function setValueSelect2(data) {
	var option = data.id;
	currency.val(option).trigger('change')
}

function addPercentage() {
	currencyName = $("#currency option:selected").text();
	if (sellerPercentageForm.valid()) {
		table.row.add( {
			"id": '',
			"description": description.val(),
			"currency": {
				id: currency.val(),
				name: currencyName
			},
	        "pcCashback": pcCashback.val()
	    } ).draw();
		ruleModal.modal('close');
		clearForm()
	}
	return 0;
}

function clearForm() {
	pcCashback.val("");
	description.val("");
}

function save() {
	loader.removeClass('hide');
	saveAwait()
}

function saveAwait() {
	$("#isactive").val( Boolean( $("#isactive").prop("checked") ) );
	$.ajax({
		type : "POST",
		data : $(formId).serializeObject(),
		url : contextPath + uri,
		success : function(obj) {
			saveItems(getItems(), obj.obj);
		}
	})
	return false;
}

function getItems() {
	var items = [];
	table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
		  var data = this.data();
		  items.push(data);
	} );
	
	var mappedItems = items.map(c => {
		return {
			id: c.id,
			description: c.description,
			pcCashback:  c.pcCashback,
			currency: {
				id: c.currency.id,
				name: c.currency.name
			}
		}
	});
	return {
		rules: mappedItems 
	};
}

function saveItems(mappedItems, obj) {
	let rules = mappedItems.rules;
	let truths = 0;
	let truthsRequired = table.data().count();
	
	let saveItemPromise = function() {
		return new Promise(resolve => {
			rules.forEach(r => {
				$.ajax({
					type : 'POST',
					data : {
						'id': r.id,
						'sellerId': obj.id, 
						'currency': r.currency.id,
						'description': r.description,
						'pcCashback':  r.pcCashback
					},
					url : contextPath + '/seller/postrules',
					success : function(obj) {
						if (obj) {
							truths++;
						}
						if (truths == truthsRequired) {
							resolve(true)
						} else {
							resolve(false)
						}
					}
				})
			})
		})
	}
	
	saveItemPromise().then(response => {
		loader.addClass('hide')
		alertDialog(obj)
	})
	
}

function alertDialog(obj) {
	swal({
        title: obj.message,
        text: "Continue to edit?",
        icon: "success",
        buttons: {
            cancel: {
                text: "Exit",
                value: null,
                visible: true,
                className: "",
                closeModal: true,
            },
            confirm: {
                text: "Continue",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    })
    .then((isConfirm) => {
        if (isConfirm) {
        	window.location.href = url;
        } else {
            window.location.href = listUrl;
        }
    });
}
// rules register
function validate() {
	
	$(formId).validate({
		submitHandler : function(form) {
			save();
		},
		ignore: [],
		rules: {
			rule: {
				percentages: true
			}
		},
		errorElement : 'div',
		errorPlacement : function(error, element) {
			var placement = $(element).data('error');
			if (placement) {
				$(placement).append(error)
			} else {
				if ($(element)[0].tagName === 'SELECT') {
					$(element).parent().append(error);
				} else {
					error.insertAfter(element);
				}
			}
		}
	});
	
	sellerPercentageForm.validate({
		rules: {
			pcCashback: {
				number: true
			}
		},
		errorElement : 'div',
		errorPlacement : function(error, element) {
			var placement = $(element).data('error');
			if (placement) {
				$(placement).append(error)
			} else {
				if ($(element)[0].tagName === 'SELECT') {
					$(element).parent().append(error);
				} else {
					error.insertAfter(element);
				}
			}
		}
	});
}

function validateCustomMethod() {
	jQuery.validator.addMethod("percentages", function(value, element) {
	    if ( table.data().count() > 0 ) return true;
	}, "Please add at least one rule.");
}

function select2Initializer() {
	$('#sellerCategory').select2();
	$('#currency').select2();

	$('#leader').select2({
		data: [{
			id: -1,
			text: "None"
		}],
// minimumInputLength: 3,
		ajax: {
			url: contextPath + "/autocompete/leader",
			dataType: 'json',
			data: function (params) {
				var query = {
					query: (params.term ? params.term : null),
				}
				return query;
			},
			processResults: function (data) {
				var result = [{
					id: -1,
					text: "None"
				}];
				result.push.apply(result,data);
				return {
					results: result
				};
			}
		}
	});

	$('#country').select2({
		data: [{
			id: -1,
			text: "None"
		}],
// minimumInputLength: 3,
		ajax: {
			url: contextPath + "/autocompete/country",
			dataType: 'json',
			data: function (params) {
				var query = {
					query: (params.term ? params.term : null),
				}
				return query;
			},
			processResults: function (data) {
				var result = [{
					id: -1,
					text: "None"
				}];
				result.push.apply(result,data);
				return {
					results: result
				};
			}
		}
	});

	$('#region').select2({
		data: [{
			id: -1,
			text: "None"
		}],
// minimumInputLength: 3,
		ajax: {
			url: contextPath + "/autocompete/region",
			dataType: 'json',
			data: function (params) {
				var query = {
					query: (params.term ? params.term : null),
					country: ($('#country').val() ? $('#country').val() : null)
				}
				return query;
			},
			processResults: function (data) {
				var result = [{
					id: -1,
					text: "None"
				}];
				result.push.apply(result,data);
				return {
					results: result
				};
			}
		}
	});

	$('#city').select2({
		data: [{
			id: -1,
			text: "None"
		}],
// minimumInputLength: 3,
		ajax: {
			url: contextPath + "/autocompete/city",
			dataType: 'json',
			data: function (params) {
				var query = {
					query: (params.term ? params.term : null),
					region: ($('#region').val() ? $('#region').val() : null),
					country: ($('#country').val() ? $('#country').val() : null)
				}
				return query;
			},
			processResults: function (data) {
				var result = [{
					id: -1,
					text: "None"
				}];
				result.push.apply(result,data);
				return {
					results: result
				};
			}
		}
	});

}

function getParamUrl() {
	var queries = {};
	$.each(document.location.search.substr(1).split('&'), function(c, q) {
		var i = q.split('=');
		try {
			queries[i[0].toString()] = i[1].toString();
		}catch {
			return null;
		}
	});
	return queries;
}
