var descTempt;
var table;
var phaseId;

$(document).ready(function() {
	validator();
	phaseId = getParamUrl().phaseId;
	openTable();
	mask();
	initializeEditor();
	initializeModal();
});

function initializeModal() {
	$('.modal').modal();
}

function initializeEditor() {
	$('#emailText-action').summernote({
		placeholder: 'Digite algo',
        tabsize: 2,
        height: 400
	});
}

function mask() {
	$('.phone').mask('(00) 00000-0000');
}

function openTable(id) {
	if (table != undefined && table != '')
		table.destroy();
	
	$.ajax({
		url : "getactions",
		data : {
			"phaseId" : phaseId
		},
		success : function(obj) {
			console.log(obj)
			table = $('#action-table').DataTable({
				data : obj,
				language : {
					"url" : "resources/vendors/data-tables/json/Portuguese-Brasil.json"
				},
				buttons: [ {
						
		                text: '<i class="material-icons left">arrow_back</i></a>',
		                className : 'btn-floating waves-effect waves-light cyan tooltipped',
		                attr:  {
		                	 'data-position':'top', 
		                	 'data-delay':'50',
		                	 'data-tooltip': 'Voltar'
		                },
		                action: function ( e, dt, node, config ) {
		                	window.location.href = "campaignregister?id="+getParamUrl().campaignId;
		                },
		                enabled: true
		            },{
					
		                text: '<i class="material-icons left">clear_all</i></a>',
		                className : 'btn-floating waves-effect waves-light purple lightrn-1 tooltipped',
		                attr:  {
		                	 'data-position':'top', 
		                	 'data-delay':'50',
		                	 'data-tooltip': 'Limpar Formulário'
		                },
		                action: function ( e, dt, node, config ) {
		                	 clearActionFields()
		                },
		                enabled: false
		            },{
		                text: '<i class="material-icons left ">clear</i></a>',
		                className : 'btn-floating waves-effect waves-light red tooltipped',
		                attr:  {
		                	 'data-position':'top', 
		                	 'data-delay':'50',
		                	 'data-tooltip': 'Remover Registro'
		                },
		                action: function ( e, dt, node, config ) {
		                	var obj = dt.row( { selected: true } ).data();
		                	actionRemove(obj.id)
		                },
		                enabled: false
		            },
		            {
		                text: '<i class="material-icons right">send</i>',
		                className : 'btn waves-effect waves-light cyan right',
		                action: function ( e, dt, node, config ) {
		                	var obj = dt.row( { selected: true } ).data();
		                	save()
		                },
		                enabled: true
		            },
		        ],
		        select : 'single',
		        select : {
					style : 'os',
					selector : 'td:first-child'
				},
				columns : [	
								{ data: "id" }, {
									data : null, "defaultContent": '', 'title': ''
								}, { 
										data: "phase",
										mRender: function(data, type, full) {
				    					return full.phase.id;
				    				}
								},
								{ data: "seqNo", "title": "Nro Seq", "width": "5px"}, {
									data: null, 
									title: "Email",
									defaultContent: "<a id=\"email-action\" class=\"btn-flat waves-effect\"> "+
													"	<i class=\"fas fa-envelope\"></i> "+ 
													"Abrir</a>" 
								}, { 
									data: null, 
									title: "SMS",
									defaultContent: "<a id=\"sms-action\" class=\"btn-flat waves-effect center\"> "+
													"	<i class=\"fas fa-sms\"></i> "+ 
													"Abrir</a>"
								}, { 
									data: null, 
									title: "WhatsApp",
									defaultContent: "<a id=\"whatsapp-action\" class=\"btn-flat waves-effect center\"> "+
													"	<i class=\"fab fa-whatsapp\"></i> "+ 
													"Abrir</a>" 
								}, { 
									data: "deadline",
									"title": "Prazo"
								},
								{ data: "deadlineType",  "title": "Tipo Prazo"}
				],
				dom : 'Bfrtip',
				autoWidth: false,
				initComplete: function(settings, json) {
					selectTableConfig(this.DataTable());
			    	actionButtons();
			    },
				searching: false,
				columnDefs : [ {
					targets : [ 0, 2],
					visible : false,
				}, { 
					className: "dt-center", 
					targets: [ 3, 4, 5, 6, 7, 8 ] 
				}, {
					orderable : false,
					className : 'select-checkbox',
					targets : 1
				}]
			})
		}
	})
}

function save() {
	$("#phase-action").val( phaseId );
    if ( $("#action-form").valid() ) {
        $.ajax({
            type: "POST",
            data: $("#action-form").serializeObject(),
            url: "actions",
            success: function (obj) {
                if (obj != false) {
                	table.clear().rows.add(obj).draw();
                    swal("Sucesso!", "O registro foi salvo!", "success");
                    clearActionFields()
                } else {
                    swal("Cancelado", obj.toString(), "error");
                    return 0;
                }
            }
        })
    }
    return false;
}

function actionRemove(id) {
	swal({
		title: "Você tem certeza que deseja cancelar?",
		text: "Não será possível recuperar esse registro após o cancelamento!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: '#DD6B55',
		confirmButtonText: 'Sim!',
		cancelButtonText: "Não!",
		closeOnConfirm: false,
		closeOnCancel: false
	},
	function(isConfirm) {
		if (isConfirm) {
			$.ajax({
				type: "POST",
				data: {
						"id": id
				},
				url: "actionremove",
				success: function(obj) {
					if (obj) {
						 table.row({
			                 selected : true
			               }).remove();
						 table.draw();
						swal("Removido!", "O registro foi removido!", "success");
						clearActionFields()
						selectDeselectTable(table)
					} 
					else {
						swal("Cancelado", "O registro não pode ser removido!", "error");
					} 
				}})
		}
		else
			swal("Cancelado", "O registro não foi removido!", "error");
	});
}

function selectTableConfig(table) {
	table.on( 'select deselect', function () {
		 selectDeselectTable(table)
	});
	$('.tooltipped').tooltip({delay: 50});
	
	$('#' +table.tables().nodes().to$().attr('id')+ ' tbody').on('dblclick', 'tr', function () {
		table.rows('.selected').deselect();
		table.rows(this).select()
	} );
}

function selectDeselectTable(table) {
    var selectedRows = table.rows( { selected: true } ).count();
// table.button( 1 ).enable( selectedRows === 1 );
    table.button( 1 ).enable( selectedRows > 0 );
    table.button( 2 ).enable( selectedRows > 0 );
    
    var obj = table.row( { selected: true } ).data();
	var tableId = table.tables().nodes().to$().attr('id')

    if (selectedRows > 0) {
    	switch (tableId) {
    		case 'action-table' : 
    			tableDoubleClick(obj)
    		break;
    		default: return 0;
    	}
    } else {
    	console.log('hi')
    	
    	switch (tableId) {
			case 'action-table' : 
				 clearActionFields()
			break;
			default: return 0;
    	}
    }
}

function actionButtons() {
	 $('#action-table tbody').on( 'click', 'a', function (event) {
		 	event.preventDefault()
	        var data = table.row( $(this).parents('tr') ).data();
		 	switch ( $(this).attr("id") ) {
		 		case 'email-action':
		 			openEmailAction(data);
		 			break;
		 		case 'sms-action':
		 			openSmsAction(data);
		 			break;
		 		case 'whatsapp-action':
		 			openWhatsAppAction(data);
		 			break;
		 		default: return 0;
		 	}
	 });
}

function tableDoubleClick(data) {
	refreshInputFields()
	$("#id-action").val(data.id)
	$("#phase-action").val(data.phase.id)
	$("#seqNo-action").val(data.seqNo)
	$("#emailSubject-action").val( data.emailSubject )
	$('#emailText-action').summernote('code', data.emailText);
	$("#smsText-action").val(data.smsText)
	$("#whatsappText-action").val(data.whatsappText)
	$("#deadline-action").val(data.deadline)
}

function openEmailAction(data) {
	console.log(data)
	$('#modal-title').html(data.emailSubject);
	$('#modal-content').html( data.emailText );
	$('#modal').modal('open');
}

function openSmsAction(data) {}

function openWhatsAppAction(data) {}

function validator() {
	$("#action-form").validate({
		submitHandler: function(form) {
			$('#emailText-action').val( $('#emailText-action').summernote('code') );
		    save();
		},
		errorElement : "div",
		errorPlacement : function(error, element) {
			var er = error.insertAfter(element.next());
			
			if (er == null)
				er.insertAfter(element.next());

		}
	});
}

function refreshInputFields() {
	$('.input-field label').addClass('active');
	setTimeout(function() {
		$('.input-field label').addClass('active');
	}, 2);
}

function clearActionFields() {
	console.log('hello world')
	$("#id-action").val("");
	$("#action-form")[0].reset();
	$("#emailText-action").jqteVal("");
	table.rows('.selected').deselect();
}

function getParamUrl() {
	var queries = {};
	$.each(document.location.search.substr(1).split('&'), function(c, q) {
		var i = q.split('=');
		try {
			queries[i[0].toString()] = i[1].toString();
		}catch {
			return null;
		}
	});
	return queries;
}
