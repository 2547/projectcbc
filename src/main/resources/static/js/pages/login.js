$(document).ready(function() {

	$('.input-field label').addClass('active');
	setTimeout(function() {
		$('.input-field label').addClass('active');
	}, 2);

	getIp();
	$('#login').focus();
})

$("#login-form").validate({
	submitHandler : function(form) {
		$("#login-form")[0].submit();
	},
	rules : {

	},
	errorElement : "div",
	errorPlacement : function(error, element) {
		var er = error.insertAfter(element.next());

		if (er == null)
			er.insertAfter(element.next());
	}
});

function getIp() {
	$.getJSON("http://jsonip.com?callback=?", function(data) {
		$("#ipAddress").val(data.ip);
	});
	;
}
