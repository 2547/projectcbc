package com.adaptaconsultoria.cashbackcore.objects.pojo;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class CountryJson {

	private String isoCode;
	private String name;
	private String ddi;
	private String isoLanguage;
	private Integer code;
	private String regionName;
	private String taxidLabel;
	private Boolean taxidMandatory;

	private List<RegionJson> regions = new ArrayList<>();

}
