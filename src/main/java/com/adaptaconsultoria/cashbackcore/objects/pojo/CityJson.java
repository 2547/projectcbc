package com.adaptaconsultoria.cashbackcore.objects.pojo;

import lombok.Data;

@Data
public class CityJson {

	private String code;
	private String name;

}
