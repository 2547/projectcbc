package com.adaptaconsultoria.cashbackcore.objects.pojo;

import lombok.Data;

@Data
public class EmailConfig {

	String host;
	Integer port;
	String username;
	String password;
	String subject;
	String message;
	String to;
	String from;

}
