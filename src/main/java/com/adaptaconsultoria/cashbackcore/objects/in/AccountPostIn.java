package com.adaptaconsultoria.cashbackcore.objects.in;

import java.math.BigDecimal;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class AccountPostIn extends DefaultIn {

	private boolean doLogin = false;

	private String countryIsoCode;

	//User
	private String firstname;
	private String lastname;
	private String name;
	private String login;
	private String password;
	private String email;
	private String phone;
	private String isoLanguage;

	//Account
	private String accountNo;
	private String taxid;
	private String sponsorAccountNo;

	//Address
	private String address;
	private String address2;
	private String addressCountryIsoCode;
	private String addressRegionCode;
	private String addressCityCode;
	private String addressNumber;
	private String addressDistrict;
	private String addressZipcode;
	private BigDecimal addressLat;
	private BigDecimal addressLng;
	private String addressGeoinformation;

}
