package com.adaptaconsultoria.cashbackcore.objects.in;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class AccountPayoutEventPostIn extends DefaultIn {

	private Long accountPayoutCode;
	private String description;

}
