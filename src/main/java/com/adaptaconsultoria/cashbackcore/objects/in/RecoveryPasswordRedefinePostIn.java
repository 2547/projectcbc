package com.adaptaconsultoria.cashbackcore.objects.in;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class RecoveryPasswordRedefinePostIn extends DefaultIn {

	private String newPassword;
	private String confirmNewPassword;

}
