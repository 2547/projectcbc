package com.adaptaconsultoria.cashbackcore.objects.in;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class SalePostIn extends DefaultIn {

	private String documentNo;
	private Long codeRule;
	private BigDecimal saleAmount;
	private Date localTime;

	// Consumidor
	private Long codeAccount;
	private String accountNo;
	private String taxid;
	private String phone;

}
