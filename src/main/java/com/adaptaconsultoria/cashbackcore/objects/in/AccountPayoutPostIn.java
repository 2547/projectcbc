package com.adaptaconsultoria.cashbackcore.objects.in;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class AccountPayoutPostIn extends DefaultIn {

	private Long code;
	private Long payoutCode;
	private String value;

}
