package com.adaptaconsultoria.cashbackcore.objects.in;

import java.util.List;

import com.adaptaconsultoria.cashbackcore.objects.rest.AccountEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class AccountEntityPostIn extends AccountEntityGetIn {

	private List<AccountEntity> entities;

}
