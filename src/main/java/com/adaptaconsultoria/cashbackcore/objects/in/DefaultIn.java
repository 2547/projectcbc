package com.adaptaconsultoria.cashbackcore.objects.in;

import lombok.Data;

@Data
public class DefaultIn {

	private String token;
	private String ipAddress;

}
