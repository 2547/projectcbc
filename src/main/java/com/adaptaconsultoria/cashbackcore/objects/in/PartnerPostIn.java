package com.adaptaconsultoria.cashbackcore.objects.in;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.cashbackcore.objects.rest.PartnerRule;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class PartnerPostIn extends AccountPostIn {

	private Long code;
	private String description;
	private String url;
	private Long codeCategory;

	private List<PartnerRule> rules = new ArrayList<>();

}
