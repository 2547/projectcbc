package com.adaptaconsultoria.cashbackcore.objects.in;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class PartnerGetIn extends DefaultIn {

	private Long code;
	private String accountNo;

}
