package com.adaptaconsultoria.cashbackcore.objects.in;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class RecoveryPasswordPostIn extends DefaultIn {

	private String email;

}
