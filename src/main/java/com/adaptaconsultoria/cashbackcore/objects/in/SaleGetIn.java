package com.adaptaconsultoria.cashbackcore.objects.in;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class SaleGetIn extends DefaultIn {

	private String dateStart;
	private String dateEnd;

}
