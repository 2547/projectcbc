package com.adaptaconsultoria.cashbackcore.objects.out;

import com.adaptaconsultoria.cashbackcore.objects.out.error.AuthPostError;
import com.adaptaconsultoria.cashbackcore.objects.rest.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true, allowGetters = true)
public class AuthPostOut {

	private String token;

	private User user;

	private boolean hasError = false;
	private AuthPostError error;

}
