package com.adaptaconsultoria.cashbackcore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.cashbackcore.objects.rest.Payout;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class PayoutGetOut extends DefaultOut {

	private List<Payout> payouts = new ArrayList<>();

	public void setPayouts(List<com.adaptaconsultoria.cashbackcore.models.Payout> mods) {
		payouts = new ArrayList<>();
		for (com.adaptaconsultoria.cashbackcore.models.Payout mod : mods) {
			payouts.add(Payout.fromModel(mod));
		}
	}

}
