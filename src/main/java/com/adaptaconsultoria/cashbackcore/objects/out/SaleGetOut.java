package com.adaptaconsultoria.cashbackcore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.cashbackcore.objects.rest.Sale;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class SaleGetOut extends DefaultOut {

	private List<Sale> sales = new ArrayList<>();

	public void setSales(List<com.adaptaconsultoria.cashbackcore.models.Sale> sales) {
		this.sales = new ArrayList<>();
		for (com.adaptaconsultoria.cashbackcore.models.Sale sale : sales) {
			this.sales.add(Sale.fromModel(sale));
		}
	}

}
