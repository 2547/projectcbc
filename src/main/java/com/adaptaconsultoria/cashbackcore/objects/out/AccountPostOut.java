package com.adaptaconsultoria.cashbackcore.objects.out;

import com.adaptaconsultoria.cashbackcore.objects.rest.User;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class AccountPostOut extends DefaultOut {

	private boolean didLogin = false;
	private User user;

}
