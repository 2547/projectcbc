package com.adaptaconsultoria.cashbackcore.objects.out;

import com.adaptaconsultoria.cashbackcore.objects.out.error.DefaultError;

import lombok.Data;

@Data
public class DefaultOut {

	private String token;

	private boolean hasError = false;
	private DefaultError error;

}
