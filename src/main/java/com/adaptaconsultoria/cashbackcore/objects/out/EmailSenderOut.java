package com.adaptaconsultoria.cashbackcore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.cashbackcore.objects.in.DefaultIn;
import com.adaptaconsultoria.cashbackcore.objects.rest.Parameter;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class EmailSenderOut extends DefaultIn {

	private String id;
	private List<String> receivers = new ArrayList<>();
	private List<Parameter> parameters = new ArrayList<>();

}
