package com.adaptaconsultoria.cashbackcore.objects.out;

import com.adaptaconsultoria.cashbackcore.objects.rest.User;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class AuthGetOut extends DefaultOut {

	private User user;

}
