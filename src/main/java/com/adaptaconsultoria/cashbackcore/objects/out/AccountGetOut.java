package com.adaptaconsultoria.cashbackcore.objects.out;

import com.adaptaconsultoria.cashbackcore.objects.rest.Account;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class AccountGetOut extends DefaultOut {

	private Account account;

}
