package com.adaptaconsultoria.cashbackcore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.cashbackcore.objects.rest.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class EntityGetOut extends DefaultOut {

	private List<Entity> entities = new ArrayList<>();

	public void setEntities(List<com.adaptaconsultoria.cashbackcore.models.Entity> mods) {
		entities = new ArrayList<>();
		for (com.adaptaconsultoria.cashbackcore.models.Entity mod : mods) {
			entities.add(Entity.fromModel(mod));
		}
	}

}
