package com.adaptaconsultoria.cashbackcore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.cashbackcore.objects.rest.PartnerCategory;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class PartnerCategoryGetOut extends DefaultOut {

	private List<PartnerCategory> categories = new ArrayList<>();

	public void setCategories(List<com.adaptaconsultoria.cashbackcore.models.PartnerCategory> mods) {
		categories = new ArrayList<>();
		for (com.adaptaconsultoria.cashbackcore.models.PartnerCategory mod : mods) {
			categories.add(PartnerCategory.fromModel(mod));
		}
	}

}
