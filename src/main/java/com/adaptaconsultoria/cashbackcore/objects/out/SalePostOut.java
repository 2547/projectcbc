package com.adaptaconsultoria.cashbackcore.objects.out;

import java.math.BigDecimal;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class SalePostOut extends DefaultOut {

	private BigDecimal cashback;

}
