package com.adaptaconsultoria.cashbackcore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.cashbackcore.objects.rest.Cashback;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class CashbackGetOut extends DefaultOut {

	private List<Cashback> cashbacks = new ArrayList<>();

	public void setCashbacks(List<com.adaptaconsultoria.cashbackcore.models.Cashback> cashbacks) {
		this.cashbacks = new ArrayList<>();
		for (com.adaptaconsultoria.cashbackcore.models.Cashback cashback : cashbacks) {
			this.cashbacks.add(Cashback.fromModel(cashback));
		}
	}

}
