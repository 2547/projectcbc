package com.adaptaconsultoria.cashbackcore.objects.out;

import java.util.List;

import com.adaptaconsultoria.cashbackcore.objects.pojo.CountryJson;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class LocalizationGetOut extends DefaultOut {

	private List<CountryJson> countries;
	private String countriesStr;

}
