package com.adaptaconsultoria.cashbackcore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.cashbackcore.objects.rest.AccountPayout;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class AccountPayoutGetOut extends DefaultOut {

	private List<AccountPayout> payouts;

	public void setPayouts(List<com.adaptaconsultoria.cashbackcore.models.AccountPayout> mods) {
		payouts = new ArrayList<>();
		for (com.adaptaconsultoria.cashbackcore.models.AccountPayout mod : mods) {
			payouts.add(AccountPayout.fromModel(mod));
		}
	}

}
