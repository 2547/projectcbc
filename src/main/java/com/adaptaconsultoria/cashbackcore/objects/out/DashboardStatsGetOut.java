package com.adaptaconsultoria.cashbackcore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.cashbackcore.models.DashboardStats;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class DashboardStatsGetOut extends DefaultOut {

	private List<DashboardStats> stats = new ArrayList<>();

}
