package com.adaptaconsultoria.cashbackcore.objects.out;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class UserLoginGetOut extends DefaultOut {

	private boolean isvalid = false;

}
