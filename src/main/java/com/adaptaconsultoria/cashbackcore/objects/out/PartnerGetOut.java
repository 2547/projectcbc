package com.adaptaconsultoria.cashbackcore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.cashbackcore.objects.rest.Partner;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class PartnerGetOut extends DefaultOut {

	private Partner partner;
	private List<Partner> partners = new ArrayList<>();

	public void setPartner(com.adaptaconsultoria.cashbackcore.models.Partner partner) {
		this.partner = Partner.fromModel(partner);
	}

	public void setPartners(List<com.adaptaconsultoria.cashbackcore.models.Partner> partners) {
		this.partners = new ArrayList<>();
		for (com.adaptaconsultoria.cashbackcore.models.Partner partner : partners) {
			this.partners.add(Partner.fromModel(partner));
		}
	}

}
