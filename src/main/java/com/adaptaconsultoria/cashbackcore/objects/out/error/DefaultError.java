package com.adaptaconsultoria.cashbackcore.objects.out.error;

import lombok.Data;

@Data
public class DefaultError {

	private String error;

}
