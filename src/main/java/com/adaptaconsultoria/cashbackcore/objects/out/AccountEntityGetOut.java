package com.adaptaconsultoria.cashbackcore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.cashbackcore.objects.rest.AccountEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class AccountEntityGetOut extends DefaultOut {

	private List<AccountEntity> entities;

	public void setEntities(List<com.adaptaconsultoria.cashbackcore.models.AccountEntity> mods) {
		entities = new ArrayList<>();
		for (com.adaptaconsultoria.cashbackcore.models.AccountEntity mod : mods) {
			entities.add(AccountEntity.fromModel(mod));
		}
	}

}
