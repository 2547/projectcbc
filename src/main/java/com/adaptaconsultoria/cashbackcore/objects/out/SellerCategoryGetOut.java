package com.adaptaconsultoria.cashbackcore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.cashbackcore.objects.rest.SellerCategory;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class SellerCategoryGetOut extends DefaultOut {

	private List<SellerCategory> categories = new ArrayList<>();

	public void setCategories(List<com.adaptaconsultoria.cashbackcore.models.SellerCategory> mods) {
		categories = new ArrayList<>();
		for (com.adaptaconsultoria.cashbackcore.models.SellerCategory mod : mods) {
			categories.add(SellerCategory.fromModel(mod));
		}
	}

}
