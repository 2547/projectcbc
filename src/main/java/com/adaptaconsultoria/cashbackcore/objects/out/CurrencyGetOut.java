package com.adaptaconsultoria.cashbackcore.objects.out;

import java.util.ArrayList;
import java.util.List;

import com.adaptaconsultoria.cashbackcore.objects.rest.Currency;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class CurrencyGetOut extends DefaultOut {

	private List<Currency> currencies = new ArrayList<>();

	public void setCurrencies(List<com.adaptaconsultoria.cashbackcore.models.Currency> mods) {
		currencies = new ArrayList<>();
		for (com.adaptaconsultoria.cashbackcore.models.Currency mod : mods) {
			currencies.add(Currency.fromModel(mod));
		}
	}

}
