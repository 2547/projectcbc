package com.adaptaconsultoria.cashbackcore.objects.rest;

import lombok.Data;

@Data
public class Entity {

	private Long code;
	private String name;
	private String description;

	public static Entity fromModel(com.adaptaconsultoria.cashbackcore.models.Entity mod) {
		Entity entity = new Entity();

		try {
			entity.setCode(mod.getId());
		} catch (Exception e) {
		}
		try {
			entity.setName(mod.getName());
		} catch (Exception e) {
		}
		try {
			entity.setDescription(mod.getDescription());
		} catch (Exception e) {
		}

		return entity;
	}

}
