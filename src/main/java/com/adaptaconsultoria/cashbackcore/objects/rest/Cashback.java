package com.adaptaconsultoria.cashbackcore.objects.rest;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

@Data
public class Cashback {

	private Sale sale;
	private String currency;
	private BigDecimal amount;
	private Boolean isreceipt = true;
	private String description;
	private Bonus bonus;
	private Date dateSale;
	private Integer level;
	private BigDecimal rate;

	public static Cashback fromModel(com.adaptaconsultoria.cashbackcore.models.Cashback mod) {
		if (mod == null) {
			return null;
		}

		Cashback cashback = new Cashback();

		try {
			cashback.setSale(Sale.fromModel(mod.getSale()));
		} catch (Exception e) {
		}

		try {
			cashback.setCurrency(mod.getCurrency().getCode());
		} catch (Exception e) {
		}

		try {
			cashback.setAmount(mod.getAmount());
		} catch (Exception e) {
		}

		try {
			cashback.setIsreceipt(mod.getIsreceipt());
		} catch (Exception e) {
		}

		try {
			cashback.setDescription(mod.getDescription());
		} catch (Exception e) {
		}

		try {
			cashback.setBonus(Bonus.fromModel(mod.getBonus()));
		} catch (Exception e) {
		}

		try {
			cashback.setDateSale(mod.getDateSale());
		} catch (Exception e) {
		}

		try {
			cashback.setLevel(mod.getLevel());
		} catch (Exception e) {
		}

		try {
			cashback.setRate(mod.getRate());
		} catch (Exception e) {
		}

		return cashback;
	}

}
