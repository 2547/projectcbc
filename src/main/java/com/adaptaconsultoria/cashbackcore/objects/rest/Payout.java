package com.adaptaconsultoria.cashbackcore.objects.rest;

import lombok.Data;

@Data
public class Payout {

	private Long code;
	private String name;
	private String description;
	private String valueMask;
	private Long valueLength;
	private String remittanceClassname;
	private PaymentMethod paymentMethod;
	private Partner partner;

	public static Payout fromModel(com.adaptaconsultoria.cashbackcore.models.Payout mod) {
		Payout payout = new Payout();

		try {
			payout.setCode(mod.getId());
		} catch (Exception e) {
		}
		try {
			payout.setName(mod.getName());
		} catch (Exception e) {
		}
		try {
			payout.setDescription(mod.getDescription());
		} catch (Exception e) {
		}
		try {
			payout.setValueMask(mod.getValueMask());
		} catch (Exception e) {
		}
		try {
			payout.setValueLength(mod.getValueLength());
		} catch (Exception e) {
		}
		try {
			payout.setRemittanceClassname(mod.getRemittanceClassname());
		} catch (Exception e) {
		}
		try {
			payout.setPaymentMethod(PaymentMethod.fromModel(mod.getPaymentMethod()));
		} catch (Exception e) {
		}
		try {
			payout.setPartner(Partner.fromModel(mod.getPartner()));
		} catch (Exception e) {
		}

		return payout;
	}

}
