package com.adaptaconsultoria.cashbackcore.objects.rest;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class AccountEntity {

	private Long code;
	private Entity entity;
	private Long entityCode;
	private BigDecimal pcEntity;

	public static AccountEntity fromModel(com.adaptaconsultoria.cashbackcore.models.AccountEntity mod) {
		if (mod == null) {
			return null;
		}

		AccountEntity account = new AccountEntity();

		try {
			account.setCode(mod.getId());
		} catch (Exception e) {
		}

		try {
			account.setEntity(Entity.fromModel(mod.getEntity()));
		} catch (Exception e) {
		}

		try {
			account.setEntityCode(account.getEntity().getCode());
		} catch (Exception e) {
		}

		try {
			account.setPcEntity(mod.getPcEntity());
		} catch (Exception e) {
		}

		return account;
	}

}
