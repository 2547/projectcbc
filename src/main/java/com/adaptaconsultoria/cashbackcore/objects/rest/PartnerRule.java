package com.adaptaconsultoria.cashbackcore.objects.rest;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class PartnerRule {

	private Long code;
	private boolean isactive;
	private String description;
	private String currency;
	private BigDecimal pcCashback;

	public static PartnerRule fromModel(com.adaptaconsultoria.cashbackcore.models.PartnerRule mod) {
		PartnerRule rule = new PartnerRule();
		try {
			rule.setCode(mod.getId());
		} catch (Exception e) {
		}
		try {
			rule.setCurrency(mod.getCurrency().getCode());
		} catch (Exception e) {
		}
		try {
			rule.setIsactive(mod.getIsactive());
		} catch (Exception e) {
		}
		try {
			rule.setPcCashback(mod.getPcCashback());
		} catch (Exception e) {
		}
		try {
			rule.setDescription(mod.getDescription());
		} catch (Exception e) {
		}
		return rule;
	}

	public boolean equalsModel(com.adaptaconsultoria.cashbackcore.models.PartnerRule mod) {
		return (mod.getDescription().equalsIgnoreCase(description) &&
				mod.getCurrency().getCode().equalsIgnoreCase(currency) &&
				mod.getPcCashback().equals(pcCashback));
	}

}
