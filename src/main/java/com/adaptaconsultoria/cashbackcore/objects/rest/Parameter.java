package com.adaptaconsultoria.cashbackcore.objects.rest;

import lombok.Data;

@Data
public class Parameter {

	private String name;
	private String value;

}
