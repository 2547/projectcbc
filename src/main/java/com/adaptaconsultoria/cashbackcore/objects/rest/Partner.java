package com.adaptaconsultoria.cashbackcore.objects.rest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Partner {

	private Long code;
	private String name;
	private String description;
	private String url;
	private String email;
	private String phone;
	private PartnerCategory partnerCategory;
	private SellerCategory sellerCategory;

	//Address
	private String address;
	private String addressCountryIsoCode;
	private String addressRegionCode;
	private String addressCityCode;
	private String addressNumber;
	private String addressDistrict;
	private String addressZipcode;
	private BigDecimal addressLat;
	private BigDecimal addressLng;
	private String addressGeoinformation;

	private List<PartnerRule> rules = new ArrayList<>();

	public static Partner fromModel(com.adaptaconsultoria.cashbackcore.models.Partner mod) {
		if (mod == null) {
			return null;
		}

		Partner partner = new Partner();

		try {
			partner.setCode(mod.getId());
		} catch (Exception e) {
		}

		try {
			partner.setName(mod.getName());
		} catch (Exception e) {
		}

		try {
			partner.setDescription(mod.getDescription());
		} catch (Exception e) {
		}

		try {
			partner.setUrl(mod.getUrl());
		} catch (Exception e) {
		}

		try {
			partner.setEmail(mod.getEmail());
		} catch (Exception e) {
		}

		try {
			partner.setPhone(mod.getPhone());
		} catch (Exception e) {
		}

		try {
			PartnerCategory category = new PartnerCategory();
			category.setCode(mod.getPartnerCategory().getId());
			category.setName(mod.getPartnerCategory().getName());
			category.setDescription(mod.getPartnerCategory().getDescription());
			partner.setPartnerCategory(category);
		} catch (Exception e) {
		}

		try {
			SellerCategory category = new SellerCategory();
			category.setCode(mod.getSellerCategory().getId());
			category.setName(mod.getSellerCategory().getName());
			category.setDescription(mod.getSellerCategory().getDescription());
			partner.setSellerCategory(category);
		} catch (Exception e) {
		}

		try {
			partner.setAddress(mod.getAddress().getAddress1());
		} catch (Exception e) {
		}

		try {
			partner.setAddressCountryIsoCode(mod.getAddress().getCountry().getIsoCode());
		} catch (Exception e) {
		}

		try {
			partner.setAddressRegionCode(mod.getAddress().getRegion().getCode());
		} catch (Exception e) {
		}

		try {
			partner.setAddressCityCode(mod.getAddress().getCity().getCode());
		} catch (Exception e) {
		}

		try {
			partner.setAddressNumber(mod.getAddress().getNumber());
		} catch (Exception e) {
		}

		try {
			partner.setAddressDistrict(mod.getAddress().getDistrict());
		} catch (Exception e) {
		}

		try {
			partner.setAddressZipcode(mod.getAddress().getZipcode());
		} catch (Exception e) {
		}

		try {
			partner.setAddressLat(mod.getAddress().getLat());
		} catch (Exception e) {
		}

		try {
			partner.setAddressLng(mod.getAddress().getLng());
		} catch (Exception e) {
		}

		try {
			partner.setAddressGeoinformation(mod.getAddress().getGeoinformation());
		} catch (Exception e) {
		}

		try {
			partner.setAddressGeoinformation(mod.getAddress().getGeoinformation());
		} catch (Exception e) {
		}

		for (com.adaptaconsultoria.cashbackcore.models.PartnerRule rule : mod.getRules()) {
			partner.getRules().add(PartnerRule.fromModel(rule));
		}

		return partner;
	}

}
