package com.adaptaconsultoria.cashbackcore.objects.rest;

import lombok.Data;

@Data
public class Token {

	private String token;
	private String error;

}
