package com.adaptaconsultoria.cashbackcore.objects.rest;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

@Data
public class Sale {

	private String documentNo;
	private String saleCurrency;
	private BigDecimal saleAmount;
	private BigDecimal saleCashback;
	private Date localTime;
	private String countryIsoCode;
	private String accountNo;
	private String currency;
	private BigDecimal cashback;

	public static Sale fromModel(com.adaptaconsultoria.cashbackcore.models.Sale mod) {
		if (mod == null) {
			return null;
		}

		Sale sale = new Sale();

		try {
			sale.setDocumentNo(mod.getDocumentno());
		} catch (Exception e) {
		}

		try {
			sale.setSaleCurrency(mod.getSaleCurrency().getCode());
		} catch (Exception e) {
		}

		try {
			sale.setSaleAmount(mod.getSaleAmount());
		} catch (Exception e) {
		}

		try {
			sale.setSaleCashback(mod.getSaleCashback());
		} catch (Exception e) {
		}

		try {
			sale.setLocalTime(mod.getLocalTime());
		} catch (Exception e) {
		}

		try {
			sale.setCountryIsoCode(mod.getCountry().getIsoCode());
		} catch (Exception e) {
		}

		try {
			sale.setAccountNo(mod.getAccount().getAccountNo());
		} catch (Exception e) {
		}

		try {
			sale.setCurrency(mod.getCurrency().getCode());
		} catch (Exception e) {
		}

		try {
			sale.setCashback(mod.getCashback());
		} catch (Exception e) {
		}

		return sale;
	}

}
