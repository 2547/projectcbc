package com.adaptaconsultoria.cashbackcore.objects.rest;

import lombok.Data;

@Data
public class PartnerCategory {

	private Long code;
	private String name;
	private String description;

	public static PartnerCategory fromModel(com.adaptaconsultoria.cashbackcore.models.PartnerCategory mod) {
		PartnerCategory currency = new PartnerCategory();

		try {
			currency.setCode(mod.getId());
		} catch (Exception e) {
		}
		try {
			currency.setName(mod.getName());
		} catch (Exception e) {
		}
		try {
			currency.setDescription(mod.getDescription());
		} catch (Exception e) {
		}

		return currency;
	}

}
