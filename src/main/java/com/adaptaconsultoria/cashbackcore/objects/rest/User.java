package com.adaptaconsultoria.cashbackcore.objects.rest;

import lombok.Data;

@Data
public class User {

	private String firstname;
	private String lastname;
	private String name;
	private String email;
	private String login;
	private Long company;
	private boolean isactive;
	private String phone;
	private String role;

	public static User fromModel(com.adaptaconsultoria.cashbackcore.models.User mod) {
		if (mod == null) {
			return null;
		}

		User user = new User();

		try {
			user.setFirstname(mod.getFirstname());
		} catch (Exception e) {
		}

		try {
			user.setLastname(mod.getLastname());
		} catch (Exception e) {
		}

		try {
			user.setName(mod.getName());
		} catch (Exception e) {
		}

		try {
			user.setEmail(mod.getEmail());
		} catch (Exception e) {
		}

		try {
			user.setLogin(mod.getLogin());
		} catch (Exception e) {
		}

		try {
			user.setCompany(mod.getCompany().getId());
		} catch (Exception e) {
		}

		try {
			user.setIsactive(mod.getIsactive());
		} catch (Exception e) {
		}

		try {
			user.setPhone(mod.getPhone());
		} catch (Exception e) {
		}

		return user;
	}

}
