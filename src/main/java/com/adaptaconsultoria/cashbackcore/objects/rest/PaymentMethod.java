package com.adaptaconsultoria.cashbackcore.objects.rest;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class PaymentMethod {

	private String name;
	private String description;
	private BigDecimal fee;
	private BigDecimal pcFee;

	public static PaymentMethod fromModel(com.adaptaconsultoria.cashbackcore.models.PaymentMethod mod) {
		if (mod == null) {
			return null;
		}

		PaymentMethod payout = new PaymentMethod();

		try {
			payout.setName(mod.getName());
		} catch (Exception e) {
		}

		try {
			payout.setDescription(mod.getDescription());
		} catch (Exception e) {
		}

		try {
			payout.setFee(mod.getFee());
		} catch (Exception e) {
		}

		try {
			payout.setPcFee(mod.getPcFee());
		} catch (Exception e) {
		}

		return payout;
	}

}
