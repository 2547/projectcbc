package com.adaptaconsultoria.cashbackcore.objects.rest;

import lombok.Data;

@Data
public class AccountPayout {

	private Long code;
	private Payout payout;
	private Long payoutCode;
	private String value;
	private Boolean isBlocked;
	private Boolean isCanceled;

	public static AccountPayout fromModel(com.adaptaconsultoria.cashbackcore.models.AccountPayout mod) {
		if (mod == null) {
			return null;
		}

		AccountPayout payout = new AccountPayout();

		try {
			payout.setCode(mod.getId());
		} catch (Exception e) {
		}

		try {
			payout.setPayout(Payout.fromModel(mod.getPayout()));
		} catch (Exception e) {
		}

		try {
			payout.setPayoutCode(mod.getPayout().getId());
		} catch (Exception e) {
		}

		try {
			payout.setValue(mod.getValue());
		} catch (Exception e) {
		}

		try {
			payout.setIsBlocked(mod.getIsBlocked());
		} catch (Exception e) {
		}

		try {
			payout.setIsCanceled(mod.getIsCanceled());
		} catch (Exception e) {
		}

		return payout;
	}

}
