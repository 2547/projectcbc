package com.adaptaconsultoria.cashbackcore.objects.rest;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class Account {

	private String countryIsoCode;
	private String accountNo;
	private String taxid;
	private BigDecimal personalValue;
	private BigDecimal earningValue;
	private BigDecimal returnedValue;
	private String accountSponsorAccount;
	private String currency;

	//Address
	private String address;
	private String addressCountryIsoCode;
	private String addressRegionCode;
	private String addressCityCode;
	private String addressNumber;
	private String addressDistrict;
	private String addressZipcode;
	private BigDecimal addressLat;
	private BigDecimal addressLng;
	private String addressGeoinformation;

	public static Account fromModel(com.adaptaconsultoria.cashbackcore.models.Account mod) {
		if (mod == null) {
			return null;
		}

		Account account = new Account();

		try {
			account.setCountryIsoCode(mod.getCountry().getIsoCode());
		} catch (Exception e) {
		}

		try {
			account.setAccountNo(mod.getAccountNo());
		} catch (Exception e) {
		}

		try {
			account.setTaxid(mod.getTaxid());
		} catch (Exception e) {
		}

		try {
			account.setPersonalValue(mod.getPersonalValue());
		} catch (Exception e) {
		}

		try {
			account.setEarningValue(mod.getEarningValue());
		} catch (Exception e) {
		}

		try {
			account.setReturnedValue(mod.getReturnedValue());
		} catch (Exception e) {
		}

		try {
			account.setAccountSponsorAccount(mod.getSponsorAccount().getAccountNo());
		} catch (Exception e) {
		}

		try {
			account.setCurrency(mod.getCurrency().getCode());
		} catch (Exception e) {
		}

		try {
			account.setAddress(mod.getAddress().getAddress1());
		} catch (Exception e) {
		}

		try {
			account.setAddressCountryIsoCode(mod.getAddress().getCountry().getIsoCode());
		} catch (Exception e) {
		}

		try {
			account.setAddressRegionCode(mod.getAddress().getRegion().getCode());
		} catch (Exception e) {
		}

		try {
			account.setAddressCityCode(mod.getAddress().getCity().getCode());
		} catch (Exception e) {
		}

		try {
			account.setAddressNumber(mod.getAddress().getNumber());
		} catch (Exception e) {
		}

		try {
			account.setAddressDistrict(mod.getAddress().getDistrict());
		} catch (Exception e) {
		}

		try {
			account.setAddressZipcode(mod.getAddress().getZipcode());
		} catch (Exception e) {
		}

		try {
			account.setAddressLat(mod.getAddress().getLat());
		} catch (Exception e) {
		}

		try {
			account.setAddressLng(mod.getAddress().getLng());
		} catch (Exception e) {
		}

		try {
			account.setAddressGeoinformation(mod.getAddress().getGeoinformation());
		} catch (Exception e) {
		}

		return account;
	}

}
