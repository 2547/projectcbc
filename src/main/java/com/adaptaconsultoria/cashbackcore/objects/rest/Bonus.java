package com.adaptaconsultoria.cashbackcore.objects.rest;

import lombok.Data;

@Data
public class Bonus {

	private String code;
	private String name;
	private String description;

	public static Bonus fromModel(com.adaptaconsultoria.cashbackcore.models.Bonus mod) {
		if (mod == null) {
			return null;
		}

		Bonus bonus = new Bonus();

		try {
			bonus.setCode(mod.getCode());
		} catch (Exception e) {
		}

		try {
			bonus.setName(mod.getName());
		} catch (Exception e) {
		}

		try {
			bonus.setDescription(mod.getDescription());
		} catch (Exception e) {
		}

		return bonus;
	}

}
