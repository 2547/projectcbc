package com.adaptaconsultoria.cashbackcore.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.models.App;
import com.adaptaconsultoria.cashbackcore.repositories.AppRepository;

@Service
public class AppService {

	@Autowired private AppRepository appRepository;
	@Autowired private TokenService tokenService;

	public App checkAppLogin(String appToken, String appPassword) throws Exception {
		App app = appRepository.findTop1ByTokenAndIsactiveTrue(appToken);
		if (app == null) {
			throw new Exception("App not found!");
		}
		if (!tokenService.compare(app.getPassword(), appPassword)) {
			throw new Exception("App not found!");
		}
		return app;
	}

}