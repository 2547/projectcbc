package com.adaptaconsultoria.cashbackcore.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.models.Account;
import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.models.Currency;
import com.adaptaconsultoria.cashbackcore.models.Partner;
import com.adaptaconsultoria.cashbackcore.models.PartnerCategory;
import com.adaptaconsultoria.cashbackcore.models.PartnerRule;
import com.adaptaconsultoria.cashbackcore.models.SellerCategory;
import com.adaptaconsultoria.cashbackcore.models.User;
import com.adaptaconsultoria.cashbackcore.objects.in.PartnerPostIn;
import com.adaptaconsultoria.cashbackcore.repositories.AccountRepository;
import com.adaptaconsultoria.cashbackcore.repositories.CurrencyRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PartnerCategoryRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PartnerRepository;
import com.adaptaconsultoria.cashbackcore.repositories.SellerCategoryRepository;
import com.adaptaconsultoria.cashbackcore.utils.SessionUtility;

@Service
public class PartnerService {

	@Autowired private UserService userService;
	@Autowired private AccountService accountService;
	@Autowired private AccountRepository accountRepository;
	@Autowired private PartnerRepository partnerRepository;
	@Autowired private CurrencyRepository currencyRepository;
	@Autowired private PartnerCategoryRepository partnerCategoryRepository;
	@Autowired private SellerCategoryRepository sellerCategoryRepository;

	public Partner save(Partner partner) {
		return partnerRepository.save(partner);
	}

	@Transactional
	public void delete(Long partnerId) throws Exception {
		Optional<Partner> partner = partnerRepository.findById(partnerId);
		if (partner.isPresent()) {
			Account account = partner.get().getAccount();
			partnerRepository.delete(partner.get());
			accountService.delete(account.getId());
		} else {
			throw new Exception("Partner not found!");
		}
	}

	public Partner createSeller(Partner seller) throws Exception {
		return createNew(seller, true);
	}
	public Partner createPartner(Partner partner) throws Exception {
		return createNew(partner, false);
	}
	private Partner createNew(Partner partner, boolean isSeller) throws Exception {
		PartnerPostIn in = new PartnerPostIn();

		in.setCountryIsoCode(partner.getAddress().getCountry().getIsoCode());
		in.setName(partner.getName());
		in.setEmail(partner.getEmail());
		in.setPhone(partner.getPhone());

		try {
			in.setSponsorAccountNo(partner.getLeader().getAccount().getAccountNo());
		} catch (Exception e) {
		}

		try {
			in.setAddress(partner.getAddress().getAddress1());
		} catch (Exception e) {
		}
		try {
			in.setAddress2(partner.getAddress().getAddress2());
		} catch (Exception e) {
		}
		try {
			in.setAddressCountryIsoCode(partner.getAddress().getCountry().getIsoCode());
		} catch (Exception e) {
		}
		try {
			in.setAddressRegionCode(partner.getAddress().getRegion().getCode());
		} catch (Exception e) {
		}
		try {
			in.setAddressCityCode(partner.getAddress().getCity().getCode());
		} catch (Exception e) {
		}
		try {
			in.setAddressNumber(partner.getAddress().getNumber());
		} catch (Exception e) {
		}
		try {
			in.setAddressDistrict(partner.getAddress().getDistrict());
		} catch (Exception e) {
		}
		try {
			in.setAddressZipcode(partner.getAddress().getZipcode());
		} catch (Exception e) {
		}
		try {
			in.setAddressLat(partner.getAddress().getLat());
		} catch (Exception e) {
		}
		try {
			in.setAddressLng(partner.getAddress().getLng());
		} catch (Exception e) {
		}
		try {
			in.setAddressGeoinformation(partner.getAddress().getGeoinformation());
		} catch (Exception e) {
		}

		in.setDescription(partner.getDescription());
		in.setUrl(partner.getUrl());

		AppSession appSession = new AppSession();
		appSession.setCompany(partner.getCompany());
		appSession.setUser(SessionUtility.getUserLoggedin());

		if (isSeller) {
			in.setCodeCategory(partner.getSellerCategory().getId());
			for (PartnerRule pRule : partner.getRules()) {
				com.adaptaconsultoria.cashbackcore.objects.rest.PartnerRule rule = new com.adaptaconsultoria.cashbackcore.objects.rest.PartnerRule();
				rule.setDescription(pRule.getDescription());
				rule.setCurrency(pRule.getCurrency().getCode());
				rule.setPcCashback(pRule.getPcCashback());

				in.getRules().add(rule);
			}
			return createSeller(in, appSession);
		} else {
			in.setCodeCategory(partner.getPartnerCategory().getId());
			return createPartner(in, appSession);
		}
	}

	public Partner createSeller(PartnerPostIn in, AppSession appSession) throws Exception {
		return createNew(in, appSession, true);
	}
	
	public Partner createPartner(PartnerPostIn in, AppSession appSession) throws Exception {
		return createNew(in, appSession, false);
	}
	
	private Partner createNew(PartnerPostIn in, AppSession appSession, boolean isSeller) throws Exception {

		Partner partner = validateCreate(in, appSession, isSeller);
		User userUpdating = null;

		partner.setName(in.getName());
		partner.setDescription(in.getDescription());
		partner.setUrl(in.getUrl());

		if (partner.getId() == null) {
			Account account = accountService.createAccount(in, appSession);
			if (account == null) {
				throw new Exception("Account could not be created!");
			}

			// Campos validados pelo método "createAccount" acima
			partner.setCreatedBy(account.getCreatedBy());
			partner.setAccount(account);
			partner.setEmail(in.getEmail());
			partner.setPhone(in.getPhone());
			partner.setAddress(account.getAddress());
		} else {
			userUpdating = userService.getUserCreating(appSession);
		}

		partner.setUpdatedBy(userUpdating);
		partner.setUpdated(new Date());

		for (PartnerRule rule : partner.getRules()) {
			rule.setCreatedBy(partner.getCreatedBy());
			rule.setUpdatedBy(userUpdating);
			rule.setUpdated(new Date());
			rule.setPartner(partner);
		}

		return save(partner);

	}

	private Partner validateCreate(PartnerPostIn in, AppSession appSession, boolean isSeller) throws Exception {

		Partner partner = new Partner();

		if (in.getCode() != null) {
			if (isSeller) {
				partner = partnerRepository.findTop1ByCompanyAndIdAndSeller(appSession.getCompany(), in.getCode());
				if (partner == null) {
					throw new Exception("Seller not found!");
				}
			} else {
				partner = partnerRepository.findTop1ByCompanyAndIdAndPartner(appSession.getCompany(), in.getCode());
				if (partner == null) {
					throw new Exception("Partner not found!");
				}
			}
		}

		partner.setCompany(appSession.getCompany());

		try {
			if (!appSession.getUser().getIsadmin()) {
				Account account = accountRepository.findTop1ByCompanyAndUserAndIsactiveTrue(appSession.getCompany(), appSession.getUser());
				if (account == null) {
					throw new Exception();
				}
				Partner p = partnerRepository.findTop1ByCompanyAndAccountNoAndPartnerCategoryNotNull(appSession.getCompany(), account.getAccountNo());
				if (p == null && !appSession.getUser().getIsadmin()) {
					throw new Exception();
				}
				in.setSponsorAccountNo(p.getAccount().getAccountNo());
			} else {
				try {
					partner.setLeader(partnerRepository.findTop1ByCompanyAndAccountNoAndPartnerCategoryNotNull(appSession.getCompany(), in.getSponsorAccountNo()));
				} catch (Exception e) {
				}
			}
		} catch (Exception e) {
			throw new Exception("Permission denied!");
		}

		if (in.getCodeCategory() == null) {
			throw new Exception("CodeCategory is required!");
		} else {
			
			if (isSeller) {
				try {
					SellerCategory category = sellerCategoryRepository.findTop1ByCompanyAndId(appSession.getCompany(), in.getCodeCategory());
					if (category == null) {
						throw new Exception();
					}
					partner.setSellerCategory(category);
				} catch (Exception e) {
					throw new Exception("Seller Category not found!");
				}
			} else {
				try {
					PartnerCategory category = partnerCategoryRepository.findTop1ByCompanyAndId(appSession.getCompany(), in.getCodeCategory());
					if (category == null) {
						throw new Exception();
					}
					partner.setPartnerCategory(category);
				} catch (Exception e) {
					throw new Exception("Partner Category not found!");
				}
			}
		}

		if (StringUtils.isBlank(in.getName())) {
			throw new Exception("Name is required!");
		}

		if (isSeller) {
			
//			if (in.getRules() == null || in.getRules().size() <= 0) {
//				throw new Exception("Rules is required!");
//			}

			List<PartnerRule> listRemove = new ArrayList<>();
			for (PartnerRule r : partner.getRules()) {
				boolean found = false;
				for (com.adaptaconsultoria.cashbackcore.objects.rest.PartnerRule rIn : in.getRules()) {
					if (rIn.equalsModel(r)) {
						found = true;
						break;
					}
				}
				if (!found) {
					listRemove.add(r);
				}
			}

			partner.getRules().removeAll(listRemove);

			for (com.adaptaconsultoria.cashbackcore.objects.rest.PartnerRule ruleIn : in.getRules()) {
				PartnerRule rule = new PartnerRule();
				rule.setCompany(partner.getCompany());
				if (partner.getId() != null) {
					boolean found = false;
					for (PartnerRule r : partner.getRules()) {
						try {
							if (ruleIn.equalsModel(r)) {
								found = true;
								break;
							}
						} catch (Exception ignore) {
						}
					}
					if (found) {
						continue;
					}

				}

				if (StringUtils.isBlank(ruleIn.getDescription())) {
					throw new Exception("Description of rule is required!");
				}
				if (ruleIn.getPcCashback() == null) {
					throw new Exception("PcCashback of rule is required!");
				} else if (ruleIn.getPcCashback().compareTo(BigDecimal.ZERO) <= 0) {
					throw new Exception("PcCashback should be greater than zero!");
				}

				try {
					if (StringUtils.isBlank(ruleIn.getCurrency())) {
						throw new Exception("Currency of rule is required!");
					}
					Currency currency = currencyRepository.findByCompanyAndCode(appSession.getCompany(), ruleIn.getCurrency());
					if (currency == null) {
						throw new Exception();
					}
					rule.setCurrency(currency);
				} catch (Exception e) {
					throw new Exception("Currency not found!");
				}

				rule.setDescription(ruleIn.getDescription());
				rule.setPcCashback(ruleIn.getPcCashback());
				partner.getRules().add(rule);
			}
		}

		return partner;
	}

}
