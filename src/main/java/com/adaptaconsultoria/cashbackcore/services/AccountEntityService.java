package com.adaptaconsultoria.cashbackcore.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.models.Account;
import com.adaptaconsultoria.cashbackcore.models.AccountEntity;
import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.Entity;
import com.adaptaconsultoria.cashbackcore.models.User;
import com.adaptaconsultoria.cashbackcore.objects.in.AccountEntityPostIn;
import com.adaptaconsultoria.cashbackcore.repositories.AccountEntityRepository;
import com.adaptaconsultoria.cashbackcore.repositories.EntityRepository;

@Service
public class AccountEntityService {

	@Autowired private UserService userService;
	@Autowired private AccountService accountService;
	@Autowired private EntityRepository entityRepository;
	@Autowired private AccountEntityRepository accountEntityRepository;

	public void createAccountEntity(AccountEntityPostIn in, AppSession appSession) throws Exception {
		User userCreating = userService.getUserCreating(appSession);

		Account account = accountService.getAccount(appSession);
		account.setUpdated(new Date());
		account.setUpdatedBy(userCreating);

		List<AccountEntity> entities = validateCreate(in, account);

		account.setEntities(entities);
//		account.getEntities().clear();
		account = accountService.save(account);

//		account.getEntities().addAll(entities);
//		account = accountService.save(account);
	}

	private List<AccountEntity> validateCreate(AccountEntityPostIn in, Account account) throws Exception {
		Company company = account.getCompany();
		User userCreating = account.getUpdatedBy();

		List<AccountEntity> entities = new ArrayList<>();

		List<Long> codes = new ArrayList<>();
		BigDecimal percents = BigDecimal.ZERO;
		for (com.adaptaconsultoria.cashbackcore.objects.rest.AccountEntity ae : in.getEntities()) {
			AccountEntity entity = new AccountEntity();

			if (ae.getPcEntity() == null) {
				throw new Exception("PcEntity is required!");
			} else if (ae.getPcEntity().compareTo(BigDecimal.ZERO) <= 0) {
				throw new Exception("PcEntity should be greater than zero!");
			}

			percents = percents.add(ae.getPcEntity());
			if (percents.compareTo(new BigDecimal("100.00")) > 0) {
				throw new Exception("Sum of percentages can not exceed 100!");
			}

			AccountEntity aec = null;
			if (ae.getCode() != null) {

				if (codes.contains(ae.getCode())) {
					throw new Exception("Duplicate records!");
				}
				codes.add(ae.getCode());

				aec = accountEntityRepository.findTop1ByCompanyAndId(company, ae.getCode());
				if (aec == null) {
					throw new Exception("AccountEntity not found with code " + ae.getCode() + "!");
				}
				boolean isAllowed = true;
				try {
					if (!account.getId().equals(aec.getAccount().getId())) {
						isAllowed = false;
					}
				} catch (Exception e) {
				}
				if (!isAllowed) {
					throw new Exception("You are trying to edit a record that does not belong to you!");
				}
			}

			if (aec != null) {
				entity = aec;
				entity.setUpdated(new Date());
				entity.setUpdatedBy(userCreating);
			}

			if (ae.getEntityCode() != null) {
				try {
					Entity e = entityRepository.findTop1ByCompanyAndIdAndIsactiveTrue(company, ae.getEntityCode());
					if (e == null) {
						throw new Exception("Entity not found! (" + ae.getEntityCode() + ")");
					}
					entity.setEntity(e);
				} catch (Exception e) {
					throw new Exception("EntityCode is required!");
				}
			} else {
				throw new Exception("EntityCode is required!");
			}

			entity.setCompany(company);
			entity.setCreatedBy(userCreating);
			entity.setAccount(account);

			entity.setPcEntity(ae.getPcEntity());
			entities.add(entity);
		}

		return entities;
	}

	public List<AccountEntity> getEntities(AppSession appSession) throws Exception {
		try {
			return accountService.getAccount(appSession).getEntities();
		} catch (Exception ignore) {
		}
		return new ArrayList<>();
	}

}
