package com.adaptaconsultoria.cashbackcore.services;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.CompanyConfig;
import com.adaptaconsultoria.cashbackcore.models.User;
import com.adaptaconsultoria.cashbackcore.objects.in.RecoveryPasswordPostIn;
import com.adaptaconsultoria.cashbackcore.objects.in.RecoveryPasswordRedefinePostIn;
import com.adaptaconsultoria.cashbackcore.repositories.CompanyConfigRepository;
import com.adaptaconsultoria.cashbackcore.repositories.UserRepository;

@Service
public class RecoveryPasswordService {

	@Autowired private AuthService authService;
	@Autowired private UserService userService;
	@Autowired private UserRepository userRepository;
	@Autowired private EmailAlcService emailAlcService;
	@Autowired private AppSessionService appSessionService;
	@Autowired private CompanyParamService companyParamService;
	@Autowired private CompanyConfigRepository companyConfigRepository;

	public void sendEmailRecovery(RecoveryPasswordPostIn in, AppSession appSession, String uri, HttpServletRequest req) throws Exception {

		if (StringUtils.isBlank(in.getEmail())) {
			throw new Exception("E-mail is required!");
		}

		Company company = appSession.getCompany();

		User user = userRepository.findTop1ByCompanyAndEmailOrderByEmail(company, in.getEmail());
		if (user == null) {
			throw new Exception("User not found!");
		}

		CompanyConfig config = companyConfigRepository.findTop1ByCompanyAndIsactiveTrue(company);
		if (config == null) {
			throw new Exception("Configuration not found!");
		}
		if (config.getApp() == null) {
			throw new Exception("App not found!");
		}

		String urlApp                 = companyParamService.getAppUrl(company);
		String endpointRemoteRecovery = companyParamService.getRemoteEndpointRecovery(company);

		AppSession recoverySession = authService.getAccessToken(config.getApp(), user, req.getRemoteAddr(), uri);

		String emailUrl = urlApp + endpointRemoteRecovery + "?token=" + recoverySession.getToken();
		// Enviar email via ALC
		emailAlcService.sendEmailRecovery(recoverySession, company, emailUrl, user.getEmail());

	}

	public AppSession redefinePassword(RecoveryPasswordRedefinePostIn in, AppSession appSession) throws Exception {
		if (StringUtils.isBlank(in.getNewPassword())) {
			throw new Exception("New Password is required!");
		}
		if (StringUtils.isBlank(in.getConfirmNewPassword())) {
			throw new Exception("Passwords do not match!");
		}
		if (!in.getNewPassword().equals(in.getConfirmNewPassword())) {
			throw new Exception("Passwords do not match!");
		}

		appSession.setUser(userService.redefinePassword(appSession.getUser(), in.getNewPassword()));

		return appSessionService.save(appSession);

	}

}
