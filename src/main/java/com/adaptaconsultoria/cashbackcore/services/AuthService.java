package com.adaptaconsultoria.cashbackcore.services;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.models.App;
import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.models.User;
import com.adaptaconsultoria.cashbackcore.objects.in.AuthPostIn;

@Service
public class AuthService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired private AppService appService;
	@Autowired private UserService userService;
	@Autowired private IPBlockedService ipBlockedService;
	@Autowired private AppSessionService appSessionService;

	public AppSession getAccessToken(AuthPostIn auth, String serviceName) throws Exception {
		return getAccessToken(auth.getAppToken(), auth.getAppPassword(), auth.getUserName(), auth.getUserPassword(), auth.getIpAddress(), serviceName);
	}
	public AppSession getAccessToken(String appToken, String appPassword, String userName, String userPassword, String ipAddress, String serviceName) throws Exception {

		App app = appService.checkAppLogin(appToken, appPassword);
		if (app == null) {
			throw new Exception("Invalid App!");
		}

		if (ipBlockedService.isIpAddressBlocked(ipAddress)) {
			throw new Exception("Access blocked!");
		}

		User user = null;
		if (StringUtils.isNotBlank(userName)) {
			user = userService.checkLogin(app.getCompany(), userName, userPassword);
			if (user == null) {
				throw new Exception("Invalid User!");
			}
			if (!user.getCompany().equals(app.getCompany())) {
				throw new Exception("Invalid User!");
			}
		}

		AppSession appSession = appSessionService.saveToken(app, user, ipAddress, serviceName);
		if (appSession == null) {
			throw new Exception("Token cannot be created!");
		}

		return appSession;
	}

	public AppSession getAccessToken(App app, User user, String ipAddress, String serviceName) throws Exception {
		AppSession appSession = appSessionService.saveToken(app, user, ipAddress, serviceName);
		if (appSession == null) {
			throw new Exception("Token cannot be created!");
		}
		return appSession;
	}

}
