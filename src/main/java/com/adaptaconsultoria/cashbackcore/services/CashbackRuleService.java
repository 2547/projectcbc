package com.adaptaconsultoria.cashbackcore.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.models.CashbackRule;
import com.adaptaconsultoria.cashbackcore.repositories.CashbackRuleRepository;

@Service
public class CashbackRuleService {

	@Autowired private CashbackRuleRepository cashbackRuleRepository;

	public CashbackRule save(CashbackRule cashbackRule) {
		return cashbackRuleRepository.save(cashbackRule);
	}

	@Transactional
	public void delete(Long cashbackRuleId) throws Exception {
		Optional<CashbackRule> cashbackRule = cashbackRuleRepository.findById(cashbackRuleId);
		if (cashbackRule.isPresent()) {
			cashbackRuleRepository.delete(cashbackRule.get());
		} else {
			throw new Exception("Cashback Rule not found!");
		}
	}

}
