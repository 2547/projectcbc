package com.adaptaconsultoria.cashbackcore.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.models.Payout;
import com.adaptaconsultoria.cashbackcore.repositories.PayoutRepository;

@Service
public class PayoutService {

	@Autowired private PayoutRepository payoutRepository;

	public Payout save(Payout payout) {
		return payoutRepository.save(payout);
	}

	@Transactional
	public void delete(Long payoutId) throws Exception {
		Optional<Payout> payout = payoutRepository.findById(payoutId);
		if (payout.isPresent()) {
			payoutRepository.delete(payout.get());
		} else {
			throw new Exception("Payout not found!");
		}
	}

}
