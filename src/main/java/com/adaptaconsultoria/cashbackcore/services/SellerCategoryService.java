package com.adaptaconsultoria.cashbackcore.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.models.SellerCategory;
import com.adaptaconsultoria.cashbackcore.repositories.SellerCategoryRepository;

@Service
public class SellerCategoryService {

	@Autowired private SellerCategoryRepository sellerCategoryRepository;

	public SellerCategory save(SellerCategory sellerCategory) {
		return sellerCategoryRepository.save(sellerCategory);
	}

	@Transactional
	public void delete(Long sellerCategoryId) throws Exception {
		Optional<SellerCategory> sellerCategory = sellerCategoryRepository.findById(sellerCategoryId);
		if (sellerCategory.isPresent()) {
			sellerCategoryRepository.delete(sellerCategory.get());
		} else {
			throw new Exception("Seller Category not found!");
		}
	}

}
