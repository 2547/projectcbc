package com.adaptaconsultoria.cashbackcore.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.models.Entity;
import com.adaptaconsultoria.cashbackcore.repositories.EntityRepository;

@Service
public class EntityService {

	@Autowired private EntityRepository entityRepository;

	public Entity save(Entity entity) {
		return entityRepository.save(entity);
	}

	@Transactional
	public void delete(Long entityId) throws Exception {
		Optional<Entity> entity = entityRepository.findById(entityId);
		if (entity.isPresent()) {
			entityRepository.delete(entity.get());
		} else {
			throw new Exception("Entity not found!");
		}
	}

}
