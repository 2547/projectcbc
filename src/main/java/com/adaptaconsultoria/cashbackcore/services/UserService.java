package com.adaptaconsultoria.cashbackcore.services;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.Country;
import com.adaptaconsultoria.cashbackcore.models.User;
import com.adaptaconsultoria.cashbackcore.objects.in.AccountPostIn;
import com.adaptaconsultoria.cashbackcore.repositories.UserRepository;

@Service
public class UserService {

	@Autowired private TokenService tokenService;
	@Autowired private UserRepository userRepository;

	public User save(User user) throws Exception {
		if (canSave(user)) {
			return userRepository.save(user);
		}
		return user;
	}

	public void remove(User user) {
		userRepository.delete(user);
	}

	public User checkLogin(Company company, String userLogin, String userPassword) throws Exception {
		User user = userRepository.findTop1ByCompanyAndLoginAndIsactiveTrueOrderByLogin(company, userLogin);
		if (user == null) {
			throw new Exception("User not found!");
		}
		if (!tokenService.compare(user.getPassword(), userPassword)) {
			throw new Exception("User not found!");
		}
		return user;
	}

	public User getUserCreating(AppSession appSession) throws Exception {
		if (appSession.getUser() != null) {
			return appSession.getUser();
		}

		Optional<User> userOpt = userRepository.findById(0L);

		if (userOpt.isPresent()) {
			User user = userOpt.get();
			return user;
		} else {
			throw new Exception("No user found!");
		}

	}

	public User createUser(AccountPostIn in, AppSession appSession, Country country) throws Exception {

		User userCreating = getUserCreating(appSession);

		User user = validateWithAccountPostIn(in, appSession);
		user.setCompany(appSession.getCompany());
		user.setCreatedBy(userCreating);
		user.setCountry(country);

		return user;

	}

	public User redefinePassword(User user, String password) throws Exception {

		user = userRepository.findById(user.getId()).get();

		user.setUpdated(new Date());
		user.setUpdatedBy(user);
		user.setPassword(tokenService.encrypt(password));

		user = save(user);

		return user;

	}

	private User validateWithAccountPostIn(AccountPostIn in, AppSession appSession) throws Exception {
		User user = new User();

		user.setFirstname(in.getFirstname());
		user.setLastname(in.getLastname());
		user.setName(in.getName());
		user.setLogin(in.getLogin());
		user.setPassword(tokenService.encrypt(in.getPassword()));
		user.setEmail(in.getEmail());
		user.setPhone(in.getPhone());

		return user;
	}

	public boolean canSave(User user) throws Exception {
		boolean ok = true;

		User userDB = userRepository.findTop1ByCompanyAndEmailOrderByEmail(user.getCompany(), user.getEmail());
		if (userDB != null) {
			if (!userDB.getId().equals(user.getId())) {
				throw new Exception("User with this e-mail already exists!");
			}
		}

		userDB = userRepository.findTop1ByCompanyAndLoginOrderByLogin(user.getCompany(), user.getLogin());
		if (userDB != null) {
			if (!userDB.getId().equals(user.getId())) {
				throw new Exception("User with this login already exists!");
			}
		}

		return ok;
	}

	public boolean userLoginExists(Company company, String login) throws Exception {
		User user = userRepository.findTop1ByCompanyAndLoginOrderByLogin(company, login);
		if (user != null) {
			return true;
		}
		return false;
	}

	public boolean userEmailExists(Company company, String email) throws Exception {
		User user = userRepository.findTop1ByCompanyAndEmailOrderByEmail(company, email);
		if (user != null) {
			return true;
		}
		return false;
	}

}