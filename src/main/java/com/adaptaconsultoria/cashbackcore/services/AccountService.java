package com.adaptaconsultoria.cashbackcore.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.models.Account;
import com.adaptaconsultoria.cashbackcore.models.Address;
import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.models.CompanyCountry;
import com.adaptaconsultoria.cashbackcore.models.User;
import com.adaptaconsultoria.cashbackcore.objects.in.AccountPostIn;
import com.adaptaconsultoria.cashbackcore.repositories.AccountRepository;
import com.adaptaconsultoria.cashbackcore.utils.EAN13;

@Service
public class AccountService {

	@Autowired private UserService userService;
	@Autowired private CountryService countryService;
	@Autowired private AddressService addressService;
	@Autowired private EmailAlcService emailAlcService;
	@Autowired private AccountRepository accountRepository;
	@Autowired private CompanyCountryService companyCountryService;

	@Transactional
	public Account save(Account account) {
		return accountRepository.save(account);
	}

	@Transactional
	public void delete(Long accountId) throws Exception {
		Optional<Account> account = accountRepository.findById(accountId);
		if (account.isPresent()) {
			User user = account.get().getUser();
			accountRepository.delete(account.get());
			userService.remove(user);
		} else {
			throw new Exception("Partner not found!");
		}
	}

	public Account createAccount(AccountPostIn in, AppSession appSession) throws Exception {

		User userCreating = userService.getUserCreating(appSession);

		Account account = validateCreate(in, appSession);

		boolean emailBemVindo = false;
		if (account.getId() == null) {
			account.setCreatedBy(userCreating);
			String accountNumber = StringUtils.leftPad(account.getCountry().getCode().toString(), 3, "0");
			accountNumber += StringUtils.leftPad(countryService.getNextAccount(account.getCountry()).toString(), 9, "0");
			accountNumber = EAN13.addDV(accountNumber);
			account.setAccountNo(accountNumber);
			emailBemVindo = true;
		}

		account.setTaxid(in.getTaxid());

		if (account.getUser() == null) {
			account.setUser(userService.createUser(in, appSession, account.getCountry()));
		} else {
			account.getUser().setFirstname(in.getFirstname());
			account.getUser().setLastname(in.getLastname());
			account.getUser().setName(in.getName());
			account.getUser().setIsoLanguage(in.getIsoLanguage());
			account.setUser(userService.save(account.getUser()));
		}

		Address address = addressService.createAddress(in, appSession);
		if (address != null) {
			account.setAddress(address);
		}

		try {
			account.setUser(userService.save(account.getUser()));
		} catch (Exception e) {
			throw new Exception("User could not be registered!\n"+e.getMessage());
		}

		try {
			if (account.getAddress() != null) {
				account.setAddress(addressService.save(account.getAddress()));
			}
		} catch (Exception ignore) {
		}

		account = save(account);

		if (emailBemVindo) {
			try {
				emailAlcService.sendEmailWellcome(account);
			} catch (Exception e) {
				System.out.println("\n\nWellcome e-mail could not be sent!\n");
			}
		}

		return account;

	}

	private Account validateCreate(AccountPostIn in, AppSession appSession) throws Exception {
		boolean editando = false;
		Account account = new Account();
		account.setCompany(appSession.getCompany());

		CompanyCountry companyCountry = null;

		try {
			if (StringUtils.isNotBlank(in.getAccountNo())) {
				account = accountRepository.findTop1ByCompanyAndAccountNo(appSession.getCompany(), in.getAccountNo());
				if (account.getId() != null) {
					editando = true;
				}
			}
		} catch (Exception ignore) {
			throw new Exception("Account not found!");
		}

		if (!editando) {

			if (StringUtils.isNotBlank(in.getCountryIsoCode())) {
				companyCountry = companyCountryService.getCompanyCountry(appSession.getCompany(), in.getCountryIsoCode());
				if (companyCountry == null) {
					throw new Exception("Country not found!");
				}
				account.setCurrency(companyCountry.getCurrency());
				account.setCountry(companyCountry.getCountry());
			} else {
				throw new Exception("CountryIsoCode is required!");
			}

			Account sponsorAccount = null;
			if (StringUtils.isNotBlank(in.getSponsorAccountNo())) {
				sponsorAccount = accountRepository.findTop1ByCompanyAndAccountNo(appSession.getCompany(), in.getSponsorAccountNo());
				try {
					if (sponsorAccount.getId() == null) {
						throw new Exception();
					}
					account.setSponsorAccount(sponsorAccount);
				} catch (Exception e) {
					throw new Exception("Sponsor Account not found!");
				}
			}

			if (companyCountry.getTaxidMandatory()) {
				if (StringUtils.isBlank(in.getTaxid())) {
					throw new Exception("Taxid is required!");
				}
			}
			if (StringUtils.isBlank(in.getEmail())) {
				throw new Exception("E-mail is required!");
			}
			if (StringUtils.isBlank(in.getLogin())) {
				in.setLogin(in.getEmail());
			}
			if (StringUtils.isBlank(in.getPassword())) {
				in.setPassword(in.getLogin());
			}

		}

		if (companyCountry != null) {
			if (companyCountry.getTaxidMandatory()) {
				if (StringUtils.isBlank(in.getTaxid()) && StringUtils.isBlank(account.getTaxid())) {
					throw new Exception("Taxid is required!");
				}
				if (StringUtils.isNotBlank(in.getTaxid())) {
					account.setTaxid(in.getTaxid());
				}
			}
		}

		if (StringUtils.isBlank(in.getName())) {
			if (StringUtils.isNotBlank(in.getFirstname())) {
				in.setName(in.getFirstname());
				if (StringUtils.isNotBlank(in.getLastname())) {
					in.setName(in.getName() + "" + in.getLastname());
				}
			}
		}

		return account;
	}

	public Account getAccount(AppSession appSession) throws Exception {
		return accountRepository.findTop1ByCompanyAndUser(appSession.getCompany(), appSession.getUser());
	}

}
