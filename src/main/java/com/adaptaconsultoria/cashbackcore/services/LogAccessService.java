package com.adaptaconsultoria.cashbackcore.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.models.LogAccess;
import com.adaptaconsultoria.cashbackcore.repositories.LogAccessRepository;

@Service
public class LogAccessService {

	@Autowired private LogAccessRepository logAcessoRepository;
	
	public LogAccess save(LogAccess logAcesso ) {
		return logAcessoRepository.save(logAcesso);
	}

}
