package com.adaptaconsultoria.cashbackcore.services;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.models.Account;
import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.models.CurrencyQuote;
import com.adaptaconsultoria.cashbackcore.models.Partner;
import com.adaptaconsultoria.cashbackcore.models.PartnerRule;
import com.adaptaconsultoria.cashbackcore.models.Sale;
import com.adaptaconsultoria.cashbackcore.models.User;
import com.adaptaconsultoria.cashbackcore.objects.in.SaleGetIn;
import com.adaptaconsultoria.cashbackcore.objects.in.SalePostIn;
import com.adaptaconsultoria.cashbackcore.repositories.AccountRepository;
import com.adaptaconsultoria.cashbackcore.repositories.CurrencyQuoteRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PartnerRepository;
import com.adaptaconsultoria.cashbackcore.repositories.SaleRepository;
import com.adaptaconsultoria.cashbackcore.utils.DateUtility;

@Service
public class SaleService {

	@Autowired private UserService userService;
	@Autowired private SaleRepository saleRepository;
	@Autowired private AccountRepository accountRepository;
	@Autowired private PartnerRepository partnerRepository;
	@Autowired private CurrencyQuoteRepository currencyQuoteRepository;

	public Sale save(Sale sale) {
		return saleRepository.save(sale);
	}

	public Sale createSale(SalePostIn in, AppSession appSession) throws Exception {

		User userCreating = userService.getUserCreating(appSession);

		Sale sale = validateCreate(in, appSession);
		sale.setCreatedBy(userCreating);
		sale.setDocumentno(in.getDocumentNo());

		return save(sale);

	}

	private Sale validateCreate(SalePostIn in, AppSession appSession) throws Exception {

		Sale sale = new Sale();
		sale.setCompany(appSession.getCompany());

		try {
			Account account = accountRepository.findTop1ByCompanyAndUser(appSession.getCompany(), appSession.getUser());
			if (account == null) {
				throw new Exception();
			}
			Partner p = partnerRepository.findTop1ByCompanyAndAccountNoAndSellerCategoryNotNull(appSession.getCompany(), account.getAccountNo());
			if (p == null) {
				throw new Exception();
			}
			sale.setPartner(p);
		} catch (Exception e) {
			throw new Exception("Permission denied!");
		}

		if (in.getSaleAmount() == null) {
			throw new Exception("SaleAmount is required!");
		} else if (in.getSaleAmount().compareTo(BigDecimal.ZERO) <= 0) {
			throw new Exception("SaleAmount should be greater than zero!");
		}

		if (in.getCodeRule() == null) {
			throw new Exception("CodeRule is required!");
		}
		if (sale.getPartner().getRules().size() <= 0) {
			throw new Exception("Partner without agreement rules!");
		}
		PartnerRule rule = null;
		for (PartnerRule r : sale.getPartner().getRules()) {
			if (r.getId().equals(in.getCodeRule())) {
				rule = r;
				break;
			}
		}
		if (rule == null) {
			throw new Exception("PartnerRule not found!");
		}

		sale.setSaleAmount(in.getSaleAmount());

		// PartnerRule
		sale.setSaleCurrency(rule.getCurrency());
		sale.setPcCashback(rule.getPcCashback());

		sale.setSaleCashback(in.getSaleAmount().multiply(sale.getPcCashback()).divide(new BigDecimal("100.00")).setScale(2, BigDecimal.ROUND_HALF_DOWN));

		if (in.getLocalTime() == null) {
			throw new Exception("LocalTime is required!");
		}

		sale.setLocalTime(in.getLocalTime());
		sale.setCountry(sale.getPartner().getAccount().getCountry());

		Account account = null;
		if (in.getCodeAccount() != null) {
			try {
				account = accountRepository.findTop1ByCompanyAndId(appSession.getCompany(), in.getCodeAccount());
			} catch (Exception e) {
			}
		}

		if (account == null) {
			try {
				if (StringUtils.isNoneBlank(in.getAccountNo())) {
					account = accountRepository.findTop1ByCompanyAndAccountNo(appSession.getCompany(), in.getAccountNo());
				}
			} catch (Exception e) {
			}
		}

		if (account == null) {
			try {
				if (StringUtils.isNoneBlank(in.getTaxid())) {
					account = accountRepository.findTop1ByCompanyAndCountryAndTaxidIgnoreCase(appSession.getCompany(), sale.getCountry(), in.getTaxid());
				}
			} catch (Exception e) {
			}
		}

		if (account == null) {
			try {
				if (StringUtils.isNoneBlank(in.getPhone())) {
					account = accountRepository.findTop1ByCompanyAndCountryAndPhone(appSession.getCompany(), sale.getCountry(), in.getPhone());
				}
			} catch (Exception e) {
			}
		}

		if (account == null) {
			throw new Exception("Account not found!");
		}

		sale.setAccountNo(in.getAccountNo());
		sale.setTaxid(in.getTaxid());
		sale.setPhone(in.getPhone());
		sale.setAccount(account);
		sale.setCurrency(sale.getAccount().getCurrency());

		if (sale.getCurrency().getId().equals(sale.getSaleCurrency().getId())) {
			sale.setCashback(sale.getSaleCashback());
		} else {
			CurrencyQuote quote = currencyQuoteRepository.findTop1ByCompanyAndCurrencyAndQuotedCurrencyAndIsactiveTrueOrderByQuotedDesc(appSession.getCompany(), sale.getSaleCurrency(), sale.getCurrency());
			if (quote == null) {
				throw new Exception("Currency quote not found!");
			}
			sale.setCashback(sale.getSaleCashback().multiply(quote.getLastPrice()).setScale(2, BigDecimal.ROUND_HALF_DOWN));
			sale.setQuoted(quote.getQuoted());
			sale.setQuotedat(quote.getLastPrice());
		}

		return sale;
	}

	public List<Sale> getSales(SaleGetIn in, AppSession appSession) {
		Account account = accountRepository.findTop1ByCompanyAndUser(appSession.getCompany(), appSession.getUser());
		if (in.getDateStart() == null) {
			return saleRepository.findByCompanyAndAccount(appSession.getCompany(), account);
		}
		Date dateStart = DateUtility.lowDateTime(DateUtility.formatDate(in.getDateStart()));
		if (in.getDateEnd() == null) {
			return saleRepository.findByCompanyAndAccountAndLocalTimeGreaterThanEqual(appSession.getCompany(), account, dateStart);
		}
		Date dateEnd = DateUtility.highDateTime(DateUtility.formatDate(in.getDateEnd()));
		return saleRepository.findByCompanyAndAccountAndLocalTimeGreaterThanEqualAndLocalTimeLessThanEqual(appSession.getCompany(), account, dateStart, dateEnd);
	}

	public List<Sale> getSalesSeller(SaleGetIn in, AppSession appSession) throws Exception {
		Account account = accountRepository.findTop1ByCompanyAndUser(appSession.getCompany(), appSession.getUser());

		Partner partner = null;
		try {
			partner = partnerRepository.findTop1ByCompanyAndAccountNoAndSellerCategoryNotNull(appSession.getCompany(), account.getAccountNo());
			if (partner == null) {
				throw new Exception();
			}
		} catch (Exception e) {
			throw new Exception("Permission denied!");
		}

		if (in.getDateStart() == null) {
			return saleRepository.findByCompanyAndPartner(appSession.getCompany(), partner);
		}
		Date dateStart = DateUtility.lowDateTime(DateUtility.formatDate(in.getDateStart()));
		if (in.getDateEnd() == null) {
			return saleRepository.findByCompanyAndPartnerAndLocalTimeGreaterThanEqual(appSession.getCompany(), partner, dateStart);
		}
		Date dateEnd = DateUtility.highDateTime(DateUtility.formatDate(in.getDateEnd()));
		return saleRepository.findByCompanyAndPartnerAndLocalTimeGreaterThanEqualAndLocalTimeLessThanEqual(appSession.getCompany(), partner, dateStart, dateEnd);
	}

}
