package com.adaptaconsultoria.cashbackcore.services;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.models.Account;
import com.adaptaconsultoria.cashbackcore.models.AccountPayout;
import com.adaptaconsultoria.cashbackcore.models.AccountPayoutEvent;
import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.models.User;
import com.adaptaconsultoria.cashbackcore.models.enumeration.EventType;
import com.adaptaconsultoria.cashbackcore.objects.in.AccountPayoutEventPostIn;
import com.adaptaconsultoria.cashbackcore.repositories.AccountPayoutEventRepository;
import com.adaptaconsultoria.cashbackcore.repositories.AccountPayoutRepository;

@Service
public class AccountPayoutEventService {

	@Autowired private UserService userService;
	@Autowired private AccountService accountService;
	@Autowired private AccountPayoutRepository accountPayoutRepository;
	@Autowired private AccountPayoutEventRepository accountPayoutEventRepository;

	public AccountPayoutEvent save(AccountPayoutEvent event) {
		return accountPayoutEventRepository.save(event);
	}

	public AccountPayoutEvent blockAccountPayoutEvent(AccountPayoutEventPostIn in, AppSession appSession) throws Exception {
		AccountPayoutEvent payout = validateCreate(in, appSession);
		return save(payout);
	}

	private AccountPayoutEvent validateCreate(AccountPayoutEventPostIn in, AppSession appSession) throws Exception {

		User userCreating = userService.getUserCreating(appSession);

		AccountPayoutEvent event = new AccountPayoutEvent();

		if (in.getAccountPayoutCode() == null) {
			throw new Exception("AccountPayoutCode is required!");
		}
		try {
			AccountPayout payout = accountPayoutRepository.findTop1ByCompanyAndId(appSession.getCompany(), in.getAccountPayoutCode());
			if (payout == null) {
				throw new Exception();
			}
			event.setAccountPayout(payout);
		} catch (Exception e) {
			throw new Exception("AccountPayout not found!");
		}

		Account account = accountService.getAccount(appSession);
		if (!event.getAccountPayout().getAccount().getId().equals(account.getId())) {
			throw new Exception("Permission denied!");
		}

		event.setCompany(appSession.getCompany());
		event.setCreatedBy(userCreating);
		event.setUpdatedBy(userCreating);
		event.setUpdated(new Date());

		if (StringUtils.isBlank(in.getDescription())) {
			throw new Exception("Description is required!");
		}

		String description = "WS Blocked: ";
		description += in.getDescription();
		event.setDescription(description);

		event.setEventType(EventType.B);

		return event;
	}

}
