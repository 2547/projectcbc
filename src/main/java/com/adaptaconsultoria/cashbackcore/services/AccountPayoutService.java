package com.adaptaconsultoria.cashbackcore.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.models.Account;
import com.adaptaconsultoria.cashbackcore.models.AccountPayout;
import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.models.Payout;
import com.adaptaconsultoria.cashbackcore.models.User;
import com.adaptaconsultoria.cashbackcore.objects.in.AccountPayoutPostIn;
import com.adaptaconsultoria.cashbackcore.repositories.AccountPayoutRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PayoutRepository;

@Service
public class AccountPayoutService {

	@Autowired private UserService userService;
	@Autowired private AccountService accountService;
	@Autowired private PayoutRepository payoutRepository;
	@Autowired private AccountPayoutRepository accountPayoutRepository;

	public AccountPayout save(AccountPayout accountPayout) {
		return accountPayoutRepository.save(accountPayout);
	}

	public AccountPayout createAccountPayout(AccountPayoutPostIn in, AppSession appSession) throws Exception {
		AccountPayout payout = validateCreate(in, appSession);
		return save(payout);
	}

	private AccountPayout validateCreate(AccountPayoutPostIn in, AppSession appSession) throws Exception {

		User userCreating = userService.getUserCreating(appSession);

		AccountPayout payout = new AccountPayout();
		try {
			if (in.getCode() != null) {
				payout = accountPayoutRepository.findTop1ByCompanyAndId(appSession.getCompany(), in.getCode());
				if (payout == null) {
					throw new Exception();
				}
			}
		} catch (Exception e) {
			throw new Exception("AccountPayout not found!");
		}

		Account account = accountService.getAccount(appSession);
		if (payout.getId() == null) {
			payout.setCompany(appSession.getCompany());
			payout.setCreatedBy(userCreating);
			payout.setAccount(account);
		} else {
			payout.setUpdatedBy(userCreating);
			payout.setUpdated(new Date());
			if (!payout.getAccount().getId().equals(account.getId())) {
				throw new Exception("Permission denied!");
			}
		}

		if (in.getPayoutCode() == null) {
			throw new Exception("PayoutCode is required!");
		}

		try {
			Payout p = payoutRepository.findTop1ByCompanyAndId(appSession.getCompany(), in.getPayoutCode());
			if (p == null) {
				throw new Exception();
			}
			payout.setPayout(p);
		} catch (Exception e) {
			throw new Exception("Payout not found!");
		}

		if (payout.getPayout().getValueRequired()) {
			if (StringUtils.isBlank(in.getValue())) {
				throw new Exception("Value is required for this Payout!");
			}
			payout.setValue(in.getValue());
		}

		return payout;
	}

	public List<AccountPayout> getAccountPayouts(AppSession appSession) throws Exception {
		try {
			Account account = accountService.getAccount(appSession);
			return accountPayoutRepository.findByCompanyAndAccount(account.getCompany(), account);
		} catch (Exception ignore) {
		}
		return new ArrayList<>();
	}

}
