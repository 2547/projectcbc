package com.adaptaconsultoria.cashbackcore.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.models.Account;
import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.models.Cashback;
import com.adaptaconsultoria.cashbackcore.objects.in.CashbackGetIn;
import com.adaptaconsultoria.cashbackcore.repositories.AccountRepository;
import com.adaptaconsultoria.cashbackcore.repositories.CashbackRepository;
import com.adaptaconsultoria.cashbackcore.utils.DateUtility;

@Service
public class CashbackService {

	@Autowired private AccountRepository accountRepository;
	@Autowired private CashbackRepository cashbackRepository;

	public List<Cashback> getCashbacks(CashbackGetIn in, AppSession appSession) {
		Account account = accountRepository.findTop1ByCompanyAndUser(appSession.getCompany(), appSession.getUser());
		if (in.getDateStart() == null) {
			return cashbackRepository.findByCompanyAndAccount(appSession.getCompany(), account);
		}
		Date dateStart = DateUtility.lowDateTime(DateUtility.formatDate(in.getDateStart()));
		if (in.getDateEnd() == null) {
			return cashbackRepository.findByCompanyAndAccountAndDateSaleGreaterThanEqual(appSession.getCompany(), account, dateStart);
		}
		Date dateEnd = DateUtility.highDateTime(DateUtility.formatDate(in.getDateEnd()));
		return cashbackRepository.findByCompanyAndAccountAndDateSaleGreaterThanEqualAndDateSaleLessThanEqual(appSession.getCompany(), account, dateStart, dateEnd);
	}

}
