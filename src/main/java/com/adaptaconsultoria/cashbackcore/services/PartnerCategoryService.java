package com.adaptaconsultoria.cashbackcore.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.models.PartnerCategory;
import com.adaptaconsultoria.cashbackcore.repositories.PartnerCategoryRepository;

@Service
public class PartnerCategoryService {

	@Autowired private PartnerCategoryRepository partnerCategoryRepository;

	public PartnerCategory save(PartnerCategory partnerCategory) {
		return partnerCategoryRepository.save(partnerCategory);
	}

	@Transactional
	public void delete(Long partnerCategoryId) throws Exception {
		Optional<PartnerCategory> partnerCategory = partnerCategoryRepository.findById(partnerCategoryId);
		if (partnerCategory.isPresent()) {
			partnerCategoryRepository.delete(partnerCategory.get());
		} else {
			throw new Exception("Partner Category not found!");
		}
	}

}
