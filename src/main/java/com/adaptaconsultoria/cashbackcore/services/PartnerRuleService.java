package com.adaptaconsultoria.cashbackcore.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.repositories.PartnerRuleRepository;

@Service
public class PartnerRuleService {
	
	@Autowired 
	private PartnerRuleRepository partnerRuleRepository;
	
	public Object remove(Long id) {
		try {
			partnerRuleRepository.deleteById(id);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
