package com.adaptaconsultoria.cashbackcore.services.ctrls;

import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.PaymentMethod;
import com.adaptaconsultoria.cashbackcore.models.User;
import com.adaptaconsultoria.cashbackcore.repositories.DocSequenceRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PaymentMethodRepository;
import com.adaptaconsultoria.cashbackcore.services.PaymentMethodService;
import com.adaptaconsultoria.cashbackcore.utils.PageUtil;
import com.adaptaconsultoria.cashbackcore.utils.SessionUtility;

@Service
public class PaymentMethodControllerService {

	@Autowired private PaymentMethodService paymentMethodService;
	@Autowired private DocSequenceRepository docSequenceRepository;
	@Autowired private PaymentMethodRepository paymentMethodRepository;

	public ModelAndView getList() {
		PageUtil pageUtil = new PageUtil("paymentmethod/list");
		pageUtil.setURI("/paymentmethod");
		pageUtil.setPageTitle("Payment Method");
		pageUtil.setTitle("Payment Method");
		pageUtil.setInnerTitle("Payment Method");
		pageUtil.setText("Double click to edit.");
		pageUtil.setSubTitle("List");
		pageUtil.setTableId("paymentmethod-list-table");
		pageUtil.setJs("paymentmethod-list.js");
		pageUtil.setAttr("menuId", "paymentmethod-menu");
		return pageUtil.getModel();
	}

	public ModelAndView getPost(Long id) {
		PageUtil pageUtil = new PageUtil("paymentmethod/form");
		pageUtil.setURI("/paymentmethod");
		pageUtil.setPageTitle("Payment Method");
		pageUtil.setTitle("Payment Method");
		pageUtil.setInnerTitle("Payment Method");
		pageUtil.setFormId("paymentmethod-form");
		pageUtil.setJs("paymentmethod.js");
		pageUtil.setAttr("commandName", "obj");
		pageUtil.setAttr("mi", "paymentmethodlist");
		pageUtil.setAttr("menuId", "paymentmethod-menu");

		PaymentMethod obj = new PaymentMethod();
		Company company = SessionUtility.getUserLoggedin().getCompany();

		if (id != null) {
			pageUtil.setSubTitle("Edit");
			try {
				obj = paymentMethodRepository.findTop1ByCompanyAndId(company, id);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			pageUtil.setSubTitle("New");
		}

		pageUtil.setAttr("docSequences", docSequenceRepository.findByCompany(company));
		pageUtil.setAttr("paymentMethods", paymentMethodRepository.findByCompany(company));

		pageUtil.setAttr("obj", obj);
		pageUtil.setCommandName("paymentmethod");

		return pageUtil.getModel();
	}

	public HashMap<Object, Object> save(PaymentMethod paymentMethod) {
		HashMap<Object, Object> map = new HashMap<>();
		try {

			User user = SessionUtility.getUserLoggedin();
			Boolean isEditing = paymentMethod.getId() != null;

			PaymentMethod ps = new PaymentMethod();
			if (isEditing) {
				ps = paymentMethodRepository.findById(paymentMethod.getId()).get();
				ps.setUpdatedBy(user);
				ps.setUpdated(new Date());
			} else {
				ps.setCompany(user.getCompany());
				ps.setCreatedBy(user);
			}
			ps.setIsactive(paymentMethod.getIsactive());
			ps.setName(paymentMethod.getName());
			ps.setDescription(paymentMethod.getDescription());
			ps.setFee(paymentMethod.getFee());
			ps.setPcFee(paymentMethod.getPcFee());

			ps = paymentMethodService.save(ps);
			map.put("sucesso", true);
			map.put("obj", ps);
			map.put("message", "Success!");
			map.put("isEditing", isEditing);
		} catch (Exception e) {
			map.put("sucesso", false);
			map.put("obj", paymentMethod);
			map.put("message", e.getLocalizedMessage());
		}
		return map;
	}

	public boolean delete(Long id) {
		try {
			paymentMethodService.delete(id);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
