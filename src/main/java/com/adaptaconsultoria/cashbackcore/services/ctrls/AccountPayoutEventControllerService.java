package com.adaptaconsultoria.cashbackcore.services.ctrls;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.cashbackcore.models.AccountPayoutEvent;
import com.adaptaconsultoria.cashbackcore.models.User;
import com.adaptaconsultoria.cashbackcore.models.enumeration.EventType;
import com.adaptaconsultoria.cashbackcore.services.AccountPayoutEventService;
import com.adaptaconsultoria.cashbackcore.utils.PageUtil;
import com.adaptaconsultoria.cashbackcore.utils.SessionUtility;

@Service
public class AccountPayoutEventControllerService {

	@Autowired private AccountPayoutEventService accountPayoutEventService;

	public ModelAndView getPost() {
		PageUtil pageUtil = new PageUtil("payoutevent/form");
		pageUtil.setURI("/payoutevent");
		pageUtil.setPageTitle("Account Payout Event");
		pageUtil.setTitle("Account Payout Event");
		pageUtil.setInnerTitle("Accoutn Payout Event");
		pageUtil.setFormId("payoutevent-form");
		pageUtil.setJs("payoutevent.js");
		pageUtil.setAttr("commandName", "obj");
		pageUtil.setAttr("mi", "payouteventlist");
		pageUtil.setAttr("menuId", "payout-event-menu");
		pageUtil.setSubTitle("New");
		pageUtil.setAttr("obj", new AccountPayoutEvent());
		pageUtil.setCommandName("payoutevent");

		pageUtil.setAttr("eventTypes", EventType.values());

		return pageUtil.getModel();
	}

	public HashMap<Object, Object> save(AccountPayoutEvent payoutEvent) {
		HashMap<Object, Object> map = new HashMap<>();
		try {

			User user = SessionUtility.getUserLoggedin();

			AccountPayoutEvent ps = new AccountPayoutEvent();
			ps.setCompany(user.getCompany());
			ps.setCreatedBy(user);
			ps.setIsactive(payoutEvent.getIsactive());
			ps.setDescription(payoutEvent.getDescription());
			ps.setAccountPayout(payoutEvent.getAccountPayout());
			ps.setEventType(payoutEvent.getEventType());

			ps = accountPayoutEventService.save(ps);
			map.put("sucesso", true);
			map.put("obj", ps);
			map.put("message", "Success!");
			map.put("isEditing", false);
		} catch (Exception e) {
			map.put("sucesso", false);
			map.put("obj", payoutEvent);
			map.put("message", e.getLocalizedMessage());
		}
		return map;
	}

}
