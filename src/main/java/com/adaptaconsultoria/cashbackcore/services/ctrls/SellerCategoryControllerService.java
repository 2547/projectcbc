package com.adaptaconsultoria.cashbackcore.services.ctrls;

import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.SellerCategory;
import com.adaptaconsultoria.cashbackcore.models.User;
import com.adaptaconsultoria.cashbackcore.repositories.SellerCategoryRepository;
import com.adaptaconsultoria.cashbackcore.services.SellerCategoryService;
import com.adaptaconsultoria.cashbackcore.utils.PageUtil;
import com.adaptaconsultoria.cashbackcore.utils.SessionUtility;

@Service
public class SellerCategoryControllerService {

	@Autowired private SellerCategoryService sellerCategoryService;
	@Autowired private SellerCategoryRepository sellerCategoryRepository;

	public ModelAndView getList() {
		PageUtil pageUtil = new PageUtil("sellercategory/list");
		pageUtil.setURI("/sellercategory");
		pageUtil.setPageTitle("Seller Category");
		pageUtil.setTitle("Seller Category");
		pageUtil.setInnerTitle("Seller Category");
		pageUtil.setText("Double click to edit.");
		pageUtil.setSubTitle("List");
		pageUtil.setTableId("sellercategory-list-table");
		pageUtil.setJs("sellercategory-list.js");
		pageUtil.setAttr("menuId", "sellercategory-menu");
		return pageUtil.getModel();
	}

	public ModelAndView getPost(Long id) {
		PageUtil pageUtil = new PageUtil("sellercategory/form");
		pageUtil.setURI("/sellercategory");
		pageUtil.setPageTitle("Seller Category");
		pageUtil.setTitle("Seller Category");
		pageUtil.setInnerTitle("Seller Category");
		pageUtil.setFormId("sellercategory-form");
		pageUtil.setJs("sellercategory.js");
		pageUtil.setAttr("commandName", "obj");
		pageUtil.setAttr("mi", "sellercategorylist");
		pageUtil.setAttr("menuId", "sellercategory-menu");

		SellerCategory obj = new SellerCategory();
		Company company = SessionUtility.getUserLoggedin().getCompany();

		if (id != null) {
			pageUtil.setSubTitle("Edit");
			try {
				obj = sellerCategoryRepository.findTop1ByCompanyAndId(company, id);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			pageUtil.setSubTitle("New");
		}

		pageUtil.setAttr("obj", obj);
		pageUtil.setCommandName("sellerCategory");

		return pageUtil.getModel();
	}

	public HashMap<Object, Object> save(SellerCategory sellerCategory) {
		HashMap<Object, Object> map = new HashMap<>();
		try {

			User user = SessionUtility.getUserLoggedin();
			Boolean isEditing = sellerCategory.getId() != null;

			SellerCategory ps = new SellerCategory();
			if (isEditing) {
				ps = sellerCategoryRepository.findById(sellerCategory.getId()).get();
				ps.setUpdatedBy(user);
				ps.setUpdated(new Date());
			} else {
				ps.setCompany(user.getCompany());
				ps.setCreatedBy(user);
			}
			ps.setIsactive(sellerCategory.getIsactive());
			ps.setName(sellerCategory.getName());
			ps.setDescription(sellerCategory.getDescription());

			ps = sellerCategoryService.save(ps);
			map.put("sucesso", true);
			map.put("obj", ps);
			map.put("message", "Success!");
			map.put("isEditing", isEditing);
		} catch (Exception e) {
			map.put("sucesso", false);
			map.put("obj", sellerCategory);
			map.put("message", e.getLocalizedMessage());
		}
		return map;
	}

	public boolean delete(Long id) {
		try {
			sellerCategoryService.delete(id);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
