package com.adaptaconsultoria.cashbackcore.services.ctrls;

import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.cashbackcore.models.Address;
import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.Partner;
import com.adaptaconsultoria.cashbackcore.models.PartnerRule;
import com.adaptaconsultoria.cashbackcore.models.User;
import com.adaptaconsultoria.cashbackcore.repositories.CurrencyRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PartnerCategoryRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PartnerRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PartnerRuleRepository;
import com.adaptaconsultoria.cashbackcore.repositories.SellerCategoryRepository;
import com.adaptaconsultoria.cashbackcore.services.AddressService;
import com.adaptaconsultoria.cashbackcore.services.PartnerService;
import com.adaptaconsultoria.cashbackcore.utils.PageUtil;
import com.adaptaconsultoria.cashbackcore.utils.SessionUtility;

@Service
public class PartnerControllerService {

	@Autowired
	private AddressService addressService;
	@Autowired
	private PartnerService partnerService;
	@Autowired
	private PartnerRepository partnerRepository;
	@Autowired
	private CurrencyRepository currencyRepository;
	@Autowired
	private SellerCategoryRepository sellerCategoryRepository;
	@Autowired
	private PartnerCategoryRepository partnerCategoryRepository;
	@Autowired
	private PartnerRuleRepository partnerRuleRepository;

	public ModelAndView getPartnerList() {
		PageUtil pageUtil = new PageUtil("partner/list");
		pageUtil.setURI("/partner");
		pageUtil.setPageTitle("Partner");
		pageUtil.setTitle("Partner");
		pageUtil.setInnerTitle("Partner");
		pageUtil.setText("Double click to edit.");
		pageUtil.setSubTitle("List");
		pageUtil.setTableId("partner-list-table");
		pageUtil.setJs("partner-list.js");
		pageUtil.setAttr("menuId", "partner-menu");
		return pageUtil.getModel();
	}

	public ModelAndView getPartnerPost(Long id) {
		PageUtil pageUtil = new PageUtil("partner/form");
		pageUtil.setURI("/partner");
		pageUtil.setPageTitle("Partner");
		pageUtil.setTitle("Partner");
		pageUtil.setInnerTitle("Partner");
		pageUtil.setFormId("partner-form");
		pageUtil.setJs("partner.js");

		pageUtil.setAttr("commandName", "obj");
		pageUtil.setAttr("mi", "partnerlist");
		pageUtil.setAttr("menuId", "partner-menu");

		Partner obj = new Partner();
		Company company = SessionUtility.getUserLoggedin().getCompany();

		if (id != null) {
			pageUtil.setSubTitle("Edit");
			try {
				obj = partnerRepository.findTop1ByCompanyAndIdAndPartner(company, id);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			pageUtil.setSubTitle("New");
		}

		pageUtil.setAttr("categories", partnerCategoryRepository.findByCompany(company));

		pageUtil.setAttr("obj", obj);
		pageUtil.setCommandName("partner");

		return pageUtil.getModel();
	}

	public ModelAndView getSellerList() {
		PageUtil pageUtil = new PageUtil("seller/list");
		pageUtil.setURI("/seller");
		pageUtil.setPageTitle("Seller");
		pageUtil.setTitle("Seller");
		pageUtil.setInnerTitle("Seller");
		pageUtil.setText("Double click to edit.");
		pageUtil.setSubTitle("List");
		pageUtil.setTableId("seller-list-table");

		pageUtil.setJs("seller-list.js");
		pageUtil.setAttr("menuId", "seller-menu");
		return pageUtil.getModel();
	}

	public ModelAndView getSellerPost(Long id) {
		PageUtil pageUtil = new PageUtil("seller/form");
		pageUtil.setURI("/seller");
		pageUtil.setPageTitle("Seller");
		pageUtil.setTitle("Seller");
		pageUtil.setInnerTitle("Seller");
		pageUtil.setFormId("seller-form");
		pageUtil.setJs("seller.js");
		pageUtil.setTableId("seller-percentage-list-table");
		pageUtil.setAttr("commandName", "obj");
		pageUtil.setAttr("mi", "sellerlist");
		pageUtil.setAttr("menuId", "seller-menu");

		Partner obj = new Partner();
		Company company = SessionUtility.getUserLoggedin().getCompany();

		if (id != null) {
			pageUtil.setSubTitle("Edit");
			try {
				obj = partnerRepository.findTop1ByCompanyAndIdAndSeller(company, id);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			pageUtil.setSubTitle("New");
		}

		pageUtil.setAttr("categories", sellerCategoryRepository.findByCompany(company));
		pageUtil.setAttr("currencies", currencyRepository.findByCompany(company));

		pageUtil.setAttr("obj", obj);
		pageUtil.setCommandName("partner");

		return pageUtil.getModel();
	}

	public HashMap<Object, Object> saveSeller(Partner seller) {
		seller.setPartnerCategory(null);
		return save(seller, true);
	}

	public HashMap<Object, Object> savePartner(Partner partner) {
		partner.setSellerCategory(null);
		return save(partner, false);
	}

	public Object saveSellerRule(PartnerRule partnerRule, Long sellerId) {
		try {
	
			Partner s = partnerRepository.findById(sellerId).get();
			partnerRule.setCompany(s.getCompany());
			partnerRule.setCreatedBy(s.getCreatedBy());
			partnerRule.setPartner(s);
			partnerRuleRepository.save(partnerRule);
			
			
//			partnerRepository.findById(sellerId).ifPresent( s -> {
//				partnerRule.setCompany(s.getCompany());
//				partnerRule.setCreatedBy(s.getCreatedBy());
//				partnerRule.setPartner(s);
//				partnerRuleRepository.save(partnerRule);
//			});
//			
			
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	private HashMap<Object, Object> save(Partner partner, boolean isSeller) {
		HashMap<Object, Object> map = new HashMap<>();
		try {

			User user = SessionUtility.getUserLoggedin();
			Boolean isEditing = partner.getId() != null;

			Partner leader = null;
			try {
				leader = partnerRepository.findById(partner.getLeader().getId()).get();
			} catch (Exception ignore) {
			}

			Partner ps = new Partner();
			Address address = new Address();
			if (isEditing) {
				ps = partnerRepository.findById(partner.getId()).get();
				ps.setUpdatedBy(user);
				ps.setUpdated(new Date());
				ps.setIsactive(partner.getIsactive());
				ps.setName(partner.getName());
				ps.setDescription(partner.getDescription());
				ps.setUrl(partner.getUrl());
				ps.setEmail(partner.getEmail());
				ps.setPhone(partner.getPhone());
				if (isSeller) {
					ps.getRules().clear();
					ps.setSellerCategory(partner.getSellerCategory());
					for (PartnerRule pRule : partner.getRules()) {
						PartnerRule rule = new PartnerRule();
						rule.setCompany(ps.getCompany());
						rule.setCreatedBy(user);
						rule.setUpdatedBy(user);
						rule.setUpdated(new Date());
						rule.setPartner(ps);
						rule.setDescription(pRule.getDescription());
						rule.setCurrency(pRule.getCurrency());
						rule.setPcCashback(pRule.getPcCashback());
						ps.getRules().add(rule);
					}
				} else {
					ps.setPartnerCategory(partner.getPartnerCategory());
				}

				address = partner.getAddress();
				if (ps.getAddress() == null) {
					address.setCompany(ps.getCompany());
					address.setCreatedBy(user);
				} else {
					address.setId(ps.getAddress().getId());
					address.setCompany(ps.getCompany());
					address.setCreatedBy(ps.getCreatedBy());
				}
				address.setUpdatedBy(user);
				address.setUpdated(new Date());

				address = addressService.save(address);

				ps.setAddress(address);
			} else {
				partner.setCompany(user.getCompany());
				partner.setCreatedBy(user);
				if (isSeller) {
					ps = partnerService.createSeller(partner);
				} else {
					ps = partnerService.createPartner(partner);
				}
			}

			ps.setLeader(leader);

			ps = partnerService.save(ps);
			map.put("sucesso", true);
			map.put("obj", ps);
			map.put("message", "Success!");
			map.put("isEditing", isEditing);
		} catch (Exception e) {
			map.put("sucesso", false);
			map.put("obj", partner);
			map.put("message", e.getLocalizedMessage());
		}
		return map;
	}

	public boolean delete(Long id) {
		try {
			partnerService.delete(id);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
