package com.adaptaconsultoria.cashbackcore.services.ctrls;

import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.CashbackRule;
import com.adaptaconsultoria.cashbackcore.models.User;
import com.adaptaconsultoria.cashbackcore.models.enumeration.Beneficiary;
import com.adaptaconsultoria.cashbackcore.repositories.BonusRepository;
import com.adaptaconsultoria.cashbackcore.repositories.CashbackRuleRepository;
import com.adaptaconsultoria.cashbackcore.services.CashbackRuleService;
import com.adaptaconsultoria.cashbackcore.utils.PageUtil;
import com.adaptaconsultoria.cashbackcore.utils.SessionUtility;

@Service
public class CashbackRuleControllerService {

	@Autowired private BonusRepository bonusRepository;
	@Autowired private CashbackRuleService cashbackRuleService;
	@Autowired private CashbackRuleRepository cashbackRuleRepository;

	public ModelAndView getList() {
		PageUtil pageUtil = new PageUtil("cashbackrule/list");
		pageUtil.setURI("/cashbackrule");
		pageUtil.setPageTitle("Cashback Rule");
		pageUtil.setTitle("Cashback Rule");
		pageUtil.setInnerTitle("Cashback Rule");
		pageUtil.setText("Double click to edit.");
		pageUtil.setSubTitle("List");
		pageUtil.setTableId("cashbackrule-list-table");
		pageUtil.setJs("cashbackrule-list.js");
		pageUtil.setAttr("menuId", "cashbackrule-menu");
		return pageUtil.getModel();
	}

	public ModelAndView getPost(Long id) {
		PageUtil pageUtil = new PageUtil("cashbackrule/form");
		pageUtil.setURI("/cashbackrule");
		pageUtil.setPageTitle("Cashback Rule");
		pageUtil.setTitle("Cashback Rule");
		pageUtil.setInnerTitle("Cashback Rule");
		pageUtil.setFormId("cashbackrule-form");
		pageUtil.setJs("cashbackrule.js");
		pageUtil.setAttr("commandName", "obj");
		pageUtil.setAttr("mi", "cashbackrulelist");
		pageUtil.setAttr("menuId", "cashbackrule-menu");

		CashbackRule obj = new CashbackRule();
		Company company = SessionUtility.getUserLoggedin().getCompany();

		if (id != null) {
			pageUtil.setSubTitle("Edit");
			try {
				obj = cashbackRuleRepository.findTop1ByCompanyAndId(company, id);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			pageUtil.setSubTitle("New");
		}

		pageUtil.setAttr("bonuss", bonusRepository.findByCompany(company));
		pageUtil.setAttr("beneficiaries", Beneficiary.values());

		pageUtil.setAttr("obj", obj);
		pageUtil.setCommandName("cashbackrule");

		return pageUtil.getModel();
	}

	public HashMap<Object, Object> save(CashbackRule cashbackRule) {
		HashMap<Object, Object> map = new HashMap<>();
		try {

			User user = SessionUtility.getUserLoggedin();
			Boolean isEditing = cashbackRule.getId() != null;

			CashbackRule ps = new CashbackRule();
			if (isEditing) {
				ps = cashbackRuleRepository.findById(cashbackRule.getId()).get();
				ps.setUpdatedBy(user);
				ps.setUpdated(new Date());
			} else {
				ps.setCompany(user.getCompany());
				ps.setCreatedBy(user);
			}
			ps.setIsactive(cashbackRule.getIsactive());
			ps.setBonus(cashbackRule.getBonus());
			ps.setBeneficiary(cashbackRule.getBeneficiary());
			ps.setLevel(cashbackRule.getLevel());
			ps.setRate(cashbackRule.getRate());

			ps = cashbackRuleService.save(ps);
			map.put("sucesso", true);
			map.put("obj", ps);
			map.put("message", "Success!");
			map.put("isEditing", isEditing);
		} catch (Exception e) {
			map.put("sucesso", false);
			map.put("obj", cashbackRule);
			map.put("message", e.getLocalizedMessage());
		}
		return map;
	}

	public boolean delete(Long id) {
		try {
			cashbackRuleService.delete(id);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
