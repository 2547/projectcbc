package com.adaptaconsultoria.cashbackcore.services.ctrls;

import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.Entity;
import com.adaptaconsultoria.cashbackcore.models.Partner;
import com.adaptaconsultoria.cashbackcore.models.User;
import com.adaptaconsultoria.cashbackcore.repositories.EntityRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PartnerRepository;
import com.adaptaconsultoria.cashbackcore.services.EntityService;
import com.adaptaconsultoria.cashbackcore.utils.PageUtil;
import com.adaptaconsultoria.cashbackcore.utils.SessionUtility;

@Service
public class EntityControllerService {

	@Autowired private EntityService entityService;
	@Autowired private EntityRepository entityRepository;
	@Autowired private PartnerRepository partnerRepository;

	public ModelAndView getList() {
		PageUtil pageUtil = new PageUtil("entity/list");
		pageUtil.setURI("/entity");
		pageUtil.setPageTitle("Entity");
		pageUtil.setTitle("Entity");
		pageUtil.setInnerTitle("Entity");
		pageUtil.setText("Double click to edit.");
		pageUtil.setSubTitle("List");
		pageUtil.setTableId("entity-list-table");
		pageUtil.setJs("entity-list.js");
		pageUtil.setAttr("menuId", "entity-menu");
		return pageUtil.getModel();
	}

	public ModelAndView getPost(Long id) {
		PageUtil pageUtil = new PageUtil("entity/form");
		pageUtil.setURI("/entity");
		pageUtil.setPageTitle("Entity");
		pageUtil.setTitle("Entity");
		pageUtil.setInnerTitle("Entity");
		pageUtil.setFormId("entity-form");
		pageUtil.setJs("entity.js");
		pageUtil.setAttr("commandName", "obj");
		pageUtil.setAttr("mi", "entitylist");
		pageUtil.setAttr("menuId", "entity-menu");

		Entity obj = new Entity();
		Company company = SessionUtility.getUserLoggedin().getCompany();

		if (id != null) {
			pageUtil.setSubTitle("Edit");
			try {
				obj = entityRepository.findTop1ByCompanyAndId(company, id);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			pageUtil.setSubTitle("New");
		}

		Partner partner = null;
		try {
			partner = partnerRepository.findTop1ByCompanyAndAccountNoAndPartnerCategoryNotNull(company, obj.getAccount().getAccountNo());
		} catch (Exception e) {
		}
		pageUtil.setAttr("partner", partner);

		pageUtil.setAttr("obj", obj);
		pageUtil.setCommandName("entity");

		return pageUtil.getModel();
	}

	public HashMap<Object, Object> save(Entity entity) {
		HashMap<Object, Object> map = new HashMap<>();
		try {

			User user = SessionUtility.getUserLoggedin();
			Boolean isEditing = entity.getId() != null;

			Entity ps = new Entity();
			if (isEditing) {
				ps = entityRepository.findById(entity.getId()).get();
				ps.setUpdatedBy(user);
				ps.setUpdated(new Date());
			} else {
				ps.setCompany(user.getCompany());
				ps.setCreatedBy(user);
			}
			ps.setIsactive(entity.getIsactive());
			ps.setName(entity.getName());
			ps.setDescription(entity.getDescription());
			ps.setAccount(entity.getAccount());

			ps = entityService.save(ps);
			map.put("sucesso", true);
			map.put("obj", ps);
			map.put("message", "Success!");
			map.put("isEditing", isEditing);
		} catch (Exception e) {
			map.put("sucesso", false);
			map.put("obj", entity);
			map.put("message", e.getLocalizedMessage());
		}
		return map;
	}

	public boolean delete(Long id) {
		try {
			entityService.delete(id);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
