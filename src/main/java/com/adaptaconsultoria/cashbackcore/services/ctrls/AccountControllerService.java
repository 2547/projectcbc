package com.adaptaconsultoria.cashbackcore.services.ctrls;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.cashbackcore.models.Account;
import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.CompanyConfig;
import com.adaptaconsultoria.cashbackcore.repositories.AccountRepository;
import com.adaptaconsultoria.cashbackcore.repositories.CompanyConfigRepository;
import com.adaptaconsultoria.cashbackcore.repositories.CompanyRepository;
import com.adaptaconsultoria.cashbackcore.services.AuthService;
import com.adaptaconsultoria.cashbackcore.services.CompanyParamService;
import com.adaptaconsultoria.cashbackcore.utils.PageUtil;
import com.adaptaconsultoria.cashbackcore.utils.SessionUtility;

@Service
public class AccountControllerService {

	@Autowired private AuthService authService;
	@Autowired private AccountRepository accountRepository;
	@Autowired private CompanyRepository companyRepository;
	@Autowired private CompanyParamService companyParamService;
	@Autowired private CompanyConfigRepository companyConfigRepository;

	public ModelAndView getList() {
		PageUtil pageUtil = new PageUtil("accountaccess/list");
		pageUtil.setURI("/accountaccess");
		pageUtil.setPageTitle("Account Access");
		pageUtil.setTitle("Account Access");
		pageUtil.setInnerTitle("Account Access");
		pageUtil.setSubTitle("List");
		pageUtil.setTableId("accountaccess-list-table");
		pageUtil.setJs("accountaccess-list.js");
		pageUtil.setAttr("menuId", "accountaccess-menu");
		return pageUtil.getModel();
	}

	public ModelAndView getRedirect(Long id, HttpServletRequest req) {
		PageUtil pageUtil = new PageUtil("accountaccess/form");
		pageUtil.setURI("/accountaccess/redirect");
		pageUtil.setPageTitle("Account Access - Redirect");
		pageUtil.setTitle("Account Access - Redirect");
		pageUtil.setInnerTitle("Account Access - Redirect");
		pageUtil.setSubTitle("Redirect");
		pageUtil.setTableId("accountaccess-redirect-table");
		pageUtil.setJs("accountaccess-redirect.js");
		pageUtil.setAttr("menuId", "accountaccess-menu");

		Integer timeRedirect = 5;
		String redirectUrl = req.getContextPath() + "/accountaccess";
		try {
			Company company = companyRepository.findById(SessionUtility.getUserLoggedin().getCompany().getId()).get();

			CompanyConfig config = companyConfigRepository.findTop1ByCompanyAndIsactiveTrue(company);
			if (config == null) {
				throw new Exception();
			}
			if (config.getApp() == null) {
				throw new Exception();
			}

			String urlApp = companyParamService.getAppUrl(company);
			String endpointRemoteAuthentication = companyParamService.getAppUrl(company);

			Account account = accountRepository.findTop1ByCompanyAndId(company, id);
			if (account == null) {
				throw new Exception();
			}

			AppSession appSession = authService.getAccessToken(config.getApp(), account.getUser(), req.getRemoteAddr(), "/accountaccess");

			redirectUrl = urlApp + endpointRemoteAuthentication + "?token=" + appSession.getToken();
			timeRedirect = 0;
		} catch (Exception e) {
		}
		pageUtil.setAttr("timeRedirect", timeRedirect);
		pageUtil.setAttr("redirectUrl", redirectUrl);

		return pageUtil.getModel();
	}

}
