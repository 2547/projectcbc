package com.adaptaconsultoria.cashbackcore.services.ctrls;

import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.PartnerCategory;
import com.adaptaconsultoria.cashbackcore.models.User;
import com.adaptaconsultoria.cashbackcore.repositories.PartnerCategoryRepository;
import com.adaptaconsultoria.cashbackcore.services.PartnerCategoryService;
import com.adaptaconsultoria.cashbackcore.utils.PageUtil;
import com.adaptaconsultoria.cashbackcore.utils.SessionUtility;

@Service
public class PartnerCategoryControllerService {

	@Autowired private PartnerCategoryService partnerCategoryService;
	@Autowired private PartnerCategoryRepository partnerCategoryRepository;

	public ModelAndView getList() {
		PageUtil pageUtil = new PageUtil("partnercategory/list");
		pageUtil.setURI("/partnercategory");
		pageUtil.setPageTitle("Partner Category");
		pageUtil.setTitle("Partner Category");
		pageUtil.setInnerTitle("Partner Category");
		pageUtil.setText("Double click to edit.");
		pageUtil.setSubTitle("List");
		pageUtil.setTableId("partnercategory-list-table");
		pageUtil.setJs("partnercategory-list.js");
		pageUtil.setAttr("menuId", "partnercategory-menu");
		return pageUtil.getModel();
	}

	public ModelAndView getPost(Long id) {
		PageUtil pageUtil = new PageUtil("partnercategory/form");
		pageUtil.setURI("/partnercategory");
		pageUtil.setPageTitle("Partner Category");
		pageUtil.setTitle("Partner Category");
		pageUtil.setInnerTitle("Partner Category");
		pageUtil.setFormId("partnercategory-form");
		pageUtil.setJs("partnercategory.js");
		pageUtil.setAttr("commandName", "obj");
		pageUtil.setAttr("mi", "partnercategorylist");
		pageUtil.setAttr("menuId", "partnercategory-menu");

		PartnerCategory obj = new PartnerCategory();
		Company company = SessionUtility.getUserLoggedin().getCompany();

		if (id != null) {
			pageUtil.setSubTitle("Edit");
			try {
				obj = partnerCategoryRepository.findTop1ByCompanyAndId(company, id);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			pageUtil.setSubTitle("New");
		}

		pageUtil.setAttr("obj", obj);
		pageUtil.setCommandName("partnerCategory");

		return pageUtil.getModel();
	}

	public HashMap<Object, Object> save(PartnerCategory partnerCategory) {
		HashMap<Object, Object> map = new HashMap<>();
		try {

			User user = SessionUtility.getUserLoggedin();
			Boolean isEditing = partnerCategory.getId() != null;

			PartnerCategory ps = new PartnerCategory();
			if (isEditing) {
				ps = partnerCategoryRepository.findById(partnerCategory.getId()).get();
				ps.setUpdatedBy(user);
				ps.setUpdated(new Date());
			} else {
				ps.setCompany(user.getCompany());
				ps.setCreatedBy(user);
			}
			ps.setIsactive(partnerCategory.getIsactive());
			ps.setName(partnerCategory.getName());
			ps.setIsleader(partnerCategory.getIsleader());
			ps.setDescription(partnerCategory.getDescription());

			ps = partnerCategoryService.save(ps);
			map.put("sucesso", true);
			map.put("obj", ps);
			map.put("message", "Success!");
			map.put("isEditing", isEditing);
		} catch (Exception e) {
			map.put("sucesso", false);
			map.put("obj", partnerCategory);
			map.put("message", e.getLocalizedMessage());
		}
		return map;
	}

	public boolean delete(Long id) {
		try {
			partnerCategoryService.delete(id);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
