package com.adaptaconsultoria.cashbackcore.services.ctrls;

import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.Payout;
import com.adaptaconsultoria.cashbackcore.models.User;
import com.adaptaconsultoria.cashbackcore.repositories.DocSequenceRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PaymentMethodRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PayoutRepository;
import com.adaptaconsultoria.cashbackcore.services.PayoutService;
import com.adaptaconsultoria.cashbackcore.utils.PageUtil;
import com.adaptaconsultoria.cashbackcore.utils.SessionUtility;

@Service
public class PayoutControllerService {

	@Autowired private PayoutService payoutService;
	@Autowired private PayoutRepository payoutRepository;
	@Autowired private DocSequenceRepository docSequenceRepository;
	@Autowired private PaymentMethodRepository paymentMethodRepository;

	public ModelAndView getList() {
		PageUtil pageUtil = new PageUtil("payout/list");
		pageUtil.setURI("/payout");
		pageUtil.setPageTitle("Payout");
		pageUtil.setTitle("Payout");
		pageUtil.setInnerTitle("Payout");
		pageUtil.setText("Double click to edit.");
		pageUtil.setSubTitle("List");
		pageUtil.setTableId("payout-list-table");
		pageUtil.setJs("payout-list.js");
		pageUtil.setAttr("menuId", "payout-menu");
		return pageUtil.getModel();
	}

	public ModelAndView getPost(Long id) {
		PageUtil pageUtil = new PageUtil("payout/form");
		pageUtil.setURI("/payout");
		pageUtil.setPageTitle("Payout");
		pageUtil.setTitle("Payout");
		pageUtil.setInnerTitle("Payout");
		pageUtil.setFormId("payout-form");
		pageUtil.setJs("payout.js");
		pageUtil.setAttr("commandName", "obj");
		pageUtil.setAttr("mi", "payoutlist");
		pageUtil.setAttr("menuId", "payout-menu");

		Payout obj = new Payout();
		Company company = SessionUtility.getUserLoggedin().getCompany();

		if (id != null) {
			pageUtil.setSubTitle("Edit");
			try {
				obj = payoutRepository.findTop1ByCompanyAndId(company, id);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			pageUtil.setSubTitle("New");
		}

		pageUtil.setAttr("docSequences", docSequenceRepository.findByCompany(company));
		pageUtil.setAttr("paymentMethods", paymentMethodRepository.findByCompany(company));

		pageUtil.setAttr("obj", obj);
		pageUtil.setCommandName("payout");

		return pageUtil.getModel();
	}

	public HashMap<Object, Object> save(Payout payout) {
		HashMap<Object, Object> map = new HashMap<>();
		try {

			User user = SessionUtility.getUserLoggedin();
			Boolean isEditing = payout.getId() != null;

			Payout ps = new Payout();
			if (isEditing) {
				ps = payoutRepository.findById(payout.getId()).get();
				ps.setUpdatedBy(user);
				ps.setUpdated(new Date());
			} else {
				ps.setCompany(user.getCompany());
				ps.setCreatedBy(user);
			}
			ps.setIsactive(payout.getIsactive());
			ps.setName(payout.getName());
			ps.setDescription(payout.getDescription());
			ps.setValueMask(payout.getValueMask());
			ps.setValueLength(payout.getValueLength());
			ps.setRemittanceSequence(payout.getRemittanceSequence());
			ps.setRemittanceClassname(payout.getRemittanceClassname());
			ps.setPaymentMethod(payout.getPaymentMethod());
			ps.setPartner(payout.getPartner());

			ps = payoutService.save(ps);
			map.put("sucesso", true);
			map.put("obj", ps);
			map.put("message", "Success!");
			map.put("isEditing", isEditing);
		} catch (Exception e) {
			map.put("sucesso", false);
			map.put("obj", payout);
			map.put("message", e.getLocalizedMessage());
		}
		return map;
	}

	public boolean delete(Long id) {
		try {
			payoutService.delete(id);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
