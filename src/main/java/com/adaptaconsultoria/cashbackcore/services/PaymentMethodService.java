package com.adaptaconsultoria.cashbackcore.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.models.PaymentMethod;
import com.adaptaconsultoria.cashbackcore.repositories.PaymentMethodRepository;

@Service
public class PaymentMethodService {

	@Autowired private PaymentMethodRepository paymentMethodRepository;

	public PaymentMethod save(PaymentMethod paymentMethod) {
		return paymentMethodRepository.save(paymentMethod);
	}

	@Transactional
	public void delete(Long paymentMethodId) throws Exception {
		Optional<PaymentMethod> paymentMethod = paymentMethodRepository.findById(paymentMethodId);
		if (paymentMethod.isPresent()) {
			paymentMethodRepository.delete(paymentMethod.get());
		} else {
			throw new Exception("Payment Method not found!");
		}
	}

}
