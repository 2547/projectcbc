package com.adaptaconsultoria.cashbackcore.services.util;

import java.net.InetAddress;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class AppInfoService {

	@Autowired
	private Environment env;

	private final static String ipAPI = "https://api.ipify.org?format=json";

	public String getAppToken() {
		try {
			return env.getProperty("app.token");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public String getAppPassword() {
		try {
			return env.getProperty("app.password");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public String getIpAdress() {
		InetAddress inetAddress = null;
		try {
			inetAddress = InetAddress.getLocalHost();
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<Object> obj = restTemplate.exchange(ipAPI, HttpMethod.GET, requestHeaders(), Object.class);
			Optional<Object> object = Optional.of(obj.getBody());
			Map<Object, Object> map = (Map<Object, Object>) object.get();
			String ipAddress = (String) map.get("ip");
			return ipAddress;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return inetAddress.getHostAddress();
	}

	public MultiValueMap<String, String> getBasicPublicServiceRequest() {
		MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
		try {
			map.add("ipAddress", getIpAdress());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return map;
	}

	public MultiValueMap<String, String> getBasicPrivateServiceRequest() {
		MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
		try {
			map.add("ipAddress", getIpAdress());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return map;
	}

	public String getGetRequest(String url, MultiValueMap<String, String> parameters) {

		try {
			return UriComponentsBuilder.fromHttpUrl(url).queryParams(parameters).toUriString();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;

	}

	public HttpEntity<String> getPostRequestHeaders(String params) {
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
			return new HttpEntity<String>(params, headers);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return new HttpEntity<>(null, null);
	}

	public HttpEntity<?> requestHeaders() {
		try {
			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.add("Accept", MediaType.APPLICATION_JSON_VALUE);
			return new HttpEntity<>(requestHeaders);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}

	}

}
