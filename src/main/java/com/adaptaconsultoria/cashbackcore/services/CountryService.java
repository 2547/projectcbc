package com.adaptaconsultoria.cashbackcore.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.models.Country;
import com.adaptaconsultoria.cashbackcore.models.DocSequence;

@Service
public class CountryService {

	@Autowired private DocSequenceService docSequenceService;

	public Long getNextAccount(Country country) throws Exception {
		DocSequence docSequence = country.getAccountSequence();
		if (docSequence == null) {
			throw new Exception("No sequence for next account!");
		}

		return docSequenceService.nextSequence(docSequence);
	}

}
