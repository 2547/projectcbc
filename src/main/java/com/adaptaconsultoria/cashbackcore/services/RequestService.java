package com.adaptaconsultoria.cashbackcore.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.adaptaconsultoria.cashbackcore.services.util.AppInfoService;
import com.adaptaconsultoria.cashbackcore.utils.JsonUtility;

@Service
public class RequestService {

	@Autowired private AppInfoService appInfoService;

	public Object postRequest(String url, Object obj) {
		try {
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<Object> o = restTemplate.exchange(url, HttpMethod.POST,
					appInfoService.getPostRequestHeaders(JsonUtility.objToJsonString(obj)), Object.class);
			Optional<Object> object = Optional.of(o.getBody());
			return object.get();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public Object getRequest(String url, MultiValueMap<String, String> params) {
		try {
			MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
			RestTemplate restTemplate = new RestTemplate();

			map = appInfoService.getBasicPrivateServiceRequest();

			if (!params.isEmpty())
				map.addAll(params);

			ResponseEntity<?> objIn = restTemplate.exchange(appInfoService.getGetRequest(url, map), HttpMethod.GET,
					appInfoService.requestHeaders(), Object.class);
			Optional<Object> obj = Optional.of(objIn.getBody());
			return obj.get();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public Object getRequestNoParams(String url, MultiValueMap<String, String> params) {
		try {
			MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
			RestTemplate restTemplate = new RestTemplate();

			if (!params.isEmpty())
				map.addAll(params);

			ResponseEntity<?> objIn = restTemplate.exchange(appInfoService.getGetRequest(url, map), HttpMethod.GET,
					appInfoService.requestHeaders(), Object.class);
			Optional<Object> obj = Optional.of(objIn.getBody());
			return obj.get();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

}
