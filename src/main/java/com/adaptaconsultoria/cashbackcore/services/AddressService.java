package com.adaptaconsultoria.cashbackcore.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adaptaconsultoria.cashbackcore.models.Address;
import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.models.City;
import com.adaptaconsultoria.cashbackcore.models.Country;
import com.adaptaconsultoria.cashbackcore.models.Region;
import com.adaptaconsultoria.cashbackcore.models.User;
import com.adaptaconsultoria.cashbackcore.objects.in.AccountPostIn;
import com.adaptaconsultoria.cashbackcore.repositories.AddressRepository;
import com.adaptaconsultoria.cashbackcore.repositories.CityRepository;
import com.adaptaconsultoria.cashbackcore.repositories.CountryRepository;
import com.adaptaconsultoria.cashbackcore.repositories.RegionRepository;

@Service
public class AddressService {

	@Autowired private UserService userService;
	@Autowired private CityRepository cityRepository;
	@Autowired private RegionRepository regionRepository;
	@Autowired private CountryRepository countryRepository;
	@Autowired private AddressRepository addressRepository;

	public Address save(Address address) {
		return addressRepository.save(address);
	}

	public void remove(Address address) {
		addressRepository.delete(address);
	}

	public Address createAddress(AccountPostIn in, AppSession appSession) throws Exception {

		User userCreating = userService.getUserCreating(appSession);

		Address address = validateWithAccountPostIn(in, appSession);
		address.setCreatedBy(userCreating);

		return address;

	}

	private Address validateWithAccountPostIn(AccountPostIn in, AppSession appSession) throws Exception {
		Address address = new Address();

		address.setCompany(appSession.getCompany());

		try {
			Country country = countryRepository.findTop1ByCompanyAndIsoCodeIgnoreCaseAndIsactiveTrue(address.getCompany(), in.getAddressCountryIsoCode());
			address.setCountry(country);
		} catch (Exception e) {
		}

		try {

			Region region = null;
			if (address.getCountry() != null) {
				region = regionRepository.findTop1ByCompanyAndCountryAndCodeIgnoreCaseAndIsactiveTrue(address.getCompany(), address.getCountry(), in.getAddressRegionCode());
			}
			if (region == null) {
				region = regionRepository.findTop1ByCompanyAndCodeIgnoreCaseAndIsactiveTrue(address.getCompany(), in.getAddressRegionCode());
			}
			if (region != null) {
				if (address.getCountry() == null) {
					address.setCountry(region.getCountry());
				}
				address.setRegion(region);
			}

		} catch (Exception e) {
		}

		try {

			City city = null;
			if (address.getRegion() != null) {
				city = cityRepository.findTop1ByCompanyAndRegionAndCodeIgnoreCaseAndIsactiveTrue(address.getCompany(), address.getRegion(), in.getAddressCityCode());
			}
			if (city == null) {
				city = cityRepository.findTop1ByCompanyAndCodeIgnoreCaseAndIsactiveTrue(address.getCompany(), in.getAddressCityCode());
			}
			if (city != null) {
				address.setCity(city);
				if (address.getRegion() == null) {
					address.setRegion(city.getRegion());
					if (address.getCountry() == null) {
						address.setCountry(city.getRegion().getCountry());
					}
				}
			}

		} catch (Exception e) {
		}

		address.setAddress1(in.getAddress());
		address.setAddress2(in.getAddress2());
		address.setNumber(in.getAddressNumber());
		address.setDistrict(in.getAddressDistrict());
		address.setZipcode(in.getAddressZipcode());
		address.setLat(in.getAddressLat());
		address.setLng(in.getAddressLng());
		address.setGeoinformation(in.getAddressGeoinformation());

		return address;
	}

}
