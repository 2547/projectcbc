package com.adaptaconsultoria.cashbackcore.controllers;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.cashbackcore.configs.Converter;
import com.adaptaconsultoria.cashbackcore.models.Account;
import com.adaptaconsultoria.cashbackcore.models.Address;
import com.adaptaconsultoria.cashbackcore.models.City;
import com.adaptaconsultoria.cashbackcore.models.Country;
import com.adaptaconsultoria.cashbackcore.models.Currency;
import com.adaptaconsultoria.cashbackcore.models.Partner;
import com.adaptaconsultoria.cashbackcore.models.PartnerRule;
import com.adaptaconsultoria.cashbackcore.models.Region;
import com.adaptaconsultoria.cashbackcore.models.SellerCategory;
import com.adaptaconsultoria.cashbackcore.repositories.AccountRepository;
import com.adaptaconsultoria.cashbackcore.repositories.AddressRepository;
import com.adaptaconsultoria.cashbackcore.repositories.CityRepository;
import com.adaptaconsultoria.cashbackcore.repositories.CountryRepository;
import com.adaptaconsultoria.cashbackcore.repositories.CurrencyRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PartnerRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PartnerRuleRepository;
import com.adaptaconsultoria.cashbackcore.repositories.RegionRepository;
import com.adaptaconsultoria.cashbackcore.repositories.SellerCategoryRepository;
import com.adaptaconsultoria.cashbackcore.services.PartnerRuleService;
import com.adaptaconsultoria.cashbackcore.services.ctrls.PartnerControllerService;
import com.adaptaconsultoria.cashbackcore.utils.SessionUtility;

@Controller
@RequestMapping("/seller")
public class SellerController {

	@Autowired private CityRepository cityRepository;
	@Autowired private RegionRepository regionRepository;
	@Autowired private CountryRepository countryRepository;
	@Autowired private AddressRepository addressRepository;
	@Autowired private AccountRepository accountRepository;
	@Autowired private PartnerRepository partnerRepository;
	@Autowired private CurrencyRepository currencyRepository;
	@Autowired private SellerCategoryRepository sellerCategoryRepository;
	@Autowired private PartnerControllerService partnerControllerService;
	@Autowired private PartnerRuleRepository partnerRuleRepository;
	@Autowired private PartnerRuleService partnerRuleService;
	
	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(SellerCategory.class, "sellerCategory", new Converter(sellerCategoryRepository));
		binder.registerCustomEditor(Partner.class, "leader", new Converter(partnerRepository));
		binder.registerCustomEditor(Account.class, "account", new Converter(accountRepository));
		binder.registerCustomEditor(Address.class, "address", new Converter(addressRepository));
		binder.registerCustomEditor(Country.class, "country", new Converter(countryRepository));
		binder.registerCustomEditor(Region.class, "region", new Converter(regionRepository));
		binder.registerCustomEditor(City.class, "city", new Converter(cityRepository));
		binder.registerCustomEditor(Currency.class, "currency", new Converter(currencyRepository));
	}

	@GetMapping(value = "/list")
	public ModelAndView list() {
		return partnerControllerService.getSellerList();
	}
	
	@ResponseBody
	@GetMapping(value = "/getpercentagelist")
	public ResponseEntity<?> getPercentageList() {
		return ResponseEntity.ok(new ArrayList<PartnerRule>());
	}

	@ResponseBody
	@GetMapping(value = "/getlist")
	public ResponseEntity<?> getList(HttpSession session) {
		return ResponseEntity.ok(partnerRepository.findByCompanyAndSellerCategoryNotNull(SessionUtility.getUserLoggedin().getCompany()));
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> remove(@PathVariable Long id) {
		return ResponseEntity.ok(partnerControllerService.delete(id));
	}

	@GetMapping
	public ModelAndView get(Long id) {
		return partnerControllerService.getSellerPost(id);
	}

	@PostMapping
	public ResponseEntity<?> post(Partner seller, Address address) {
		seller.setAddress(address);
		return ResponseEntity.ok(partnerControllerService.saveSeller(seller));
	}
	
	@PostMapping(value = "/postrules")
	public ResponseEntity<?> postRules(PartnerRule rule, Long sellerId) {
		return ResponseEntity.ok(partnerControllerService.saveSellerRule(rule, sellerId));
	}
	
	@GetMapping(value = "/getrules")
	public ResponseEntity<?> getRules(Long sellerId) {
		return ResponseEntity.ok(partnerRuleRepository.findByPartnerId(sellerId));
	}
	
	@DeleteMapping(value = "removepercentage/{id}")
	public ResponseEntity<?> percentage(@PathVariable Long id) {
		return ResponseEntity.ok( partnerRuleService.remove(id));
	}
}
