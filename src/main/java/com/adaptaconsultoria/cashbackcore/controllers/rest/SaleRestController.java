package com.adaptaconsultoria.cashbackcore.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.objects.in.SaleGetIn;
import com.adaptaconsultoria.cashbackcore.objects.in.SalePostIn;
import com.adaptaconsultoria.cashbackcore.objects.out.DefaultOut;
import com.adaptaconsultoria.cashbackcore.objects.out.SaleGetOut;
import com.adaptaconsultoria.cashbackcore.objects.out.SalePostOut;
import com.adaptaconsultoria.cashbackcore.objects.out.error.DefaultError;
import com.adaptaconsultoria.cashbackcore.repositories.AppSessionRepository;
import com.adaptaconsultoria.cashbackcore.services.SaleService;

@RestController
@RequestMapping("/api/sale")
public class SaleRestController extends DefaultRestController {

	@Autowired private SaleService saleService;
	@Autowired private AppSessionRepository appSessionRepository;

	@PostMapping
	public DefaultOut post(@RequestBody SalePostIn in) {
		SalePostOut out = new SalePostOut();
		try {
			if (this.validarCampos(in, out, "/api/sale")) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				out.setCashback(saleService.createSale(in, appSession).getCashback());
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@GetMapping
	public DefaultOut get(SaleGetIn in) {
		SaleGetOut out = new SaleGetOut();
		try {
			if (this.validarCampos(in, out, "/api/sale")) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				out.setSales(saleService.getSales(in, appSession));
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@GetMapping(path = "/seller")
	public DefaultOut getSeller(SaleGetIn in) {
		SaleGetOut out = new SaleGetOut();
		try {
			if (this.validarCampos(in, out, "/api/sale/seller")) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				out.setSales(saleService.getSalesSeller(in, appSession));
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}
