package com.adaptaconsultoria.cashbackcore.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.objects.in.AccountPayoutPostIn;
import com.adaptaconsultoria.cashbackcore.objects.in.DefaultIn;
import com.adaptaconsultoria.cashbackcore.objects.out.AccountPayoutGetOut;
import com.adaptaconsultoria.cashbackcore.objects.out.DefaultOut;
import com.adaptaconsultoria.cashbackcore.objects.out.error.DefaultError;
import com.adaptaconsultoria.cashbackcore.repositories.AppSessionRepository;
import com.adaptaconsultoria.cashbackcore.services.AccountPayoutService;

@RestController
@RequestMapping("/api/accountpayout")
public class AccountPayoutRestController extends DefaultRestController {

	@Autowired private AccountPayoutService accountPayoutService;
	@Autowired private AppSessionRepository appSessionRepository;

	@PostMapping
	public DefaultOut post(@RequestBody AccountPayoutPostIn in) {
		DefaultOut out = new DefaultOut();
		try {
			if (this.validarCampos(in, out, "/api/accountpayout")) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				accountPayoutService.createAccountPayout(in, appSession);

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			try {
				error.setError(e.getCause().getCause().getMessage());
			} catch (Exception e2) {
				error.setError(e.getMessage());
			}
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@GetMapping
	public DefaultOut get(DefaultIn in) {
		AccountPayoutGetOut out = new AccountPayoutGetOut();
		try {
			if (this.validarCampos(in, out, "/api/accountpayout")) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				out.setPayouts(accountPayoutService.getAccountPayouts(appSession));

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}
