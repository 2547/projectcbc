package com.adaptaconsultoria.cashbackcore.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.objects.in.DefaultIn;
import com.adaptaconsultoria.cashbackcore.objects.out.DashboardStatsGetOut;
import com.adaptaconsultoria.cashbackcore.objects.out.DefaultOut;
import com.adaptaconsultoria.cashbackcore.objects.out.error.DefaultError;
import com.adaptaconsultoria.cashbackcore.repositories.AppSessionRepository;
import com.adaptaconsultoria.cashbackcore.repositories.DashboardRepository;
import com.adaptaconsultoria.cashbackcore.services.AccountService;

@RestController
@RequestMapping("/api/dashboard")
public class DashboardRestController extends DefaultRestController {

	@Autowired private AccountService accountService;
	@Autowired private DashboardRepository dashboardRepository;
	@Autowired private AppSessionRepository appSessionRepository;

	@GetMapping(path = "/stats")
	public DefaultOut get(DefaultIn in) {
		DashboardStatsGetOut out = new DashboardStatsGetOut();
		try {
			if (this.validarCampos(in, out, "/api/cashback")) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				out.setStats(dashboardRepository.findStatsByAccount(accountService.getAccount(appSession).getId()));
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}
