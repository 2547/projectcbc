package com.adaptaconsultoria.cashbackcore.controllers.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.objects.in.DefaultIn;
import com.adaptaconsultoria.cashbackcore.objects.out.DefaultOut;
import com.adaptaconsultoria.cashbackcore.objects.out.LocalizationGetOut;
import com.adaptaconsultoria.cashbackcore.objects.out.error.DefaultError;
import com.adaptaconsultoria.cashbackcore.objects.pojo.CountryJson;
import com.adaptaconsultoria.cashbackcore.repositories.AppSessionRepository;
import com.adaptaconsultoria.cashbackcore.services.LocalizationService;
import com.adaptaconsultoria.cashbackcore.utils.JsonUtility;

@RestController
@RequestMapping("/api/localization")
public class LocalizationRestController extends DefaultRestController {

	@Autowired private LocalizationService localizationService;
	@Autowired private AppSessionRepository appSessionRepository;

	@GetMapping
	@SuppressWarnings("unchecked")
	public DefaultOut get(DefaultIn in) {
		LocalizationGetOut out = new LocalizationGetOut();
		try {
			if (this.validarCampos(in, out, "/api/localization", false)) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				String obj = localizationService.get(appSession);
				out.setCountriesStr(obj);
				try {
					out.setCountries((List<CountryJson>) JsonUtility.fromJson(obj));
				} catch (Exception e) {
				}
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}
