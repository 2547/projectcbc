package com.adaptaconsultoria.cashbackcore.controllers.rest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.cashbackcore.models.Account;
import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.models.Partner;
import com.adaptaconsultoria.cashbackcore.objects.in.AuthPostIn;
import com.adaptaconsultoria.cashbackcore.objects.in.DefaultIn;
import com.adaptaconsultoria.cashbackcore.objects.out.AuthGetOut;
import com.adaptaconsultoria.cashbackcore.objects.out.AuthPostOut;
import com.adaptaconsultoria.cashbackcore.objects.out.error.AuthPostError;
import com.adaptaconsultoria.cashbackcore.objects.out.error.DefaultError;
import com.adaptaconsultoria.cashbackcore.objects.rest.User;
import com.adaptaconsultoria.cashbackcore.repositories.AccountRepository;
import com.adaptaconsultoria.cashbackcore.repositories.AppSessionRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PartnerRepository;
import com.adaptaconsultoria.cashbackcore.services.AppSessionService;
import com.adaptaconsultoria.cashbackcore.services.AuthService;

@RestController
@RequestMapping("/api/auth")
public class AuthRestController extends DefaultRestController {

	@Autowired private AuthService authService;
	@Autowired private AccountRepository accountRepository;
	@Autowired private PartnerRepository partnerRepository;
	@Autowired private AppSessionService appSessionService;
	@Autowired private AppSessionRepository appSessionRepository;

	@GetMapping
	public AuthGetOut get(DefaultIn in) {
		AuthGetOut out = new AuthGetOut();
		try {
			if (this.validarCampos(in, out, "/api/auth", true, false)) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				appSession.setIpAddress(in.getIpAddress());
				appSession = appSessionService.save(appSession);

				out.setUser(User.fromModel(appSession.getUser()));

				String role = "CUSTOMER";
				try {
					Account account = accountRepository.findTop1ByCompanyAndUserAndIsactiveTrue(appSession.getCompany(), appSession.getUser());
					Partner partner = partnerRepository.findTop1ByCompanyAndAccount(appSession.getCompany(), account);
					if (partner.getPartnerCategory() != null) {
						role = "PARTNER";
					} else if (partner.getSellerCategory() != null) {
						role = "SELLER";
					}
				} catch (Exception e) {
				}
				out.getUser().setRole(role);

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@PostMapping
	public AuthPostOut post(@RequestBody AuthPostIn in) {
		AuthPostOut out = new AuthPostOut();
		try {
			if (this.validarCampos(in, out)) {
				AppSession appSession = authService.getAccessToken(in, "/api/auth");
				if (appSession == null) {
					AuthPostError error = new AuthPostError();
					error.setError("Token could not be created!");
					out.setError(error);
				} else {
					out.setUser(User.fromModel(appSession.getUser()));
					out.setToken(appSession.getToken());

					String role = "CUSTOMER";
					try {
						Account account = accountRepository.findTop1ByCompanyAndUserAndIsactiveTrue(appSession.getCompany(), appSession.getUser());
						Partner partner = partnerRepository.findTop1ByCompanyAndAccount(appSession.getCompany(), account);
						if (partner.getPartnerCategory() != null) {
							role = "PARTNER";
						} else if (partner.getSellerCategory() != null) {
							role = "SELLER";
						}
					} catch (Exception e) {
					}
					out.getUser().setRole(role);

				}
			}
		} catch (Exception e) {
			AuthPostError error = new AuthPostError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	private Boolean validarCampos(AuthPostIn in, AuthPostOut out) {
		Boolean ok = true;
		AuthPostError error = new AuthPostError();

		if (StringUtils.isBlank(in.getAppToken())) {
			error.setAppToken("appToken is required!");
			ok = false;
		}

		if (StringUtils.isBlank(in.getAppPassword())) {
			error.setAppPassword("appPassword is required!");
			ok = false;
		}

//		if (StringUtils.isBlank(in.getUserName())) {
//			error.setUserName("userName is required!");
//			ok = false;
//		}
//
//		if (StringUtils.isBlank(in.getUserPassword())) {
//			error.setUserPassword("userPassword is required!");
//			ok = false;
//		}

		if (StringUtils.isBlank(in.getIpAddress())) {
			error.setUserPassword("ipAddress is required!");
			ok = false;
		}

		if (!ok) {
			out.setError(error);
		}

		out.setHasError(!ok);
		return ok;
	}
}