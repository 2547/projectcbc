package com.adaptaconsultoria.cashbackcore.controllers.rest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.objects.in.UserLoginGetIn;
import com.adaptaconsultoria.cashbackcore.objects.out.DefaultOut;
import com.adaptaconsultoria.cashbackcore.objects.out.UserLoginGetOut;
import com.adaptaconsultoria.cashbackcore.objects.out.error.DefaultError;
import com.adaptaconsultoria.cashbackcore.repositories.AppSessionRepository;
import com.adaptaconsultoria.cashbackcore.services.UserService;

@RestController
@RequestMapping("/api/user")
public class UserRestController extends DefaultRestController {

	@Autowired private UserService userService;
	@Autowired private AppSessionRepository appSessionRepository;

	@GetMapping(path = "/login")
	public DefaultOut getLogin(UserLoginGetIn in) {
		UserLoginGetOut out = new UserLoginGetOut();
		try {
			if (this.validarCampos(in, out, "/api/user/login", false)) {

				if (StringUtils.isNotBlank(in.getLogin())) {
					AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
					boolean loginExists = userService.userLoginExists(appSession.getCompany(), in.getLogin());
					out.setIsvalid(!loginExists);
				}

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@GetMapping(path = "/email")
	public DefaultOut getEmail(UserLoginGetIn in) {
		UserLoginGetOut out = new UserLoginGetOut();
		try {
			if (this.validarCampos(in, out, "/api/user/email", false)) {

				if (StringUtils.isNotBlank(in.getEmail())) {
					AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
					boolean loginExists = userService.userEmailExists(appSession.getCompany(), in.getEmail());
					out.setIsvalid(!loginExists);
				}

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}
