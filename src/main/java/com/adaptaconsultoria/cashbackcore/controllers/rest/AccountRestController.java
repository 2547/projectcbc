package com.adaptaconsultoria.cashbackcore.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.cashbackcore.models.Account;
import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.models.Partner;
import com.adaptaconsultoria.cashbackcore.objects.in.AccountFindGetIn;
import com.adaptaconsultoria.cashbackcore.objects.in.AccountGetIn;
import com.adaptaconsultoria.cashbackcore.objects.in.AccountPostIn;
import com.adaptaconsultoria.cashbackcore.objects.out.AccountFindGetOut;
import com.adaptaconsultoria.cashbackcore.objects.out.AccountGetOut;
import com.adaptaconsultoria.cashbackcore.objects.out.AccountPostOut;
import com.adaptaconsultoria.cashbackcore.objects.out.DefaultOut;
import com.adaptaconsultoria.cashbackcore.objects.out.error.DefaultError;
import com.adaptaconsultoria.cashbackcore.objects.rest.User;
import com.adaptaconsultoria.cashbackcore.repositories.AccountRepository;
import com.adaptaconsultoria.cashbackcore.repositories.AppSessionRepository;
import com.adaptaconsultoria.cashbackcore.repositories.AutoCompleteRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PartnerRepository;
import com.adaptaconsultoria.cashbackcore.services.AccountService;
import com.adaptaconsultoria.cashbackcore.services.AppSessionService;

@RestController
@RequestMapping("/api/account")
public class AccountRestController extends DefaultRestController {

	@Autowired private AccountService accountService;
	@Autowired private PartnerRepository partnerRepository;
	@Autowired private AccountRepository accountRepository;
	@Autowired private AppSessionService appSessionService;
	@Autowired private AppSessionRepository appSessionRepository;
	@Autowired private AutoCompleteRepository autoCompleteRepository;

	@PostMapping
	public DefaultOut post(@RequestBody AccountPostIn in) {
		AccountPostOut out = new AccountPostOut();
		try {
			if (this.validarCampos(in, out, "/api/account", false)) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				Account account = accountService.createAccount(in, appSession);

				out.setDidLogin(in.isDoLogin() && account.getUser() != null);
				if (out.isDidLogin()) {
					appSession.setUser(account.getUser());
					out.setUser(User.fromModel(appSession.getUser()));
				}

				appSessionService.save(appSession);
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@GetMapping
	public DefaultOut get(AccountGetIn in) {
		AccountGetOut out = new AccountGetOut();
		try {
			if (this.validarCampos(in, out, "/api/account")) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				Account account = accountService.getAccount(appSession);

				if (account == null) {
					throw new Exception("Account not fount!");
				}
				out.setAccount(com.adaptaconsultoria.cashbackcore.objects.rest.Account.fromModel(account));
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@GetMapping(path = "/find")
	public DefaultOut getFind(AccountFindGetIn in) {
		AccountFindGetOut out = new AccountFindGetOut();
		try {
			if (this.validarCampos(in, out, "/api/account/find")) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());

				try {
					Account account = accountRepository.findTop1ByCompanyAndUserAndIsactiveTrue(appSession.getCompany(), appSession.getUser());
					if (account == null) {
						throw new Exception();
					}
					Partner p = partnerRepository.findTop1ByCompanyAndAccountAndIsactiveTrue(appSession.getCompany(), account);
					if (p == null) {
						throw new Exception();
					}
				} catch (Exception e) {
					throw new Exception("Permission denied!");
				}

				in.setQuery("%"+in.getQuery()+"%");
				out.setAccounts(autoCompleteRepository.findAccountNoByCompany(appSession.getCompany().getId(), in.getQuery()));

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}
