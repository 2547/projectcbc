package com.adaptaconsultoria.cashbackcore.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.cashbackcore.models.Account;
import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.objects.in.AccountEntityGetIn;
import com.adaptaconsultoria.cashbackcore.objects.in.AccountEntityPostIn;
import com.adaptaconsultoria.cashbackcore.objects.out.AccountEntityGetOut;
import com.adaptaconsultoria.cashbackcore.objects.out.DefaultOut;
import com.adaptaconsultoria.cashbackcore.objects.out.error.DefaultError;
import com.adaptaconsultoria.cashbackcore.repositories.AppSessionRepository;
import com.adaptaconsultoria.cashbackcore.services.AccountEntityService;
import com.adaptaconsultoria.cashbackcore.services.AccountService;

@RestController
@RequestMapping("/api/accountentity")
public class AccountEntityRestController extends DefaultRestController {

	@Autowired private AccountService accountService;
	@Autowired private AccountEntityService accountEntityService;
	@Autowired private AppSessionRepository appSessionRepository;

	@PostMapping
	public DefaultOut post(@RequestBody AccountEntityPostIn in) {
		DefaultOut out = new DefaultOut();
		try {
			if (this.validarCampos(in, out, "/api/accountentity")) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				accountEntityService.createAccountEntity(in, appSession);

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			try {
				error.setError(e.getCause().getCause().getMessage());
			} catch (Exception e2) {
				error.setError(e.getMessage());
			}
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@GetMapping
	public DefaultOut get(AccountEntityGetIn in) {
		AccountEntityGetOut out = new AccountEntityGetOut();
		try {
			if (this.validarCampos(in, out, "/api/accountentity")) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				Account account = accountService.getAccount(appSession);

				if (account == null) {
					throw new Exception("Account not fount!");
				}

				out.setEntities(account.getEntities());

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}
