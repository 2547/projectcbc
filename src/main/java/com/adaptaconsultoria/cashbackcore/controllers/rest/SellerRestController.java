package com.adaptaconsultoria.cashbackcore.controllers.rest;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.cashbackcore.models.Account;
import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.models.Partner;
import com.adaptaconsultoria.cashbackcore.models.SellerCategory;
import com.adaptaconsultoria.cashbackcore.objects.in.DefaultIn;
import com.adaptaconsultoria.cashbackcore.objects.in.PartnerGetIn;
import com.adaptaconsultoria.cashbackcore.objects.in.PartnerPostIn;
import com.adaptaconsultoria.cashbackcore.objects.out.DefaultOut;
import com.adaptaconsultoria.cashbackcore.objects.out.PartnerGetOut;
import com.adaptaconsultoria.cashbackcore.objects.out.SellerCategoryGetOut;
import com.adaptaconsultoria.cashbackcore.objects.out.error.DefaultError;
import com.adaptaconsultoria.cashbackcore.repositories.AccountRepository;
import com.adaptaconsultoria.cashbackcore.repositories.AppSessionRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PartnerRepository;
import com.adaptaconsultoria.cashbackcore.repositories.SellerCategoryRepository;
import com.adaptaconsultoria.cashbackcore.services.PartnerService;

@RestController
@RequestMapping("/api/seller")
public class SellerRestController extends DefaultRestController {

	@Autowired private PartnerService partnerService;
	@Autowired private PartnerRepository partnerRepository;
	@Autowired private AccountRepository accountRepository;
	@Autowired private AppSessionRepository appSessionRepository;
	@Autowired private SellerCategoryRepository sellerCategoryRepository;

	@PostMapping
	public DefaultOut post(@RequestBody PartnerPostIn in) {
		DefaultOut out = new DefaultOut();
		try {
			if (this.validarCampos(in, out, "/api/seller")) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				Partner partner = partnerService.createSeller(in, appSession);

				if (partner == null) {
					throw new Exception("Seller could not be created!");
				}

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@GetMapping
	public DefaultOut get(PartnerGetIn in) {
		PartnerGetOut out = new PartnerGetOut();
		try {
			if (this.validarCampos(in, out, "/api/seller")) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				Account account;
				Partner partner;
				try {
					account = accountRepository.findTop1ByCompanyAndUserAndIsactiveTrue(appSession.getCompany(), appSession.getUser());
					if (account == null) {
						throw new Exception();
					}
					partner = partnerRepository.findTop1ByCompanyAndAccountAndIsactiveTrue(appSession.getCompany(), account);
				} catch (Exception e) {
					throw new Exception("Permission denied!");
				}

				if (StringUtils.isNotBlank(in.getAccountNo())) {
					if (partner == null) {
						throw new Exception("Permission denied!");
					}
					Partner p = partnerRepository.findTop1ByCompanyAndAccountNoAndSellerCategoryNotNull(appSession.getCompany(), in.getAccountNo());
					if (p == null) {
						throw new Exception("Partner not found!");
					}
					out.setPartner(p);
				} else if (in.getCode() != null) {
					if (partner == null) {
						throw new Exception("Permission denied!");
					}
					Partner p = partnerRepository.findTop1ByCompanyAndIdAndSeller(appSession.getCompany(), in.getCode());
					if (p == null) {
						throw new Exception("Partner not found!");
					}
					out.setPartner(p);
				} else {
					out.setPartners(partnerRepository.findByCompanyAndSellerCategoryNotNull(appSession.getCompany()));
				}
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@GetMapping(path = "/category")
	public DefaultOut getCategory(DefaultIn in) {
		SellerCategoryGetOut out = new SellerCategoryGetOut();
		try {
			if (this.validarCampos(in, out, "/api/seller/category", false)) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				List<SellerCategory> categories = sellerCategoryRepository.findByCompany(appSession.getCompany());
				out.setCategories(categories);
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}
