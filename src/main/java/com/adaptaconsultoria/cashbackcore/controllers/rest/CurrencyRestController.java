package com.adaptaconsultoria.cashbackcore.controllers.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.models.Currency;
import com.adaptaconsultoria.cashbackcore.objects.in.DefaultIn;
import com.adaptaconsultoria.cashbackcore.objects.out.CurrencyGetOut;
import com.adaptaconsultoria.cashbackcore.objects.out.DefaultOut;
import com.adaptaconsultoria.cashbackcore.objects.out.error.DefaultError;
import com.adaptaconsultoria.cashbackcore.repositories.AppSessionRepository;
import com.adaptaconsultoria.cashbackcore.repositories.CurrencyRepository;

@RestController
@RequestMapping("/api/currency")
public class CurrencyRestController extends DefaultRestController {

	@Autowired private CurrencyRepository currencyRepository;
	@Autowired private AppSessionRepository appSessionRepository;

	@GetMapping
	public DefaultOut get(DefaultIn in) {
		CurrencyGetOut out = new CurrencyGetOut();
		try {
			if (this.validarCampos(in, out, "/api/currency", false)) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				List<Currency> currencies = currencyRepository.findByCompany(appSession.getCompany());
				out.setCurrencies(currencies);
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}
