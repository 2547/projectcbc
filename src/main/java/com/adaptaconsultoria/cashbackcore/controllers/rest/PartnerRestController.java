package com.adaptaconsultoria.cashbackcore.controllers.rest;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.models.Partner;
import com.adaptaconsultoria.cashbackcore.models.PartnerCategory;
import com.adaptaconsultoria.cashbackcore.objects.in.DefaultIn;
import com.adaptaconsultoria.cashbackcore.objects.in.PartnerGetIn;
import com.adaptaconsultoria.cashbackcore.objects.in.PartnerPostIn;
import com.adaptaconsultoria.cashbackcore.objects.out.DefaultOut;
import com.adaptaconsultoria.cashbackcore.objects.out.PartnerCategoryGetOut;
import com.adaptaconsultoria.cashbackcore.objects.out.PartnerGetOut;
import com.adaptaconsultoria.cashbackcore.objects.out.error.DefaultError;
import com.adaptaconsultoria.cashbackcore.repositories.AppSessionRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PartnerCategoryRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PartnerRepository;
import com.adaptaconsultoria.cashbackcore.services.PartnerService;

@RestController
@RequestMapping("/api/partner")
public class PartnerRestController extends DefaultRestController {

	@Autowired private PartnerService partnerService;
	@Autowired private PartnerRepository partnerRepository;
	@Autowired private AppSessionRepository appSessionRepository;
	@Autowired private PartnerCategoryRepository partnerCategoryRepository;

	@PostMapping
	public DefaultOut post(@RequestBody PartnerPostIn in) {
		DefaultOut out = new DefaultOut();
		try {
			if (this.validarCampos(in, out, "/api/partner")) {

				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				Partner partner = partnerService.createPartner(in, appSession);

				if (partner == null) {
					throw new Exception("Partner could not be created!");
				}

			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@GetMapping
	public DefaultOut get(PartnerGetIn in) {
		PartnerGetOut out = new PartnerGetOut();
		try {
			if (this.validarCampos(in, out, "/api/partner")) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				if (StringUtils.isBlank(in.getAccountNo())) {
					throw new Exception("AccountNo is required!");
				}
				Partner partner = partnerRepository.findTop1ByCompanyAndAccountNoAndPartnerCategoryNotNull(appSession.getCompany(), in.getAccountNo());
				if (partner == null) {
					throw new Exception("Partner not found!");
				}
				out.setPartner(partner);
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

	@GetMapping(path = "/category")
	public DefaultOut get(DefaultIn in) {
		PartnerCategoryGetOut out = new PartnerCategoryGetOut();
		try {
			if (this.validarCampos(in, out, "/api/partner/category", false)) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				List<PartnerCategory> categories = partnerCategoryRepository.findByCompany(appSession.getCompany());
				out.setCategories(categories);
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}
