package com.adaptaconsultoria.cashbackcore.controllers.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.models.Entity;
import com.adaptaconsultoria.cashbackcore.objects.in.DefaultIn;
import com.adaptaconsultoria.cashbackcore.objects.out.DefaultOut;
import com.adaptaconsultoria.cashbackcore.objects.out.EntityGetOut;
import com.adaptaconsultoria.cashbackcore.objects.out.error.DefaultError;
import com.adaptaconsultoria.cashbackcore.repositories.AppSessionRepository;
import com.adaptaconsultoria.cashbackcore.repositories.EntityRepository;

@RestController
@RequestMapping("/api/entity")
public class EntityRestController extends DefaultRestController {

	@Autowired private EntityRepository entityRepository;
	@Autowired private AppSessionRepository appSessionRepository;

	@GetMapping
	public DefaultOut get(DefaultIn in) {
		EntityGetOut out = new EntityGetOut();
		try {
			if (this.validarCampos(in, out, "/api/entity", false)) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				List<Entity> entities = entityRepository.findByCompanyAndIsactiveTrue(appSession.getCompany());
				out.setEntities(entities);
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}
