package com.adaptaconsultoria.cashbackcore.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adaptaconsultoria.cashbackcore.models.AppSession;
import com.adaptaconsultoria.cashbackcore.objects.in.CashbackGetIn;
import com.adaptaconsultoria.cashbackcore.objects.out.CashbackGetOut;
import com.adaptaconsultoria.cashbackcore.objects.out.DefaultOut;
import com.adaptaconsultoria.cashbackcore.objects.out.error.DefaultError;
import com.adaptaconsultoria.cashbackcore.repositories.AppSessionRepository;
import com.adaptaconsultoria.cashbackcore.services.CashbackService;

@RestController
@RequestMapping("/api/cashback")
public class CashbackRestController extends DefaultRestController {

	@Autowired private CashbackService cashbackService;
	@Autowired private AppSessionRepository appSessionRepository;

	@GetMapping
	public DefaultOut get(CashbackGetIn in) {
		CashbackGetOut out = new CashbackGetOut();
		try {
			if (this.validarCampos(in, out, "/api/cashback")) {
				AppSession appSession = appSessionRepository.findTop1ByTokenAndIsactiveTrue(out.getToken());
				out.setCashbacks(cashbackService.getCashbacks(in, appSession));
			}
		} catch (Exception e) {
			DefaultError error = new DefaultError();
			error.setError(e.getMessage());
			out.setHasError(true);
			out.setError(error);
		}
		return out;
	}

}
