package com.adaptaconsultoria.cashbackcore.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.cashbackcore.configs.Converter;
import com.adaptaconsultoria.cashbackcore.models.DocSequence;
import com.adaptaconsultoria.cashbackcore.models.Partner;
import com.adaptaconsultoria.cashbackcore.models.PaymentMethod;
import com.adaptaconsultoria.cashbackcore.models.Payout;
import com.adaptaconsultoria.cashbackcore.repositories.DocSequenceRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PartnerRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PaymentMethodRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PayoutRepository;
import com.adaptaconsultoria.cashbackcore.services.ctrls.PayoutControllerService;
import com.adaptaconsultoria.cashbackcore.utils.SessionUtility;

@Controller
@RequestMapping("/payout")
public class PayoutController {

	@Autowired private PayoutRepository payoutRepository;
	@Autowired private PartnerRepository partnerRepository;
	@Autowired private DocSequenceRepository docSequenceRepository;
	@Autowired private PaymentMethodRepository paymentMethodRepository;
	@Autowired private PayoutControllerService payoutControllerService;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(Partner.class, "partner", new Converter(partnerRepository));
		binder.registerCustomEditor(DocSequence.class, "remittanceSequence", new Converter(docSequenceRepository));
		binder.registerCustomEditor(PaymentMethod.class, "paymentMethod", new Converter(paymentMethodRepository));
	}

    @GetMapping(value = "/list")
	public ModelAndView list() {
		return payoutControllerService.getList();
	}

	@ResponseBody
	@GetMapping(value = "/getlist")
	public ResponseEntity<?> getList(HttpSession session) {
		return ResponseEntity.ok(payoutRepository.findByCompany(SessionUtility.getUserLoggedin().getCompany()));
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> remove(@PathVariable Long id) {
		return ResponseEntity.ok(payoutControllerService.delete(id));
	}

	@GetMapping
	public ModelAndView get(Long id) {
		return payoutControllerService.getPost(id);
	}

	@PostMapping
	public ResponseEntity<?> post(Payout payout) {
		return ResponseEntity.ok(payoutControllerService.save(payout));
	}

}
