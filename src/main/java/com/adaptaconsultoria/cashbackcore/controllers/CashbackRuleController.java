package com.adaptaconsultoria.cashbackcore.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.cashbackcore.configs.Converter;
import com.adaptaconsultoria.cashbackcore.models.Bonus;
import com.adaptaconsultoria.cashbackcore.models.CashbackRule;
import com.adaptaconsultoria.cashbackcore.repositories.BonusRepository;
import com.adaptaconsultoria.cashbackcore.repositories.CashbackRuleRepository;
import com.adaptaconsultoria.cashbackcore.services.ctrls.CashbackRuleControllerService;
import com.adaptaconsultoria.cashbackcore.utils.SessionUtility;

@Controller
@RequestMapping("/cashbackrule")
public class CashbackRuleController {

	@Autowired private BonusRepository bonusRepository;
	@Autowired private CashbackRuleRepository cashbackRuleRepository;
	@Autowired private CashbackRuleControllerService cashbackRuleControllerService;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(Bonus.class, "bonus", new Converter(bonusRepository));
	}

    @GetMapping(value = "/list")
	public ModelAndView list() {
		return cashbackRuleControllerService.getList();
	}

	@ResponseBody
	@GetMapping(value = "/getlist")
	public ResponseEntity<?> getList(HttpSession session) {
		return ResponseEntity.ok(cashbackRuleRepository.findByCompany(SessionUtility.getUserLoggedin().getCompany()));
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> remove(@PathVariable Long id) {
		return ResponseEntity.ok(cashbackRuleControllerService.delete(id));
	}

	@GetMapping
	public ModelAndView get(Long id) {
		return cashbackRuleControllerService.getPost(id);
	}

	@PostMapping
	public ResponseEntity<?> post(CashbackRule cashbackRule) {
		return ResponseEntity.ok(cashbackRuleControllerService.save(cashbackRule));
	}

}
