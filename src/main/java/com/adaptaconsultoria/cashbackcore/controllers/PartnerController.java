package com.adaptaconsultoria.cashbackcore.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.cashbackcore.configs.Converter;
import com.adaptaconsultoria.cashbackcore.models.Account;
import com.adaptaconsultoria.cashbackcore.models.Address;
import com.adaptaconsultoria.cashbackcore.models.City;
import com.adaptaconsultoria.cashbackcore.models.Country;
import com.adaptaconsultoria.cashbackcore.models.Partner;
import com.adaptaconsultoria.cashbackcore.models.PartnerCategory;
import com.adaptaconsultoria.cashbackcore.models.Region;
import com.adaptaconsultoria.cashbackcore.repositories.AccountRepository;
import com.adaptaconsultoria.cashbackcore.repositories.AddressRepository;
import com.adaptaconsultoria.cashbackcore.repositories.CityRepository;
import com.adaptaconsultoria.cashbackcore.repositories.CountryRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PartnerCategoryRepository;
import com.adaptaconsultoria.cashbackcore.repositories.PartnerRepository;
import com.adaptaconsultoria.cashbackcore.repositories.RegionRepository;
import com.adaptaconsultoria.cashbackcore.services.ctrls.PartnerControllerService;
import com.adaptaconsultoria.cashbackcore.utils.SessionUtility;

@Controller
@RequestMapping("/partner")
public class PartnerController {

	@Autowired private CityRepository cityRepository;
	@Autowired private RegionRepository regionRepository;
	@Autowired private CountryRepository countryRepository;
	@Autowired private AddressRepository addressRepository;
	@Autowired private AccountRepository accountRepository;
	@Autowired private PartnerRepository partnerRepository;
	@Autowired private PartnerCategoryRepository partnerCategoryRepository;
	@Autowired private PartnerControllerService partnerControllerService;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(PartnerCategory.class, "partnerCategory", new Converter(partnerCategoryRepository));
		binder.registerCustomEditor(Partner.class, "leader", new Converter(partnerRepository));
		binder.registerCustomEditor(Account.class, "account", new Converter(accountRepository));
		binder.registerCustomEditor(Address.class, "address", new Converter(addressRepository));
		binder.registerCustomEditor(Country.class, "country", new Converter(countryRepository));
		binder.registerCustomEditor(Region.class, "region", new Converter(regionRepository));
		binder.registerCustomEditor(City.class, "city", new Converter(cityRepository));
	}

    @GetMapping(value = "/list")
	public ModelAndView list() {
		return partnerControllerService.getPartnerList();
	}

	@ResponseBody
	@GetMapping(value = "/getlist")
	public ResponseEntity<?> getList(HttpSession session) {
		return ResponseEntity.ok(partnerRepository.findByCompanyAndPartnerCategoryNotNull(SessionUtility.getUserLoggedin().getCompany()));
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> remove(@PathVariable Long id) {
		return ResponseEntity.ok(partnerControllerService.delete(id));
	}

	@GetMapping
	public ModelAndView get(Long id) {
		return partnerControllerService.getPartnerPost(id);
	}

	@PostMapping
	public ResponseEntity<?> post(Partner partner, Address address) {
		partner.setAddress(address);
		return ResponseEntity.ok(partnerControllerService.savePartner(partner));
	}

}
