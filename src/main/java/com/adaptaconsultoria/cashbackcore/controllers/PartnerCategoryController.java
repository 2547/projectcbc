package com.adaptaconsultoria.cashbackcore.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.cashbackcore.models.PartnerCategory;
import com.adaptaconsultoria.cashbackcore.repositories.PartnerCategoryRepository;
import com.adaptaconsultoria.cashbackcore.services.ctrls.PartnerCategoryControllerService;
import com.adaptaconsultoria.cashbackcore.utils.SessionUtility;

@Controller
@RequestMapping("/partnercategory")
public class PartnerCategoryController {

	@Autowired private PartnerCategoryRepository partnerCategoryRepository;
	@Autowired private PartnerCategoryControllerService partnerCategoryControllerService;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
	}

    @GetMapping(value = "/list")
	public ModelAndView list() {
		return partnerCategoryControllerService.getList();
	}

	@ResponseBody
	@GetMapping(value = "/getlist")
	public ResponseEntity<?> getList(HttpSession session) {
		return ResponseEntity.ok(partnerCategoryRepository.findByCompany(SessionUtility.getUserLoggedin().getCompany()));
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> remove(@PathVariable Long id) {
		return ResponseEntity.ok(partnerCategoryControllerService.delete(id));
	}

	@GetMapping
	public ModelAndView get(Long id) {
		return partnerCategoryControllerService.getPost(id);
	}

	@PostMapping
	public ResponseEntity<?> post(PartnerCategory partnerCategory) {
		return ResponseEntity.ok(partnerCategoryControllerService.save(partnerCategory));
	}

}
