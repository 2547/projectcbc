package com.adaptaconsultoria.cashbackcore.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.cashbackcore.configs.Converter;
import com.adaptaconsultoria.cashbackcore.models.Account;
import com.adaptaconsultoria.cashbackcore.models.Entity;
import com.adaptaconsultoria.cashbackcore.repositories.AccountRepository;
import com.adaptaconsultoria.cashbackcore.repositories.EntityRepository;
import com.adaptaconsultoria.cashbackcore.services.ctrls.EntityControllerService;
import com.adaptaconsultoria.cashbackcore.utils.SessionUtility;

@Controller
@RequestMapping("/entity")
public class EntityController {

	@Autowired private EntityRepository entityRepository;
	@Autowired private AccountRepository accountRepository;
	@Autowired private EntityControllerService entityControllerService;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(Account.class, "account", new Converter(accountRepository));
	}

    @GetMapping(value = "/list")
	public ModelAndView list() {
		return entityControllerService.getList();
	}

	@ResponseBody
	@GetMapping(value = "/getlist")
	public ResponseEntity<?> getList(HttpSession session) {
		return ResponseEntity.ok(entityRepository.findByCompany(SessionUtility.getUserLoggedin().getCompany()));
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> remove(@PathVariable Long id) {
		return ResponseEntity.ok(entityControllerService.delete(id));
	}

	@GetMapping
	public ModelAndView get(Long id) {
		return entityControllerService.getPost(id);
	}

	@PostMapping
	public ResponseEntity<?> post(Entity entity) {
		return ResponseEntity.ok(entityControllerService.save(entity));
	}

}
