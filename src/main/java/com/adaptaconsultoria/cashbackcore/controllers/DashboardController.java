package com.adaptaconsultoria.cashbackcore.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DashboardController {
	
	@GetMapping(path = "/dashboard")
	public ModelAndView home(HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("dashboard");
		modelAndView.addObject("page", "Dashboard");
		modelAndView.addObject("menuId", "dashboard-menu");
		return modelAndView;
	}

}
