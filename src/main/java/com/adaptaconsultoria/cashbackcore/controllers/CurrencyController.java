package com.adaptaconsultoria.cashbackcore.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.cashbackcore.models.Currency;
import com.adaptaconsultoria.cashbackcore.repositories.CurrencyRepository;
import com.adaptaconsultoria.cashbackcore.services.ctrls.CurrencyControllerService;
import com.adaptaconsultoria.cashbackcore.utils.SessionUtility;

@Controller
@RequestMapping("/currency")
public class CurrencyController {

	@Autowired private CurrencyRepository currencyRepository;
	@Autowired private CurrencyControllerService currencyControllerService;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
	}

    @GetMapping(value = "/list")
	public ModelAndView list() {
		return currencyControllerService.getList();
	}

	@ResponseBody
	@GetMapping(value = "/getlist")
	public ResponseEntity<?> getList(HttpSession session) {
		return ResponseEntity.ok(currencyRepository.findByCompany(SessionUtility.getUserLoggedin().getCompany()));
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> remove(@PathVariable Long id) {
		return ResponseEntity.ok(currencyControllerService.delete(id));
	}

	@GetMapping
	public ModelAndView get(Long id) {
		return currencyControllerService.getPost(id);
	}

	@PostMapping
	public ResponseEntity<?> post(Currency currency) {
		return ResponseEntity.ok(currencyControllerService.save(currency));
	}

}
