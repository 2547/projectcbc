package com.adaptaconsultoria.cashbackcore.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.adaptaconsultoria.cashbackcore.configs.Converter;
import com.adaptaconsultoria.cashbackcore.models.AccountPayout;
import com.adaptaconsultoria.cashbackcore.models.AccountPayoutEvent;
import com.adaptaconsultoria.cashbackcore.repositories.AccountPayoutRepository;
import com.adaptaconsultoria.cashbackcore.services.ctrls.AccountPayoutEventControllerService;

@Controller
@RequestMapping("/payoutevent")
public class AccountPayoutEventController {

	@Autowired private AccountPayoutRepository accountPayoutRepository;
	@Autowired private AccountPayoutEventControllerService accountPayoutEventControllerService;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		binder.registerCustomEditor(AccountPayout.class, "accountPayout", new Converter(accountPayoutRepository));
	}

	@GetMapping
	public ModelAndView get() {
		return accountPayoutEventControllerService.getPost();
	}

	@PostMapping
	public ResponseEntity<?> post(AccountPayoutEvent accountPayoutEvent) {
		return ResponseEntity.ok(accountPayoutEventControllerService.save(accountPayoutEvent));
	}

}
