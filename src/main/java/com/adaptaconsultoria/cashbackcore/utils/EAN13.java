package com.adaptaconsultoria.cashbackcore.utils;

import org.apache.commons.lang3.StringUtils;

public class EAN13 {

	private EAN13() {
	}

	public static String addDV(String number) {
		number = StringUtils.leftPad(number, 12, "0");
		number += calcDV(number);
		return number;
	}

	public static int calcDV(String number) {

		Integer verificar1 = Integer.parseInt(number.substring(0, 1));
		Integer verificar2 = Integer.parseInt(number.substring(1, 2));
		Integer verificar3 = Integer.parseInt(number.substring(2, 3));
		Integer verificar4 = Integer.parseInt(number.substring(3, 4));
		Integer verificar5 = Integer.parseInt(number.substring(4, 5));
		Integer verificar6 = Integer.parseInt(number.substring(5, 6));
		Integer verificar7 = Integer.parseInt(number.substring(6, 7));
		Integer verificar8 = Integer.parseInt(number.substring(7, 8));
		Integer verificar9 = Integer.parseInt(number.substring(8, 9));
		Integer verificar10 = Integer.parseInt(number.substring(9, 10));
		Integer verificar11 = Integer.parseInt(number.substring(10, 11));
		Integer verificar12 = Integer.parseInt(number.substring(11, 12));

		Integer resultado1 =   verificar1 * 1;
		Integer resultado2 =   verificar2 * 3;
		Integer resultado3 =   verificar3 * 1;
		Integer resultado4 =   verificar4 * 3;
		Integer resultado5 =   verificar5 * 1;
		Integer resultado6 =   verificar6 * 3;
		Integer resultado7 =   verificar7 * 1;
		Integer resultado8 =   verificar8 * 3;
		Integer resultado9 =   verificar9 * 1;
		Integer resultado10 = verificar10 * 3;
		Integer resultado11 = verificar11 * 1;
		Integer resultado12 = verificar12 * 3;

		Integer resultadoSomado = resultado1 + resultado2 + resultado3 + resultado4 + resultado5 + resultado6 + 
				resultado7 + resultado8 + resultado9 + resultado10 + resultado11 + resultado12;  

		Double resultadoConverter = new Double(resultadoSomado);
		Double resultado = resultadoConverter / 10;
		resultado = Math.floor(resultado);
		resultado = resultado + 1;
		resultado = resultado * 10;
		resultado = resultado - resultadoSomado;

		int dv = resultado.intValue();
		if (dv > 9) {
			dv = 0;
		}
		return dv;

	}

}
