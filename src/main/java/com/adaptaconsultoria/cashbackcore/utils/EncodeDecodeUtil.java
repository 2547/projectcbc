package com.adaptaconsultoria.cashbackcore.utils;

import java.util.Base64;

public class EncodeDecodeUtil {
	public static String encodeString(String string) {
		try {
			return Base64.getEncoder().encodeToString(string.getBytes());
		} catch (Exception e) {
			return null;
		}
	}
	
	public static String decodeString(String string) {
		try {
			byte[] decodedBytes = Base64.getDecoder().decode(string);
			return new String(decodedBytes);
		} catch (Exception e) {
			return null;
		}
	}
}
