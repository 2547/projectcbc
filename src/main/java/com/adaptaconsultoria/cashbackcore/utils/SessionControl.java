package com.adaptaconsultoria.cashbackcore.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.adaptaconsultoria.cashbackcore.models.User;
import com.google.gson.Gson;

public class SessionControl
{
	public static final String PARAM_NAME = "log";
	public static final String USER_NAME = "username";
	public static final String USER_ID = "userid";
	public static final String USER_COMPANY_ID = "usercompanyid";

	public static void setAtributo(HttpSession session, String name, String value)
	{
		session.setAttribute(name, value);
	}

	public static String getAttribute(HttpSession session, String name)
	{
		return (String) session.getAttribute(name);
	}
	
	public static void setUser(HttpSession session, User users) throws Exception 
	{
		setAtributo(session, USER_NAME, users.getName() );
		setAtributo(session, USER_ID, users.getId().toString() );
		setAtributo(session, USER_COMPANY_ID, users.getCompany().getId().toString() );
	}

	public static void setAtributoParams(HttpServletRequest request, HttpSession session)
	{
		Gson gson = new Gson(); 
		String json = gson.toJson(request.getParameterMap());
		session.setAttribute(PARAM_NAME, json);
	}
	
	public static String getAtributoParams(HttpSession session)
	{
		return (String) session.getAttribute(PARAM_NAME);
	}
}
