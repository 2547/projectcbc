package com.adaptaconsultoria.cashbackcore.utils;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class DateUtility implements Serializable {

	private static final long serialVersionUID = 1L;
	
    /**
     * Não é possivel instanciar DateUtils.
     */
    private DateUtility(){}

    /**
     * Retorna o valor do horário minimo para a data de referencia passada.
     * <BR>
     * <BR> Por exemplo se a data for "30/01/2009 as 17h:33m:12s e 299ms" a data
     * retornada por este metodo será "30/01/2009 as 00h:00m:00s e 000ms".
     * @param date de referencia.
     * @return {@link Date} que representa o horário minimo para dia informado.
     */
    public static Date lowDateTime(Date date) {
        Calendar aux = Calendar.getInstance();
        aux.setTime(date);
        toOnlyDate(aux); //zera os parametros de hour,min,sec,milisec
        return aux.getTime();
    }


    public static String lowDateTimeToString(Date date) {
        return lowDateTimeToString(date, "dd/MM/yyyy HH:mm:ss");
    }
    public static String lowDateTimeToString(Date date, String format) {
        Calendar aux = Calendar.getInstance();
        aux.setTime(date);
        toOnlyDate(aux); //zera os parametros de hour,min,sec,milisec
        date = aux.getTime();

        SimpleDateFormat fmt = new SimpleDateFormat(format);
        return fmt.format(date);
    }


    /**
	 * Retorna o valor do horário maximo para a data de referencia passada. <BR>
	 * <BR>
	 * Por exemplo se a data for "30/01/2009 as 17h:33m:12s e 299ms" a data
	 * retornada por este metodo será "30/01/2009 as 23h:59m:59s e 999ms".
	 * 
	 * @param date
	 *            de referencia.
	 * @return {@link Date} que representa o horário maximo para dia informado.
	 */
	public static Date highDateTime(Date date) {
		// Calendar aux = Calendar.getInstance();
		// aux.setTime(date);
		// toOnlyDate(aux); //zera os parametros de hour,min,sec,milisec
		// aux.roll(Calendar.DATE, true); //vai para o dia seguinte
		// aux.roll(Calendar.MILLISECOND, false); //reduz 1 milisegundo
		// return aux.getTime();

		Calendar aux = Calendar.getInstance();
		aux.setTime(date);
		toLastHourDay(aux); // Ultimo horario do dia
		return aux.getTime();
	}

	public static String highDateTimeToString(Date date) {
        return highDateTimeToString(date, "dd/MM/yyyy HH:mm:ss");
	}
	public static String highDateTimeToString(Date date, String format) {
		Calendar aux = Calendar.getInstance();
		aux.setTime(date);
		toLastHourDay(aux); // Ultimo horario do dia
		date = aux.getTime();
        SimpleDateFormat fmt = new SimpleDateFormat(format);
        return fmt.format(date);
	}
	public static String dateToString(Date date, String format) {
        SimpleDateFormat fmt = new SimpleDateFormat(format);
        return fmt.format(date);
	}

    /**
     * Zera todas as referencias de hora, minuto, segundo e milesegundo do
     * {@link Calendar}.
     * @param date a ser modificado.
     */
    public static void toOnlyDate(Calendar date) {
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);
    }
    
    public static void toLastHourDay(Calendar date) {
        date.set(Calendar.HOUR_OF_DAY, 23);
        date.set(Calendar.MINUTE, 59);
        date.set(Calendar.SECOND, 59);
        date.set(Calendar.MILLISECOND, 999);
    }
    
	public Date returnCurrentDate() {
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		Date dataAtual = null;

		try {
			dataAtual = formatador.parse(formatador.format(new Date()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return dataAtual;
	}

	public static Date formatDate(String dataFormatacao) {

		Date dataFormatada;
		try {
			SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			dataFormatada = formatador.parse(formatador.format(dataFormatacao));
		} catch (Exception e) {
			dataFormatada = null;
		}

		if (dataFormatada != null) {
			return dataFormatada;
		}

		try {
			SimpleDateFormat formatador = new SimpleDateFormat("dd-MMM-yyyy");
			dataFormatada = formatador.parse(dataFormatacao);
		} catch (Exception e) {
			dataFormatada = null;
		}

		if (dataFormatada != null) {
			return dataFormatada;
		}

		try {
			SimpleDateFormat formatador = new SimpleDateFormat("yyyy-MM-dd");
			dataFormatada = formatador.parse(dataFormatacao);
		} catch (Exception e) {
			dataFormatada = null;
		}

		if (dataFormatada != null) {
			return dataFormatada;
		}

		return null;
	}

	public static Date formatDate(String data, String formatacao) {

		Date dataFormatada;
		SimpleDateFormat formatador = new SimpleDateFormat(formatacao);
		try {
			dataFormatada = formatador.parse(formatador.format(data));
		} catch (Exception e) {
			dataFormatada = null;
		}

		if (dataFormatada != null) {
			return dataFormatada;
		}

		try {
			dataFormatada = formatador.parse(data);
		} catch (Exception e) {
			dataFormatada = null;
		}

		if (dataFormatada != null) {
			return dataFormatada;
		}

		return null;
	}

	public Date formatDateXMLIn(String dataFormatacao) {
		Date dataFormatada = null;
        
        String ano = dataFormatacao.substring(0, 4);
        String mes = dataFormatacao.substring(5, 7);
        String dia = dataFormatacao.substring(8, 10);
        String hora = dataFormatacao.substring(11, 13);
        String minuto = dataFormatacao.substring(14, 16);
        String segundo = dataFormatacao.substring(17, 19);
        
        Integer longAno = Integer.parseInt(ano);
        Integer longMes = Integer.parseInt(mes);
        Integer longDia = Integer.parseInt(dia);
        Integer longHora = Integer.parseInt(hora);
        Integer longMinuto = Integer.parseInt(minuto);
        Integer longSegundo = Integer.parseInt(segundo);
                
        String data = longDia + "/" + longMes + "/" + longAno + " " + longHora + ":" + longMinuto + ":" + longSegundo;
        
		try {
	        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	        dataFormatada = formato.parse(data);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dataFormatada;
	}

	/**
	 * Adiciona quantidade de dias na data.
	 *
	 * @param data
	 * @param qtd
	 * @return
	 */
	public static Date addDay(Date data, int qtd) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		cal.add(Calendar.DAY_OF_MONTH, qtd);
		return cal.getTime();
	}
 
	/**
	 * Adiciona quantidade de meses na data.
	 *
	 * @param data
	 * @param qtd
	 * @return
	 */
	public static Date addMonth(Date data, int qtd) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		cal.add(Calendar.MONTH, qtd);
		return cal.getTime();
	}
 
	/**
	 * Adiciona quantidade de anos na data.
	 *
	 * @param data
	 * @param qtd
	 * @return
	 */
	public static Date addYear(Date data, int qtd) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		cal.add(Calendar.YEAR, qtd);
		return cal.getTime();
	}

	/**
	 * Subtrai quantidade de dias na data.
	 *
	 * @param data
	 * @param qtd
	 * @return
	 */
	public static Date subtractDay(Date data, int qtd) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		cal.add(Calendar.DAY_OF_MONTH, (qtd * -1));
		return cal.getTime();
	}

	/**
	 * Quebra periodo por meses
	 *
	 * @param date1
	 * @param date2
	 * @return dates
	 */
	public static List<Date> splitDateForMonth(Date date1, Date date2) {
		Calendar cal1 = Calendar.getInstance();
		cal1.setTime(date1);

		Calendar cal2 = Calendar.getInstance();
		cal2.setTime(date2);

		int mes1 = cal1.get(Calendar.MONTH);
		int mes2 = cal2.get(Calendar.MONTH);

		int qtdMes = ((mes2 - mes1) + 1);

		List<Date> dates = new ArrayList<>();
		for (int i = 0; i < qtdMes; i++) {

			Calendar data = Calendar.getInstance();
			data.setTime(cal1.getTime());
			data.set(Calendar.DAY_OF_MONTH, 1);
			data.add(Calendar.MONTH, i);

			dates.add(data.getTime());
		}

		return dates;
	}

	/**
	 * Primeiro dia do mês
	 *
	 * @param date
	 * @return date
	 */
	public static Date firstDayOfMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}

	/**
	 * Último dia do mês
	 *
	 * @param date
	 * @return date
	 */
	public static Date lastDayOfMonth(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.add(Calendar.MONTH, 1);
		cal.add(Calendar.DAY_OF_MONTH, -1);
		return cal.getTime();
	}

	public static Date firstDay(Integer dia) {
		Calendar data = Calendar.getInstance();
		data.setTime(new Date());
		data.set(Calendar.DAY_OF_MONTH, dia);

		Calendar amanha = Calendar.getInstance();
		amanha.setTime(new Date());
		amanha.add(Calendar.DAY_OF_MONTH, 1);
		
		while (amanha.after(data)) {
			data.add(Calendar.MONTH, 1);
		}
		
		return data.getTime();
	}

}
