package com.adaptaconsultoria.cashbackcore.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "company_config")
public class CompanyConfig implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "company_config_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private Date updated = new Date();

	@ManyToOne
	@JoinColumn(name = "updatedby")
	private User updatedBy;

	@NotNull
	private Boolean isactive = true;

	@Size(max = 255)
	@Column(name = "email_host")
	private String emailHost;

	@Size(max = 255)
	@Column(name = "email_password")
	private String emailPassword;

	@Size(max = 10)
	@Column(name = "email_port")
	private String emailPort;

	@Size(max = 255)
	@Column(name = "email_user")
	private String emailUser;

	@Size(max = 255)
	@Column(name = "logo_big")
	private String logoBig;

	@Size(max = 255)
	@Column(name = "logo_medium")
	private String logoMedium;

	@Size(max = 255)
	@Column(name = "logo_small")
	private String logoSmall;

	@Size(max = 255)
	@Column(name = "logo_login")
	private String logoLogin;

	@Size(max = 255)
	@Column(name = "login_backgroud")
	private String loginBackgroud;

	@ManyToOne
	@JoinColumn(name = "app_id")
	private App app;

//	@Column(name = "url_app")
//	private String urlApp;

//	@Column(name = "endpoint_remote_authentication")
//	private String endpointRemoteAuthentication;

//	@Column(name = "endpoint_remote_recovery")
//	private String endpointRemoteRecovery;

//	@Column(name = "url_alc")
//	private String urlAlc;

//	@Column(name = "endpoint_alc_authentication")
//	private String endpointAlcAuthentication;

//	@Column(name = "endpoint_alc_sendemail")
//	private String endpointAlcSendemail;

//	@Column(name = "code_email_alc")
//	private String codeEmailAlc;

}
