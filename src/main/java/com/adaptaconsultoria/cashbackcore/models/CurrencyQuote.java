package com.adaptaconsultoria.cashbackcore.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "currency_quote")
public class CurrencyQuote implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "currency_quote_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private Date updated = new Date();

	@ManyToOne
	@JoinColumn(name = "updatedby")
	private User updatedBy;

	@NotNull
	private Boolean isactive = true;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "currency_id")
	private Currency currency;

	@NotNull
	private Date quoted;

	@Size(max = 10)
	private String delay;

	@Column(name = "last_price")
	private BigDecimal lastPrice;

	@Column(name = "buy_price")
	private BigDecimal buyPrice;

	@Column(name = "sell_price")
	private BigDecimal sellPrice;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "quoted_currency_id")
	private Currency quotedCurrency;

}