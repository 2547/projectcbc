package com.adaptaconsultoria.cashbackcore.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "sale")
public class Sale implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "sale_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private Date updated = new Date();

	@ManyToOne
	@JoinColumn(name = "updatedby")
	private User updatedBy;

	@NotNull
	private Boolean isactive = true;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "partner_id")
	private Partner partner;

	@Size(max = 40)
	private String documentno;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "sale_currency_id")
	private Currency saleCurrency;

	@NotNull
	@Column(name = "sale_amount")
	private BigDecimal saleAmount;

	@NotNull
	@Column(name = "pc_cashback")
	private BigDecimal pcCashback;

	@NotNull
	@Column(name = "sale_cashback")
	private BigDecimal saleCashback;

	@NotNull
	@Column(name = "local_time")
	private Date localTime;

	@NotNull
	@Column(name = "server_time")
	private Date serverTime = new Date();

	@ManyToOne
	@JoinColumn(name = "country_id")
	private Country country;

	@Size(max = 13)
	@Column(name = "accountno")
	private String accountNo;

	@Size(max = 20)
	private String taxid;

	@Size(max = 20)
	private String phone;

	@ManyToOne
	@JoinColumn(name = "account_id")
	private Account account;

	@ManyToOne
	@JoinColumn(name = "currency_id")
	private Currency currency;

	private Date quoted;

	private BigDecimal quotedat;

	private BigDecimal cashback;

	private Boolean isvalid = true;

	private Boolean indispute = false;

	private Boolean ispaid = false;

	@Column(name = "company_amount")
	private BigDecimal companyAmount;

	@Column(name = "platform_amount")
	private BigDecimal platformAmount;

}
