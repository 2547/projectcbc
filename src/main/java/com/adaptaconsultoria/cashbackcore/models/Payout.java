package com.adaptaconsultoria.cashbackcore.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "payout")
public class Payout implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "payout_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private Date updated = new Date();

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "updatedby")
	private User updatedBy;

	@NotNull
	private Boolean isactive = true;

	@NotEmpty
	@Size(max = 255)
	private String name;

	@Column(columnDefinition = "text")
	private String description;

	@Size(max = 255)
	@Column(name = "value_mask")
	private String valueMask;

	@Column(name = "value_length")
	private Long valueLength;

	@ManyToOne
	@JoinColumn(name = "remittance_docsequence_id")
	private DocSequence remittanceSequence;

	@Size(max = 255)
	@Column(name = "remittance_classname")
	private String remittanceClassname;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "paymentmethod_id")
	private PaymentMethod paymentMethod;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "partner_id")
	private Partner partner;

	@NotNull
	@Column(name = "value_required")
	private Boolean valueRequired;

}
