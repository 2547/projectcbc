package com.adaptaconsultoria.cashbackcore.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "account_payout")
public class AccountPayout implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "account_payout_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private Date updated = new Date();

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "updatedby")
	private User updatedBy;

	@NotNull
	private Boolean isactive = true;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "account_id")
	private Account account;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "payout_id")
	private Payout payout;

	private String value;

	@NotNull
	@Column(name = "isblocked")
	private Boolean isBlocked = true;

	@NotNull
	@Column(name = "iscanceled")
	private Boolean isCanceled = false;

}
