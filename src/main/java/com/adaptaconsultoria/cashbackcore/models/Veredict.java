package com.adaptaconsultoria.cashbackcore.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import com.adaptaconsultoria.cashbackcore.models.enumeration.Decision;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "veredict")
public class Veredict implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "veredict_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private Date updated = new Date();

	@ManyToOne
	@JoinColumn(name = "updatedby")
	private User updatedBy;

	@NotNull
	private Boolean isactive = true;

	@NotEmpty
	@Size(max = 80)
	private String name;

	@Column(columnDefinition = "text")
	private String description;

	@NotEmpty
	@Column(columnDefinition = "text")
	private String notes;

	@NotNull
	@Enumerated(EnumType.STRING)
	private Decision decision;

}