package com.adaptaconsultoria.cashbackcore.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "log_access")
public class LogAccess implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "log_access_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@NotNull
	private Boolean isactive = false;

	@Size(max = 255)
	private String url;

	@Size(max = 255)
	private String uri;

	@Size(max = 255)
	private String method;

	@Size(max = 255)
	private String context;

	@Column(name = "query_string", columnDefinition = "text")
	private String queryString;

	@Column(length = Integer.MAX_VALUE, columnDefinition = "text")
	private String parameters;

	@Size(max = 255)
	@Column(name = "device_name")
	private String deviceName;

	@Size(max = 255)
	private String email;

	@Size(max = 255)
	@Column(name = "ip_address")
	private String ipAddress;

	@Size(max = 255)
	private String name;

}