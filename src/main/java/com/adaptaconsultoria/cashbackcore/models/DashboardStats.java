package com.adaptaconsultoria.cashbackcore.models;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Subselect;

import lombok.Data;

@Data
@Entity
@Subselect("")
public class DashboardStats {

	@Id
	@Column(name = "p_id")
	private String id;

	@Column(name = "p_name")
	private String name;

	@Column(name = "p_value")
	private BigDecimal value;

	@Column(name = "p_color")
	private String color;

	@Column(name = "p_icon")
	private String icon;

}
