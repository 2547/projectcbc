package com.adaptaconsultoria.cashbackcore.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Audited
@Table(name = "account")
public class Account implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "account_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private Date updated = new Date();

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "updatedby")
	private User updatedBy;

	@NotNull
	private Boolean isactive = true;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "country_id")
	private Country country;

	@NotEmpty
	@Size(max = 13)
	@Column(name = "accountno")
	private String accountNo;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "users_id")
	private User user;

	@Size(max = 20)
	private String taxid;

	@NotNull
	@Column(name = "personal_value")
	private BigDecimal personalValue = BigDecimal.ZERO;

	@NotNull
	@Column(name = "earning_value")
	private BigDecimal earningValue = BigDecimal.ZERO;

	@NotNull
	@Column(name = "returned_value")
	private BigDecimal returnedValue = BigDecimal.ZERO;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "sponsor_account_id")
	private Account sponsorAccount;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "currency_id")
	private Currency currency;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "address_id")
	private Address address;

	@JsonIgnore
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "account", cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE}, orphanRemoval = true)
	private List<AccountEntity> entities = new ArrayList<>();


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Boolean getIsactive() {
		return isactive;
	}

	public void setIsactive(Boolean isactive) {
		this.isactive = isactive;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getTaxid() {
		return taxid;
	}

	public void setTaxid(String taxid) {
		this.taxid = taxid;
	}

	public BigDecimal getPersonalValue() {
		return personalValue;
	}

	public void setPersonalValue(BigDecimal personalValue) {
		this.personalValue = personalValue;
	}

	public BigDecimal getEarningValue() {
		return earningValue;
	}

	public void setEarningValue(BigDecimal earningValue) {
		this.earningValue = earningValue;
	}

	public BigDecimal getReturnedValue() {
		return returnedValue;
	}

	public void setReturnedValue(BigDecimal returnedValue) {
		this.returnedValue = returnedValue;
	}

	public Account getSponsorAccount() {
		return sponsorAccount;
	}

	public void setSponsorAccount(Account sponsorAccount) {
		this.sponsorAccount = sponsorAccount;
	}

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public List<AccountEntity> getEntities() {
		return entities;
	}

	public void setEntities(List<AccountEntity> entities) {
		this.entities = entities;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
