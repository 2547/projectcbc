package com.adaptaconsultoria.cashbackcore.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "app")
public class App implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "app_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	@Column(length = 255)
	private String name;

	@NotNull
	@Column(length = 256)
	private String token;

	@NotNull
	@Column(length = 256)
	private String password;

	@NotNull
	private Boolean isactive = false;

}
