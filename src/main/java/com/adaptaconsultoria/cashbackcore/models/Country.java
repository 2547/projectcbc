package com.adaptaconsultoria.cashbackcore.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.envers.Audited;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Audited
@Table(name = "country")
public class Country implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "country_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	@NotNull
	private Date created = new Date();

	@JsonIgnore
	@NotNull
	@ManyToOne
	@JoinColumn(name = "createdby")
	private User createdBy;

	@NotNull
	private Date updated = new Date();

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "updatedby")
	private User updatedBy;

	@NotNull
	private Boolean isactive = true;

	@NotEmpty
	@Size(max = 10)
	@Column(name = "iso_code")
	private String isoCode;

	@NotEmpty
	@Size(max = 255)
	private String name;

	@Size(max = 10)
	private String ddi;

	@Size(max = 10)
	@Column(name = "iso_language")
	private String isoLanguage;

	@Size(max = 255)
	private String flag;

	@Column(name = "address_format_line", columnDefinition = "text")
	private String addressFormatLine;

	@Column(name = "address_format_label", columnDefinition = "text")
	private String addressFormatLabel;

	@ManyToOne
	@JoinColumn(name = "leader_id")
	private Partner leader;

	@NotNull
	private Integer code;

	@ManyToOne
	@JoinColumn(name = "account_sequence_id")
	private DocSequence accountSequence;

	@Column(name = "regionname")
	private String regionName;

}