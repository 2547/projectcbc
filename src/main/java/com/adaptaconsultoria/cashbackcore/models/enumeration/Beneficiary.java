package com.adaptaconsultoria.cashbackcore.models.enumeration;

public enum Beneficiary {

	C("Customer"),
	S("Sponsor"),
	L("Leader"),
	P("Platform");

	private String description;

	Beneficiary(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

}
