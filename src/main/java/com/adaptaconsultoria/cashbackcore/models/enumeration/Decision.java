package com.adaptaconsultoria.cashbackcore.models.enumeration;

public enum Decision {

	A("Accepted"),
	R("Rejected"),
	I("Inconclusive");

	private String descricao;

	Decision(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
