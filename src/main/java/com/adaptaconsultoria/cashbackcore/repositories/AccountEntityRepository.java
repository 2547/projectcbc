package com.adaptaconsultoria.cashbackcore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.AccountEntity;
import com.adaptaconsultoria.cashbackcore.models.Company;

public interface AccountEntityRepository extends JpaRepository<AccountEntity, Long> {

	AccountEntity findTop1ByCompanyAndId(Company company, Long id);

}
