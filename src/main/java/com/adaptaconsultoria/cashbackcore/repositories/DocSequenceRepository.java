package com.adaptaconsultoria.cashbackcore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.DocSequence;

public interface DocSequenceRepository extends JpaRepository<DocSequence, Long> {

	List<DocSequence> findByCompany(Company company);

}
