package com.adaptaconsultoria.cashbackcore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.Company;

public interface CompanyRepository extends JpaRepository<Company, Long> {

}
