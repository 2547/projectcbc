package com.adaptaconsultoria.cashbackcore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.Bonus;

public interface BonusRepository extends JpaRepository<Bonus, Long> {

	List<Bonus> findByCompany(Company company);

	Bonus findTop1ByCompanyAndId(Company company, Long id);

}
