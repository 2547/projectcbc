package com.adaptaconsultoria.cashbackcore.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.Account;
import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.Partner;
import com.adaptaconsultoria.cashbackcore.models.Sale;

public interface SaleRepository extends JpaRepository<Sale, Long> {

	/* ******* */
	/* Account */
	/* ******* */
	List<Sale> findByCompanyAndAccount(Company company, Account account);

	List<Sale> findByCompanyAndAccountAndLocalTimeGreaterThanEqual(Company company, Account account, Date localTimeStart);

	List<Sale> findByCompanyAndAccountAndLocalTimeGreaterThanEqualAndLocalTimeLessThanEqual(Company company, Account account, Date localTimeStart, Date localTimeEnd);

	/* ******* */
	/* Partner */
	/* ******* */
	List<Sale> findByCompanyAndPartner(Company company, Partner partner);

	List<Sale> findByCompanyAndPartnerAndLocalTimeGreaterThanEqual(Company company, Partner partner, Date localTimeStart);

	List<Sale> findByCompanyAndPartnerAndLocalTimeGreaterThanEqualAndLocalTimeLessThanEqual(Company company, Partner partner, Date localTimeStart, Date localTimeEnd);

}
