package com.adaptaconsultoria.cashbackcore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.PartnerCategory;

public interface PartnerCategoryRepository extends JpaRepository<PartnerCategory, Long> {

	List<PartnerCategory> findByCompany(Company company);

	PartnerCategory findTop1ByCompanyAndId(Company company, Long id);

}
