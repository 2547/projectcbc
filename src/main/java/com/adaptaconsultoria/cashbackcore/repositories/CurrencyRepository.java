package com.adaptaconsultoria.cashbackcore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.Currency;

public interface CurrencyRepository extends JpaRepository<Currency, Long> {

	List<Currency> findByCompany(Company company);

	Currency findTop1ByCompanyAndId(Company company, Long id);

	Currency findByCompanyAndCode(Company company, String code);

}
