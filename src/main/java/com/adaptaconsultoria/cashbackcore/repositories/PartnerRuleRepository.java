package com.adaptaconsultoria.cashbackcore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.cashbackcore.models.PartnerRule;

public interface PartnerRuleRepository extends JpaRepository<PartnerRule, Long> {
	@Query("select p from PartnerRule p where p.partner.id = :id")
	List<PartnerRule> findByPartnerId(@Param("id") Long id);
}
