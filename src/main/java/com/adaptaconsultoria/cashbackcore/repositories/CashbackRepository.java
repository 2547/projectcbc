package com.adaptaconsultoria.cashbackcore.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.Account;
import com.adaptaconsultoria.cashbackcore.models.Cashback;
import com.adaptaconsultoria.cashbackcore.models.Company;

public interface CashbackRepository extends JpaRepository<Cashback, Long> {

	List<Cashback> findByCompanyAndAccount(Company company, Account account);

	List<Cashback> findByCompanyAndAccountAndDateSaleGreaterThanEqual(Company company, Account account, Date dateSaleStart);

	List<Cashback> findByCompanyAndAccountAndDateSaleGreaterThanEqualAndDateSaleLessThanEqual(Company company, Account account, Date dateSaleStart, Date dateSaleEnd);

}
