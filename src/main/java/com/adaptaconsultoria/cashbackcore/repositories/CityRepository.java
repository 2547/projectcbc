package com.adaptaconsultoria.cashbackcore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.City;
import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.Region;

public interface CityRepository extends JpaRepository<City, Long> {

	City findTop1ByCompanyAndCodeIgnoreCaseAndIsactiveTrue(Company company, String code);

	City findTop1ByCompanyAndRegionAndCodeIgnoreCaseAndIsactiveTrue(Company company, Region region,  String code);

	List<City> findByCompanyAndRegionAndIsactiveTrue(Company company, Region region);

}
