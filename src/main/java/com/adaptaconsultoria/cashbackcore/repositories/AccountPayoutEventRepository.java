package com.adaptaconsultoria.cashbackcore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.AccountPayoutEvent;

public interface AccountPayoutEventRepository extends JpaRepository<AccountPayoutEvent, Long> {
}
