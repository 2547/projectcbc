package com.adaptaconsultoria.cashbackcore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.App;

public interface AppRepository extends JpaRepository<App, Long> {

	App findTop1ByTokenAndIsactiveTrue(String appToken);

}
