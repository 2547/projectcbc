package com.adaptaconsultoria.cashbackcore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.Payout;

public interface PayoutRepository extends JpaRepository<Payout, Long> {

	List<Payout> findByCompany(Company company);

	List<Payout> findByCompanyAndIsactiveTrue(Company company);

	Payout findTop1ByCompanyAndId(Company company, Long id);

}
