package com.adaptaconsultoria.cashbackcore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.Account;
import com.adaptaconsultoria.cashbackcore.models.AccountPayout;
import com.adaptaconsultoria.cashbackcore.models.Company;

public interface AccountPayoutRepository extends JpaRepository<AccountPayout, Long> {

	AccountPayout findTop1ByCompanyAndId(Company company, Long id);

	List<AccountPayout> findByCompanyAndAccount(Company company, Account account);

}
