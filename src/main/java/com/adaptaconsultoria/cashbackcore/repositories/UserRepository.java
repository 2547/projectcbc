package com.adaptaconsultoria.cashbackcore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.User;

public interface UserRepository extends JpaRepository<User, Long> {

	@Query("select u from User u where u.company = :company and lower(u.login) = lower(:login) order by u.login")
	User findTop1ByCompanyAndLoginOrderByLogin(@Param("company") Company company, @Param("login") String login);

	@Query("select u from User u where u.company = :company and lower(u.email) = lower(:email) order by u.email")
	User findTop1ByCompanyAndEmailOrderByEmail(@Param("company") Company company, @Param("email") String email);

	User findTop1ByCompanyAndLoginAndIsactiveTrueOrderByLogin(Company company, String login);

	List<User> findByCompanyOrderByName(Company company);
	
	User findByLoginAndIsadminTrueAndIsactiveTrueOrderByLogin(String login);

}
