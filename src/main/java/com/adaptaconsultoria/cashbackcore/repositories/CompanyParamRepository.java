package com.adaptaconsultoria.cashbackcore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.CompanyParam;

public interface CompanyParamRepository extends JpaRepository<CompanyParam, Long> {

	CompanyParam findTop1ByCompanyAndNameAndIsactiveTrue(Company company, String name);

}
