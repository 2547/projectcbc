package com.adaptaconsultoria.cashbackcore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.Entity;

public interface EntityRepository extends JpaRepository<Entity, Long> {

	Entity findTop1ByCompanyAndId(Company company, Long id);

	List<Entity> findByCompany(Company company);

	List<Entity> findByCompanyAndIsactiveTrue(Company company);

	Entity findTop1ByCompanyAndIdAndIsactiveTrue(Company company, Long id);

}
