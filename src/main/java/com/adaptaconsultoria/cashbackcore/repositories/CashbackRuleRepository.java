package com.adaptaconsultoria.cashbackcore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.CashbackRule;

public interface CashbackRuleRepository extends JpaRepository<CashbackRule, Long> {

	List<CashbackRule> findByCompany(Company company);

	CashbackRule findTop1ByCompanyAndId(Company company, Long id);

}
