package com.adaptaconsultoria.cashbackcore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.IPBlocked;

public interface IPBlockedRepository extends JpaRepository<IPBlocked, Long> {

	IPBlocked findTop1ByIpAddressAndIsactiveTrue(String ipAddress);

}
