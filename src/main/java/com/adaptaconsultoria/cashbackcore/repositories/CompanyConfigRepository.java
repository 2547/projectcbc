package com.adaptaconsultoria.cashbackcore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.CompanyConfig;

public interface CompanyConfigRepository extends JpaRepository<CompanyConfig, Long> {

	CompanyConfig findTop1ByCompanyAndIsactiveTrue(Company company);

}
