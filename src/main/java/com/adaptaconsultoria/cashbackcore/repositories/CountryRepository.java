package com.adaptaconsultoria.cashbackcore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.Country;

public interface CountryRepository extends JpaRepository<Country, Long> {

	Country findTop1ByCompanyAndIsoCodeIgnoreCaseAndIsactiveTrue(Company company, String isoCode);

	List<Country> findByCompanyAndIsactiveTrue(Company company);

}
