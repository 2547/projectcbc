package com.adaptaconsultoria.cashbackcore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.cashbackcore.models.DashboardStats;

public interface DashboardRepository extends JpaRepository<DashboardStats, Long> {

	@Query(value = "SELECT * FROM account_stats(:accountId)", nativeQuery = true)
	List<DashboardStats> findStatsByAccount(@Param("accountId") Long accountId);

}
