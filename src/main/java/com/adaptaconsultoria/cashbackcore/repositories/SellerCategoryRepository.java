package com.adaptaconsultoria.cashbackcore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.SellerCategory;

public interface SellerCategoryRepository extends JpaRepository<SellerCategory, Long> {

	List<SellerCategory> findByCompany(Company company);

	SellerCategory findTop1ByCompanyAndId(Company company, Long id);

}
