package com.adaptaconsultoria.cashbackcore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.LogAccess;

public interface LogAccessRepository extends JpaRepository<LogAccess, Long> {

}