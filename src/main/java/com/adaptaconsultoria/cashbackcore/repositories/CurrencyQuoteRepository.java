package com.adaptaconsultoria.cashbackcore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.Currency;
import com.adaptaconsultoria.cashbackcore.models.CurrencyQuote;

public interface CurrencyQuoteRepository extends JpaRepository<CurrencyQuote, Long> {

	CurrencyQuote findTop1ByCompanyAndCurrencyAndQuotedCurrencyAndIsactiveTrueOrderByQuotedDesc(Company company, Currency currency, Currency quotedCurrency);

}
