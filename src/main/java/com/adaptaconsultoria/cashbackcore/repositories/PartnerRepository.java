package com.adaptaconsultoria.cashbackcore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.cashbackcore.models.Account;
import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.Partner;

public interface PartnerRepository extends JpaRepository<Partner, Long> {

	Partner findTop1ByCompanyAndAccount(Company company, Account account);

	Partner findTop1ByCompanyAndAccountAndIsactiveTrue(Company company, Account account);

	Partner findTop1ByCompanyAndEmailIgnoreCase(Company company, String email);

	/* ******** */
	/* Parceiro */
	/* ******** */
	@Query("select p from Partner p where p.company = :company and p.partnerCategory is not null and id = :id")
	Partner findTop1ByCompanyAndIdAndPartner(@Param("company") Company company, @Param("id") Long id);

	@Query("select p from Partner p where p.company = :company and p.partnerCategory is not null and p.account.accountNo = :accountNo")
	Partner findTop1ByCompanyAndAccountNoAndPartnerCategoryNotNull(@Param("company") Company company, @Param("accountNo") String accountNo);

	@Query("select p from Partner p where p.company = :company and p.partnerCategory is not null")
	List<Partner> findByCompanyAndPartnerCategoryNotNull(@Param("company") Company company);

	@Query("select p from Partner p "
			+ "where p.company = :company and p.isactive = true and "
			+ "p.partnerCategory is not null and p.partnerCategory.isleader = true "
			+ "and ("
			+     "upper(p.name) like (:query) or "
			+     "upper(p.email) like upper(:query) or "
			+     "upper(p.phone) like upper(:query) "
			+ ")")
	List<Partner> findByCompanyAndPartnerLeader(@Param("company") Company company, @Param("query") String query);

	/* ************ */
	/* Credenciados */
	/* ************ */
	@Query("select p from Partner p where p.company = :company and p.sellerCategory is not null and id = :id")
	Partner findTop1ByCompanyAndIdAndSeller(@Param("company") Company company, @Param("id") Long id);

	@Query("select p from Partner p where p.company = :company and p.sellerCategory is not null")
	List<Partner> findByCompanyAndSellerCategoryNotNull(@Param("company") Company company);

	@Query("select p from Partner p where p.company = :company and p.sellerCategory is not null and p.account.accountNo = :accountNo")
	Partner findTop1ByCompanyAndAccountNoAndSellerCategoryNotNull(@Param("company") Company company, @Param("accountNo") String accountNo);

}
