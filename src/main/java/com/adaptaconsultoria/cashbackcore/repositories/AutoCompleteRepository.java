package com.adaptaconsultoria.cashbackcore.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adaptaconsultoria.cashbackcore.models.AutoComplete;

public interface AutoCompleteRepository extends JpaRepository<AutoComplete, Long> {

	@Query(value = "select p.partner_id as id, p.name as text from partner p " + 
			"where p.company_id = :company and p.isactive = true and p.partner_category_id is not null " + 
			"and (" + 
			"	(:query is null or :query = '') or " +
			"   ( " + 
			"	    upper(p.name) like upper(:query) or " + 
			"	    upper(p.email) like upper(:query) or " + 
			"	    upper(p.phone) like upper(:query) " + 
			"   ) " + 
			") order by p.name", nativeQuery = true)
	List<AutoComplete> findPartnerByCompany(@Param("company") Long company, @Param("query") String query);

	@Query(value = "select p.partner_id as id, p.name as text from partner p " + 
			"join partner_category pc on pc.partner_category_id = p.partner_category_id and pc.isleader " + 
			"where p.company_id = :company and p.isactive = true " + 
			"and (" + 
			"	(:query is null or :query = '') or " +
			"   ( " + 
			"	    upper(p.name) like upper(:query) or " + 
			"	    upper(p.email) like upper(:query) or " + 
			"	    upper(p.phone) like upper(:query) " + 
			"   ) " + 
			") order by p.name", nativeQuery = true)
	List<AutoComplete> findPartnerLeaderByCompany(@Param("company") Long company, @Param("query") String query);

	@Query(value = "select c.country_id as id, c.name as text from country c " + 
			"where c.company_id = :company and c.isactive = true " + 
			"and ( " + 
			"	(:query is null or :query = '') or " +
			"   ( " + 
			"	    upper(c.name) like upper(:query) or " + 
			"	    upper(c.iso_code) like upper(:query)" +
			"   ) " + 
			") order by c.name", nativeQuery = true)
	List<AutoComplete> findCountryByCompany(@Param("company") Long company, @Param("query") String query);

	@Query(value = "select r.region_id as id, r.name as text from region r " + 
			"where r.company_id = :company and r.isactive = true " + 
			"and ( " + 
			"	(:country is null or :country = '') or ( " + 
			"		r.country_id = cast(:country as bigint) " + 
			"	) " + 
			") and ( " + 
			"	(:query is null or :query = '') or " + 
			"	( " + 
			"		upper(r.name) like upper(:query) or " + 
			"		upper(r.code) like upper(:query) " + 
			"	) " + 
			") order by r.name", nativeQuery = true)
	List<AutoComplete> findRegionByCompanyAndCountry(@Param("company") Long company, @Param("country") String country, @Param("query") String query);

	@Query(value = "select c.city_id as id, c.name as text from city c " + 
			"join region r on r.region_id = c.region_id " + 
			"where c.company_id = :company and c.isactive = true " + 
			"and ( " + 
			"	(:country is null or :country = '') or ( " + 
			"		r.country_id = cast(:country as bigint) " + 
			"	) " + 
			") and ( " + 
			"	(:region is null or :region = '') or ( " + 
			"		c.region_id = cast(:region as bigint) " + 
			"	) " + 
			") and ( " + 
			"	(:query is null or :query = '') or " + 
			"	( " + 
			"		upper(c.name) like upper(:query) or " + 
			"		upper(c.code) like upper(:query) " + 
			"	) " + 
			") order by c.name" + 
			"", nativeQuery = true)
	List<AutoComplete> findCityByCompanyAndRegionAndCountry(@Param("company") Long company, @Param("region") String region, @Param("country") String country, @Param("query") String query);

	@Query(value = "select a.account_id as id, a.accountno || ' - ' || p.name as text from account a " + 
			"left join entity e on e.account_id = a.account_id " +
			"join partner p on p.account_id = a.account_id " + 
			"join partner_category pc on pc.partner_category_id = p.partner_category_id and pc.isleader = false " +
			"where a.company_id = :company and a.isactive = true " + 
			"and e.account_id is null " + 
			"and (" + 
			"	(:query is null or :query = '') or " + 
			"	( " + 
			"		upper(a.accountno) = replace(:query, '%', '') or " + 
			"		upper(p.name) like upper(:query) or " + 
			"		upper(p.email) like upper(:query) or " + 
			"		upper(p.phone) like upper(:query) " + 
			"	) " + 
			") order by a.accountno", nativeQuery = true)
	List<AutoComplete> findAccountNoEntityByCompany(@Param("company") Long company, @Param("query") String query);

	@Query(value = "select a.account_id as id, a.accountno || ' - ' || u.name as text " + 
			"from account a " + 
			"join users u on u.users_id = a.users_id " + 
			"where a.company_id = :company and a.isactive = true " + 
			"and ( " + 
			"	upper(a.accountno) = replace(:query, '%', '') or " + 
			"	upper(u.name) like upper(:query) or " + 
			"	upper(u.email) like upper(:query) or " + 
			"	upper(u.phone) like upper(:query) " + 
			") order by a.accountno", nativeQuery = true)
	List<AutoComplete> findAccountByCompany(@Param("company") Long company, @Param("query") String query);

	@Query(value = "select a.accountno as id, (u.name || ' - ' || COALESCE(a.taxid || ' - ', '') || a.accountno) as text " + 
			"from account a " + 
			"join users u on u.users_id = a.users_id " + 
			"where a.company_id = :company and a.isactive = true " + 
			"and replace(CAST(COALESCE(:query, '') AS TEXT), '%', '') <> '' " + 
			"and ( " + 
			"	upper(a.accountno) = replace(CAST(:query AS TEXT), '%', '') or " + 
			"	upper(u.name)  like upper(CAST(:query AS TEXT)) or " + 
			"	upper(u.email) like replace(CAST(:query AS TEXT), '%', '') or " + 
			"	upper(a.taxid) like replace(CAST(:query AS TEXT), '%', '') or " + 
			"	upper( " + 
			"		replace( " + 
			"			replace( " + 
			"				replace( " + 
			"					replace(u.phone, '-', '') " + 
			"				, ' ', '') " + 
			"			, ')', '') " + 
			"		, '(', '') " + 
			"	) = replace(CAST(:query AS TEXT), '%', '') " + 
			") order by a.accountno", nativeQuery = true)
	List<AutoComplete> findAccountNoByCompany(@Param("company") Long company, @Param("query") String query);

	@Query(value = "select ap.account_payout_id as id, p.name as text from account_payout ap " + 
			"join payout p on p.payout_id = ap.payout_id " + 
			"where ap.company_id = :company and ap.account_id = cast(:account as bigint) " + 
			"and ( " + 
			"	(:query is null or :query = '') or " + 
			"	( " + 
			"		upper(p.name) like upper(:query) " + 
			"	) " + 
			") " + 
			"order by ap.account_id", nativeQuery = true)
	List<AutoComplete> findAccountPayoutByCompanyAndAccount(@Param("company") Long company, @Param("query") String query, @Param("account") String account);

}
