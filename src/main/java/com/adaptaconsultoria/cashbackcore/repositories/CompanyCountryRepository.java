package com.adaptaconsultoria.cashbackcore.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adaptaconsultoria.cashbackcore.models.Company;
import com.adaptaconsultoria.cashbackcore.models.CompanyCountry;
import com.adaptaconsultoria.cashbackcore.models.Country;

public interface CompanyCountryRepository extends JpaRepository<CompanyCountry, Long> {

	CompanyCountry findTop1ByCompanyAndCountryAndIsactiveTrue(Company company, Country country);

}
